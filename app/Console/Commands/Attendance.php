<?php

namespace App\Console\Commands;

use App\Models\AttendanceWeeklyOff;
use App\Models\CalendarEvent;
use App\Models\Employee;
use App\Models\Leave;
use App\Models\RawAttendance;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Attendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will get the raw attendance from data table every five miniutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::today();
        //        $start=$today->subMonth('1');
        ////        dd($start);
        $employees = Employee::get();
        foreach ($employees as $employee) {

            $rawData = RawAttendance::query()
                ->where('access_date', $today->format("Y-m-d"))
                ->where('registration_id', $employee->employee_no)
                ->get();
            //            dd($rawData);
            $max        = $rawData->max('access_time');
            $min        = $rawData->min('access_time');
            $shift      = $employee->officials->first()->shift;
            $shiftIn    = $shift->in;
            $shiftOut   = $shift->out;
            $access_date= new Carbon($rawData->access_date);

            // Weekly Off
            foreach ($employee->days as $key => $weeklyOff) {
                if ($weeklyOff = $weeklyOff->id == $today->format('N')) {
                    break;
                }
            }

            // Holiday
            $calendarId = $employee->officials->calendar_id;
            $holiday = CalendarEvent::query()
                ->where('calendar_id', $calendarId)
                ->where('start', '<=', $today->format('Y-m-d'))
                ->where('end', '>=', $today->format('Y-m-d'))
                ->where('is_holiday', 1)
                ->exists();

            // Leave Management
            // $leaves = Leave::where('employee_id', $employee->id)->get();
            // if ($leaves->exists()) {
            //     foreach ($leaves as $key => $leave) {
            //         $from = new Carbon($leave->leave_from);
            //         $to = new Carbon($leave->leave_to);
            //         if ($newLeave = ($today <= $from && $today >= $to)) {
            //             break;
            //         }
            //     }
            // } else {
            //     $newLeave = false;
            // }

            $leave = Leave::query()
                ->where('employee_id', $employee->id)
                ->where('leave_from', '<=', $today->format('Y-m-d'))
                ->where('leave_to', '>=', $today->format('Y-m-d'))
                ->exists();
            // Attendance Status
            // dd($newLeave);
            // dd($leave);
            if ($leave) {
                $attendanceStatus = '7';
            } elseif ($rawData->min('access_time') == null) {
                $attendanceStatus = '2';
            } elseif ($rawData->min('access_time') <= $shiftIn && $rawData->max('access_time') <= $shiftOut) {
                $attendanceStatus = '4';
            } elseif ($rawData->min('access_time') > $shiftIn) {
                $attendanceStatus = '3';
            } elseif ($rawData->min('access_time') <= $shiftIn) {
                $attendanceStatus = '1';
            } elseif ($holiday) {
                $attendanceStatus = 5;
            } elseif ($weeklyOff == true) {
                $attendanceStatus = 6;
            }

            // Ot
            //            dd($max);
            if ($weeklyOff == true) {
                $difference     = $min->diff($max);
                $ot             = $difference->format('%h');
            } elseif ($max > $shiftOut) {
                $difference     = $shiftOut->diff($max);
                $ot             = $difference->format('%h');
            } else {
                $ot = null;
            }

            // Extra Ot
            if ($ot <= 2 || $ot == null) {
                $extraOt = null;
            } else {
                $extraOt = ($ot - 2);
            }

            $attendanceWeeklyOffs = DB::table('employee_weekly_off')->where('employee_id', $employee->id)->get();
            foreach ($attendanceWeeklyOffs as $attendanceWeeklyOff) {
                AttendanceWeeklyOff::create([
                    'day_id'        => $attendanceWeeklyOff->day_id,
                    'employee_id'   => $attendanceWeeklyOff->employee_id,
                ]);
            }

            $data = [
                'employee_id'           => $employee->id,
                'date'                  => $access_date,
                'in_time'               => $rawData->min('access_time'),
                'out_time'              => $rawData->max('access_time'),
                'ot'                    => $ot,
                'extra_ot'              => $extraOt,
                'unit_id'               => $employee->officials->first()->unit_id,
                'floor_id'              => $employee->officials->first()->floor_id,
                'section_id'            => $employee->officials->first()->section_id,
                'department_id'         => $employee->officials->first()->department_id,
                'line_id'               => $employee->officials->first()->line_id,
                'designation_id'        => $employee->officials->first()->designation_id,
                'employee_type_id'      => $employee->officials->first()->employee_type_id,
                'grade_id'              => $employee->officials->first()->grade_id,
                'shift_id'              => $employee->officials->first()->shift_id,
                'calendar_id'           => $employee->officials->first()->calendar_id,
                'leave_category_id'     => $employee->officials->first()->leave_category_id,
                'attendance_status_id'  => $attendanceStatus,
                'weekly_off_id'         => $weeklyOff,
            ];
            //            dd($data);
            Attendance::create($data);
        }
    }
}
