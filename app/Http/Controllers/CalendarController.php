<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index()
    {
        $calendars = Calendar::paginate(10);
        return view('dashboard.calendar.index', compact('calendars'));
    }

    // public function create()
    // {
    //     return view('dashboard.calendar.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'         => ['required', 'string','max:255'],
            'description'  => ['string','nullable'],
            'is_active'    => ['boolean'],
        ]);

        Calendar::create($validated);

        return redirect()->route('calendar.index')->with('success', 'Calendar Created Successfully.');
    }

    public function edit(Calendar $calendar)
    {
        return view('dashboard.calendar.edit', compact('calendar'));
    }

    public function update(Request $request, Calendar $calendar)
    {
        $validated = $request->validate([
            'name'         => ['required', 'string','max:255'],
            'description'  => ['string','nullable'],
            'is_active'    => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $calendar->update($validated);

        return redirect()->route('calendar.index')->with('success', 'Calendar updated successfully.');
    }

    public function destroy(Calendar $calendar)
    {
        $calendar->delete();
        return back()->with('delete', 'Calendar deleted successfully.');
    }

    public function status(Calendar $calendar)
    {
        if ($calendar->is_active==0) {
            $calendar->is_active ='1';
        }
        else{
            $calendar->is_active ='0';
        }

        $calendar->update();

        return redirect()->route('calendar.index')->with('success', 'Status updated successfully.');
    }
}
