<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\CalendarEvent;
use Illuminate\Http\Request;

class CalendarEventController extends Controller
{
    public function index()
    {
        $calendarEvents = CalendarEvent::with('calendar')->paginate(10);
        $calendars = Calendar::pluck('name','id');
        // dd($calendars);
        return view('dashboard.calendarEvent.index', compact('calendarEvents','calendars'));
    }

    // public function create()
    // {
    //     return view('dashboard.calendarEvent.create');
    // }

    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'calendar_id'   => ['integer'],
            'start'         => ['required', 'date'],
            'end'           => ['date'],
            'is_holiday'    => ['boolean'],
            'sms'           => ['boolean'],
            'email'         => ['boolean'],
            'is_active'     => ['boolean'],
        ]);

        CalendarEvent::create($validated);

        return redirect()->route('calendarEvent.index')->with('success', 'CalendarEvent Created Successfully.');
    }

    public function edit(CalendarEvent $calendarEvent)
    {
        $calendars = Calendar::pluck('name','id');
        return view('dashboard.calendarEvent.edit', compact('calendarEvent','calendars'));
    }

    public function update(Request $request, CalendarEvent $calendarEvent)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'calendar_id'   => ['required', 'integer'],
            'start'         => ['required', 'date'],
            'end'           => ['date'],
            'is_holiday'    => ['boolean'],
            'sms'           => ['boolean'],
            'email'         => ['boolean'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $calendarEvent->update($validated);

        return redirect()->route('calendarEvent.index')->with('success', 'CalendarEvent updated successfully.');
    }

    public function destroy(CalendarEvent $calendarEvent)
    {
        $calendarEvent->delete();
        return back()->with('delete', 'CalendarEvent deleted successfully.');
    }

    public function status(CalendarEvent $calendarEvent)
    {
        if ($calendarEvent->is_active==0) {
            $calendarEvent->is_active ='1';
        }
        else{
            $calendarEvent->is_active ='0';
        }

        $calendarEvent->update();

        return redirect()->route('calendarEvent.index')->with('success', 'Status updated successfully.');
    }
}
