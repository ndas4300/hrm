<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\AcademicResult;
use Illuminate\Http\Request;

class AcademicResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academicResults = AcademicResult::paginate(10);
        return view('dashboard.employee.academicResult.index', compact('academicResults'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        AcademicResult::create($validated);

        return redirect()->route('academicResult.index')->with('success', 'AcademicResult Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AcademicResult  $academicResult
     * @return \Illuminate\Http\Response
     */
    public function show(AcademicResult $academicResult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AcademicResult  $academicResult
     * @return \Illuminate\Http\Response
     */
    public function edit(AcademicResult $academicResult)
    {
        return view('dashboard.employee.academicResult.edit', compact('academicResult'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AcademicResult  $academicResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcademicResult $academicResult)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $academicResult->update($validated);

        return redirect()->route('academicResult.index')->with('success', 'AcademicResult updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AcademicResult  $academicResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(AcademicResult $academicResult)
    {
        $academicResult->delete();
        return back()->with('delete', 'AcademicResult deleted successfully.');
    }

    public function status(AcademicResult $academicResult)
    {
        if ($academicResult->is_active==0) {
            $academicResult->is_active ='1';
        }
        else{
            $academicResult->is_active ='0';
        }

        $academicResult->update();

        return redirect()->route('academicResult.index')->with('success', 'Status updated successfully.');
    }
}
