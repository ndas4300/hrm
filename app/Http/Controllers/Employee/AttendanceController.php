<?php

namespace App\Http\Controllers\Employee;

use App\Models\AttendanceWeeklyOff;
use App\Models\CalendarEvent;
use App\Models\Leave;
use GuzzleHttp\Client;
use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Models\RawAttendance;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeOfficial;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return false
     */
    public function index()
    {
        $today = Carbon::today();
        //        $start=$today->subMonth('1');
        ////        dd($start);
        $employees = Employee::get();
        foreach ($employees as $employee) {

            $rawData = RawAttendance::query()
                ->where('access_date', $today->format("Y-m-d"))
                ->where('registration_id', $employee->employee_no)
                ->get();
            //            dd($rawData);
            $max        = $rawData->max('access_time');
            $min        = $rawData->min('access_time');
            $shift      = $employee->officials->first()->shift;
            $shiftIn    = $shift->in;
            $shiftOut   = $shift->out;
            $access_date= new Carbon($rawData->access_date);

            // Weekly Off
            foreach ($employee->days as $key => $weeklyOff) {
                if ($weeklyOff = $weeklyOff->id == $today->format('N')) {
                    break;
                }
            }

            // Holiday
            $calendarId = $employee->officials->calendar_id;
            $holiday = CalendarEvent::query()
                ->where('calendar_id', $calendarId)
                ->where('start', '<=', $today->format('Y-m-d'))
                ->where('end', '>=', $today->format('Y-m-d'))
                ->where('is_holiday', 1)
                ->exists();

            // Leave Management
            // $leaves = Leave::where('employee_id', $employee->id)->get();
            // if ($leaves->exists()) {
            //     foreach ($leaves as $key => $leave) {
            //         $from = new Carbon($leave->leave_from);
            //         $to = new Carbon($leave->leave_to);
            //         if ($newLeave = ($today <= $from && $today >= $to)) {
            //             break;
            //         }
            //     }
            // } else {
            //     $newLeave = false;
            // }

            $leave = Leave::query()
                ->where('employee_id', $employee->id)
                ->where('leave_from', '<=', $today->format('Y-m-d'))
                ->where('leave_to', '>=', $today->format('Y-m-d'))
                ->exists();
            // Attendance Status
            // dd($newLeave);
            // dd($leave);
            if ($leave) {
                $attendanceStatus = '7';
            } elseif ($rawData->min('access_time') == null) {
                $attendanceStatus = '2';
            } elseif ($rawData->min('access_time') <= $shiftIn && $rawData->max('access_time') <= $shiftOut) {
                $attendanceStatus = '4';
            } elseif ($rawData->min('access_time') > $shiftIn) {
                $attendanceStatus = '3';
            } elseif ($rawData->min('access_time') <= $shiftIn) {
                $attendanceStatus = '1';
            } elseif ($holiday) {
                $attendanceStatus = 5;
            } elseif ($weeklyOff == true) {
                $attendanceStatus = 6;
            }

            // Ot
            //            dd($max);
            if ($weeklyOff == true) {
                $difference     = $min->diff($max);
                $ot             = $difference->format('%h');
            } elseif ($max > $shiftOut) {
                $difference     = $shiftOut->diff($max);
                $ot             = $difference->format('%h');
            } else {
                $ot = null;
            }

            // Extra Ot
            if ($ot <= 2 || $ot == null) {
                $extraOt = null;
            } else {
                $extraOt = ($ot - 2);
            }

            $attendanceWeeklyOffs = DB::table('employee_weekly_off')->where('employee_id', $employee->id)->get();
            foreach ($attendanceWeeklyOffs as $attendanceWeeklyOff) {
                AttendanceWeeklyOff::create([
                    'day_id'        => $attendanceWeeklyOff->day_id,
                    'employee_id'   => $attendanceWeeklyOff->employee_id,
                ]);
            }

            $data = [
                'employee_id'           => $employee->id,
                'date'                  => $access_date,
                'in_time'               => $rawData->min('access_time'),
                'out_time'              => $rawData->max('access_time'),
                'ot'                    => $ot,
                'extra_ot'              => $extraOt,
                'unit_id'               => $employee->officials->first()->unit_id,
                'floor_id'              => $employee->officials->first()->floor_id,
                'section_id'            => $employee->officials->first()->section_id,
                'department_id'         => $employee->officials->first()->department_id,
                'line_id'               => $employee->officials->first()->line_id,
                'designation_id'        => $employee->officials->first()->designation_id,
                'employee_type_id'      => $employee->officials->first()->employee_type_id,
                'grade_id'              => $employee->officials->first()->grade_id,
                'shift_id'              => $employee->officials->first()->shift_id,
                'calendar_id'           => $employee->officials->first()->calendar_id,
                'leave_category_id'     => $employee->officials->first()->leave_category_id,
                'attendance_status_id'  => $attendanceStatus,
                'weekly_off_id'         => $weeklyOff,
            ];
            //            dd($data);
            Attendance::create($data);
        }
    }
    /**+
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //     $data = array(
        //         "operation" => "fetch_log", "auth_user" => "webpoint", "auth_code"
        //         => "3efd234cefa324567a342deafd32672", "start_date" => "2022-02-01", "end_date" =>
        //         "2022-02-23", "start_time" => "00:00:00",
        //         "end_time" => "23:59:59"
        //     );


        //     $datapayload = json_encode($data);
        //     $api_request = curl_init('https://rumytechnologies.com/rams/json_api');
        //     curl_setopt($api_request, CURLOPT_RETURNTRANSFER, true);
        //     curl_setopt($api_request, CURLINFO_HEADER_OUT, true);
        //     curl_setopt($api_request, CURLOPT_POST, true);
        //     curl_setopt($api_request, CURLOPT_POSTFIELDS, $datapayload);
        //     curl_setopt($api_request, CURLOPT_HTTPHEADER, array('Content-Type:
        // application/json', 'Content-Length: ' . strlen($datapayload)));
        //     $result = curl_exec($api_request);
        //     // dd($result);
        //     $replace_syntax = str_replace('{"log":', "", $result);
        //     // dd($replace_syntax);
        //     // print_r($replace_syntax);
        //     $replace_syntax = substr($replace_syntax, 0, -1);
        //     // print_r($replace_syntax);
        //     $json_data = json_decode($replace_syntax);
        //     // dd($json_data);
        //     foreach ($json_data as $data) {
        //         Attendance::create([
        //             'employee_id' => $data->registration_id,
        //         ]);

        //     }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        return view('dashboard.employee.attendance.edit', compact('attendance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {

        $validated = $request->validate([
            'employee_id'           => ['required', 'integer'],
            'date'                  => ['required', 'date'],
            'in_time'               => ['required', 'date'],
            'out_time'              => ['required', 'date'],
            'ot'                    => ['required', 'date'],
            'extra_ot'              => ['required', 'date'],
            'section_id'            => ['required', 'integer'],
            'line_id'               => ['required', 'integer'],
            'department_id'         => ['required', 'integer'],
            'leave_category_id'     => ['required', 'integer'],
            'weekly_off_id'         => ['required', 'integer'],
            'attendance_status_id'  => ['required', 'integer'],
        ]);

        $attendance->update($validated);

        return redirect()->route('attendance.index')->with('success', 'Attendance updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        $attendance->delete();
        return back()->with('delete', 'Attendance deleted successfully.');
    }
}
