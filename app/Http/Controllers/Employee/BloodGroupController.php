<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\BloodGroup;
use Illuminate\Http\Request;

class BloodGroupController extends Controller
{
    public function index()
    {
        $bloodGroups = BloodGroup::paginate(10);
        return view('dashboard.employee.bloodGroup.index', compact('bloodGroups'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.bloodGroup.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],

        ]);

        BloodGroup::create($validated);

        return redirect()->route('bloodGroup.index')->with('success', 'BloodGroup Created Successfully.');
    }

    public function edit(BloodGroup $bloodGroup)
    {
        return view('dashboard.employee.bloodGroup.edit', compact('bloodGroup'));
    }

    public function update(Request $request, BloodGroup $bloodGroup)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $bloodGroup->update($validated);

        return redirect()->route('bloodGroup.index')->with('success', 'BloodGroup updated successfully.');
    }

    public function destroy(BloodGroup $bloodGroup)
    {
        $bloodGroup->delete();
        return back()->with('delete', 'BloodGroup deleted successfully.');
    }
    public function status(BloodGroup $bloodGroup)
    {
        if ($bloodGroup->is_active==0) {
            $bloodGroup->is_active ='1';
        }
        else{
            $bloodGroup->is_active ='0';
        }

        $bloodGroup->update();

        return redirect()->route('bloodGroup.index')->with('success', 'Status updated successfully.');
    }

}
