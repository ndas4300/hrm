<?php

namespace App\Http\Controllers\Employee;

use App\Models\Card;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CardController extends Controller
{
    public function index()
    {
        $cards = Card::with('employee')->paginate(10);
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.card.index', compact('cards','employee'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.card.create');
    // }

    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'employee_id'   => ['required', 'integer'],
            'number'        => ['required', 'string'],
            'assigned'      => ['required', 'date'],
            'note'          => ['required', 'string'],
            'is_active'     => ['boolean'],

        ]);

        Card::create($validated);

        return redirect()->route('card.index')->with('success', 'Card Created Successfully.');
    }

    public function edit(Card $card)
    {
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.card.edit', compact('card','employee'));
    }

    public function update(Request $request, Card $card)
    {
        $validated = $request->validate([
            'employee_id'   => ['required', 'integer'],
            'number'        => ['required', 'string'],
            'assigned'      => ['required', 'date'],
            'note'          => ['required', 'string'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $card->update($validated);

        return redirect()->route('card.index')->with('success', 'Card updated successfully.');
    }

    public function destroy(Card $card)
    {
        $card->delete();
        return back()->with('delete', 'Card deleted successfully.');
    }

    public function status(Card $card)
    {
        if ($card->is_active==0) {
            $card->is_active ='1';
        }
        else{
            $card->is_active ='0';
        }

        $card->update();

        return redirect()->route('card.index')->with('success', 'Status updated successfully.');
    }
}
