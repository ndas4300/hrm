<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Day;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $days = Day::paginate(10);
        return view('dashboard.employee.day.index', compact('days'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','unique:days', 'max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Day::create($validated);

        return redirect()->route('day.index')->with('success', 'Day Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param Day $day
     * @return void
     */
    public function show(Day $day)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Day $day
     * @return Response
     */
    public function edit(Day $day)
    {
        return view('dashboard.employee.day.edit', compact('day'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Day $day
     * @return Response
     */
    public function update(Request $request, Day $day)
    {
        // dd($request->all());
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $day->update($validated);

        return redirect()->route('day.index')->with('success', 'Day updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Day $day
     * @return Response
     */
    public function destroy(Day $day)
    {
        $day->delete();
        return back()->with('delete', 'Day deleted successfully.');
    }

    public function status(Day $day): \Illuminate\Http\RedirectResponse
    {
        if ($day->is_active==0) {
            $day->is_active ='1';
        }
        else{
            $day->is_active ='0';
        }

        $day->update();

        return redirect()->route('day.index')->with('success', 'Status updated successfully.');
    }
}
