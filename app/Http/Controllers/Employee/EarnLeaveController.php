<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\EarnLeave;
use App\Models\Employee;
use Illuminate\Http\Request;

class EarnLeaveController extends Controller
{
    public function index()
    {
        $earnLeaves = EarnLeave::with('employee')->paginate(10);
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.earnLeave.index', compact('earnLeaves','employee'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.earnLeave.create');
    // }

    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'employee_id'       => ['required', 'integer'],
            'date'              => ['required', 'date'],
            'balance'           => ['required', 'integer'],
            'previous_balance'  => ['required', 'integer'],
            'next_schedule'     => ['required', 'date'],
        ]);

        EarnLeave::create($validated);

        return redirect()->route('earnLeave.index')->with('success', 'EarnLeave Created Successfully.');
    }

    public function edit(EarnLeave $earnLeave)
    {
        dd($earnLeave);
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.earnLeave.edit', compact('earnLeave','employee'));
    }

    public function update(Request $request, EarnLeave $earnLeave)
    {
        $validated = $request->validate([
            'employee_id'       => ['required', 'integer'],
            'date'              => ['required', 'date'],
            'balance'           => ['required', 'integer'],
            'previous_balance'  => ['required', 'integer'],
            'next_schedule'     => ['required', 'date'],
        ]);

        $earnLeave->update($validated);

        return redirect()->route('earnLeave.index')->with('success', 'EarnLeave updated successfully.');
    }

    public function destroy(EarnLeave $earnLeave)
    {
        $earnLeave->delete();
        return back()->with('delete', 'EarnLeave deleted successfully.');
    }
}
