<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\AcademicResult;
use App\Models\Education;
use App\Models\Employee;
use App\Models\EmployeeAcademic;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeAcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $employeeAcademics  = EmployeeAcademic::with('employee','education','academicResult')->paginate(10);
        $employee           = Employee::pluck('name','id');
        $education          = Education::pluck('name','id');
        $academicResult     = AcademicResult::pluck('name','id');
        return view('dashboard.employee.employeeAcademic.index', compact('employeeAcademics','employee','education','academicResult'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'         => ['required','integer'],
            'education_id'        => ['required','integer'],
            'degree'              => ['required','string'],
            'major'               => ['required','string'],
            'institute'           => ['required','string'],
            'academic_result_id'  => ['required','integer'],
            'marks'               => ['required','integer'],
            'year'                => ['required','integer'],
            'duration'            => ['required','integer'],
            'achievement'         => ['required','string'],
        ]);

        EmployeeAcademic::create($validated);

        return redirect()->back()->with('success', 'EmployeeAcademic Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeAcademic  $employeeAcademic
     * @return Response
     */
    public function show(EmployeeAcademic $employeeAcademic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeAcademic  $employeeAcademic
     * @return Response
     */
    public function edit(EmployeeAcademic $employeeAcademic)
    {
        $employee       = Employee::pluck('name','id');
        $education      = Education::pluck('name','id');
        $academicResult = AcademicResult::pluck('name','id');
        return view('dashboard.employee.employeeAcademic.edit', compact('employeeAcademic','employee','education','academicResult'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeAcademic  $employeeAcademic
     * @return Response
     */
    public function update(Request $request, EmployeeAcademic $employeeAcademic)
    {
        $validated = $request->validate([
            'employee_id'         => ['required','integer'],
            'education_id'        => ['required','integer'],
            'degree'              => ['required','string'],
            'major'               => ['required','string'],
            'institute'           => ['required','string'],
            'academic_result_id'  => ['required','integer'],
            'marks'               => ['required','integer'],
            'year'                => ['required','integer'],
            'duration'            => ['required','integer'],
            'achievement'         => ['required','string'],
        ]);

        $employeeAcademic->update($validated);

        return redirect()->route('employeeAcademic.index')->with('success', 'EmployeeAcademic updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeAcademic  $employeeAcademic
     * @return Response
     */
    public function destroy(EmployeeAcademic $employeeAcademic)
    {
        $employeeAcademic->delete();
        return back()->with('delete', 'EmployeeAcademic deleted successfully.');
    }

    public function status(EmployeeAcademic $employeeAcademic)
    {
        if ($employeeAcademic->is_active==0) {
            $employeeAcademic->is_active ='1';
        }
        else{
            $employeeAcademic->is_active ='0';
        }

        $employeeAcademic->update();

        return redirect()->route('employeeAcademic.index')->with('success', 'Status updated successfully.');
    }
}
