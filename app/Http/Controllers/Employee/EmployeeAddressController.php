<?php

namespace App\Http\Controllers\Employee;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeAddress;
use App\Http\Controllers\Controller;
use App\Models\Country;

class EmployeeAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeAddresses  = EmployeeAddress::with('employee','prCountry','paCountry')->paginate(10);
        $employee           = Employee::pluck('name','id');
        $country            = Country::pluck('name','id');
        return view('dashboard.employee.employeeAddress.index', compact('employeeAddresses','employee','country'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'           => ['required','integer'],
            'pr_address_line_one'   => ['required','string'],
            'pr_address_line_two'   => ['required','string'],
            'pr_phone_one'          => ['required','integer'],
            'pr_phone_two'          => ['required','integer'],
            'pr_email'              => ['required','string'],
            'pr_village'            => ['required','string'],
            'pr_police_station'     => ['required','string'],
            'pr_post_office'        => ['required','string'],
            'pr_city'               => ['required','string'],
            'pr_country_id'         => ['required','integer'],
            'pa_address_line_one'   => ['required','string'],
            'pa_address_line_two'   => ['required','string'],
            'pa_phone_one'          => ['required','integer'],
            'pa_phone_two'          => ['required','integer'],
            'pa_email'              => ['required','string'],
            'pa_village'            => ['required','string'],
            'pa_police_station'     => ['required','string'],
            'pa_post_office'        => ['required','string'],
            'pa_city'               => ['required','string'],
            'pa_country_id'         => ['required','integer'],
        ]);

        EmployeeAddress::create($validated);

        return redirect()->back()->with('success', 'EmployeeAddress Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeAddress  $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAddress $employeeAddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeAddress  $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAddress $employeeAddress)
    {
        $employee           = Employee::pluck('name','id');
        $country            = Country::pluck('name','id');
        return view('dashboard.employee.employeeAddress.edit', compact('employeeAddress','employee','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeAddress  $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAddress $employeeAddress)
    {
        $validated = $request->validate([
            'employee_id'           => ['required','integer'],
            'pr_address_line_one'   => ['required','string'],
            'pr_address_line_two'   => ['required','string'],
            'pr_phone_one'          => ['required','integer'],
            'pr_phone_two'          => ['required','integer'],
            'pr_email'              => ['required','string'],
            'pr_village'            => ['required','string'],
            'pr_police_station'     => ['required','string'],
            'pr_post_office'        => ['required','string'],
            'pr_city'               => ['required','string'],
            'pr_country_id'         => ['required','integer'],
            'pa_address_line_one'   => ['required','string'],
            'pa_address_line_two'   => ['required','string'],
            'pa_phone_one'          => ['required','integer'],
            'pa_phone_two'          => ['required','integer'],
            'pa_email'              => ['required','string'],
            'pa_village'            => ['required','string'],
            'pa_police_station'     => ['required','string'],
            'pa_post_office'        => ['required','string'],
            'pa_city'               => ['required','string'],
            'pa_country_id'         => ['required','integer'],
        ]);

        $employeeAddress->update($validated);

        return redirect()->route('employeeAddress.index')->with('success', 'EmployeeAddress updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeAddress  $employeeAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAddress $employeeAddress)
    {
        $employeeAddress->delete();
        return back()->with('delete', 'EmployeeAddress deleted successfully.');
    }
}
