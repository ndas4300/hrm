<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\AcademicResult;
use App\Models\BloodGroup;
use App\Models\Calendar;
use App\Models\Country;
use App\Models\Day;
use App\Models\Department;
use App\Models\Designation;
use App\Models\EarnLeave;
use App\Models\Education;
use App\Models\Employee;
use App\Models\EmployeeAcademic;
use App\Models\EmployeeAddress;
use App\Models\EmployeeExperience;
use App\Models\EmployeeOfficial;
use App\Models\EmployeeStatus;
use App\Models\EmployeeTraining;
use App\Models\EmployeeType;
use App\Models\Floor;
use App\Models\Gender;
use App\Models\Grade;
use App\Models\LeaveCategory;
use App\Models\Line;
use App\Models\MaritialStatus;
use App\Models\ProfessionalCertificate;
use App\Models\Religion;
use App\Models\Section;
use App\Models\Shift;
use App\Models\Status;
use App\Models\Transport;
use App\Models\Unit;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::with('status')->paginate(10);
        return view('dashboard.employee.employee.index',compact('employees'));
    }

    public function create()
    {
        $genders        = Gender::pluck('name','id');
        $maritialStatus = MaritialStatus::pluck('name','id');
        $bloodGroup     = BloodGroup::pluck('name','id');
        $religion       = Religion::pluck('name','id');
        $status         = Status::pluck('name','id');
        $country        = Country::pluck('name','id');
        $unit           = Unit::pluck('name', 'id');
        $floor          = Floor::pluck('name', 'id');
        $section        = Section::pluck('name', 'id');
        $department     = Department::pluck('name', 'id');
        $line           = Line::pluck('name', 'id');
        $designation    = Designation::pluck('name', 'id');
        $employeeType   = EmployeeType::pluck('name', 'id');
        $grade          = Grade::pluck('name', 'id');
        $shift          = Shift::pluck('name', 'id');
        $calendar       = Calendar::pluck('name', 'id');
        $leaveCategory  = LeaveCategory::pluck('name', 'id');
        $transport      = Transport::pluck('location', 'id');
        $days           = Day::pluck('name', 'id');
        $education      = Education::pluck('name', 'id');
        $academicResult = AcademicResult::pluck('name', 'id');
        return view('dashboard.employee.employee.create', compact('genders','maritialStatus',
            'bloodGroup','religion','status','country','unit','floor','section','department','line','designation',
            'employeeType','grade','shift','calendar','leaveCategory','transport','days','education','academicResult'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'employee_no'          => ['required', 'string'],
            'name'                 => ['required', 'string','max:255'],
            'bn_name'              => ['required', 'string'],
            'father'               => ['string'],
            'mother'               => ['string'],
            'gender_id'            => ['required', 'string'],
            'dob'                  => ['required', 'date'],
            'maritial_status_id'   => ['required', 'integer'],
            'spouse'               => [ 'string'],
            'blood_group_id'       => ['required', 'integer'],
            'religion_id'          => ['required', 'integer'],
            'nid'                  => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'passport'             => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'driving_license'      => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'image'                => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'status_id'            => ['required', 'integer'],
        ]);

        $validated['nid'] = uploadFile($request->file('nid'), 'employee/nid');
        $validated['passport'] = uploadFile($request->file('passport'), 'employee/passport');
        $validated['driving_license'] = uploadFile($request->file('driving_license'), 'employee/driving_license');
        $validated['image'] = uploadFile($request->file('image'), 'employee/image');

        $employee = Employee::create($validated);

        $id=$employee->id;

        $employee->days()->attach($request->day_id);
//        dd($id );

        $validated2 = $request->validate([
            'training_title'        => ['required','string'],
            'topics_covered'        => ['required','string'],
            'training_institute'    => ['required','string'],
            'training_year'         => ['required','integer'],
            'training_duration'     => ['required','integer'],
            'training_location'     => ['required','string'],
            'country_id'            => ['required','integer'],
        ]);
        $validated2['employee_id'] = $id;

        EmployeeTraining::create($validated2);

        $validated3 = $request->validate([
            'unit_id'                   => ['required', 'integer'],
            'floor_id'                  => ['required', 'integer'],
            'section_id'                => ['required', 'integer'],
            'department_id'             => ['required', 'integer'],
            'line_id'                   => ['required', 'integer'],
            'designation_id'            => ['required', 'integer'],
            'employee_type_id'          => ['required', 'integer'],
            'grade_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'calendar_id'               => ['required', 'integer'],
            'joining_date'              => ['required', 'date'],
            'leave_category_id'         => ['required', 'integer'],
            'gross'                     => ['required', 'integer'],
            'fixed'                     => ['required', 'integer'],
            'transport_id'              => ['required', 'integer'],
            'transport_amount'          => ['required', 'integer'],
            'medical_amount'            => ['required', 'integer'],
            'house_rent'                => ['required', 'integer'],
            'other_amount'              => ['required', 'integer'],
        ]);
        $validated3['employee_id'] =$id;
        EmployeeOfficial::create($validated3);

        $validated4 = $request->validate([
            'pr_address_line_one'   => ['required','string'],
            'pr_address_line_two'   => ['required','string'],
            'pr_phone_one'          => ['required','integer'],
            'pr_phone_two'          => ['required','integer'],
            'pr_email'              => ['required','string'],
            'pr_village'            => ['required','string'],
            'pr_police_station'     => ['required','string'],
            'pr_post_office'        => ['required','string'],
            'pr_city'               => ['required','string'],
            'pr_country_id'         => ['required','integer'],
            'pa_address_line_one'   => ['required','string'],
            'pa_address_line_two'   => ['required','string'],
            'pa_phone_one'          => ['required','integer'],
            'pa_phone_two'          => ['required','integer'],
            'pa_email'              => ['required','string'],
            'pa_village'            => ['required','string'],
            'pa_police_station'     => ['required','string'],
            'pa_post_office'        => ['required','string'],
            'pa_city'               => ['required','string'],
            'pa_country_id'         => ['required','integer'],
        ]);
        $validated4['employee_id'] =$id;
        EmployeeAddress::create($validated4);


        $validated5 = $request->validate([
            'company'               => ['required','string'],
            'business'              => ['required','string'],
            'designation'           => ['required','string'],
            'area_of_experience'    => ['required','string'],
            'experience_location'   => ['required','string'],
            'experience_start'      => ['required','date'],
            'experience_end'        => ['required','date'],
        ]);
        $validated5['employee_id'] =$id;
        EmployeeExperience::create($validated5);

        $validated6 = $request->validate([
            'status_id'     => ['required', 'integer'],
            'date'          => ['required', 'date'],
            'description'   => ['required', 'string'],

        ]);
        $validated6['employee_id'] =$id;
        EmployeeStatus::create($validated6);

        $validated7 = $request->validate([
            'certification'             => ['required','string'],
            'certification_institute'   => ['required','string'],
            'certification_location'    => ['required','string'],
            'certification_start'       => ['required','date'],
            'certification_end'         => ['required','date'],
        ]);
        $validated7['employee_id'] =$id;
        ProfessionalCertificate::create($validated7);

        $validated8 = $request->validate([
            'education_id'        => ['required','integer'],
            'degree'              => ['required','string'],
            'major'               => ['required','string'],
            'institute'           => ['required','string'],
            'academic_result_id'  => ['required','integer'],
            'marks'               => ['required','integer'],
            'year'                => ['required','integer'],
            'duration'            => ['required','integer'],
            'achievement'         => ['required','string'],
        ]);
        $validated8['employee_id'] =$id;
        EmployeeAcademic::create($validated8);

        $validated9 = $request->validate([
            'date'              => ['required', 'date'],
            'balance'           => ['required', 'integer'],
            'previous_balance'  => ['required', 'integer'],
            'next_schedule'     => ['required', 'date'],
        ]);
        $validated9['employee_id'] =$id;
        EarnLeave::create($validated9);

        return redirect()->route('employee.index')->with('success', 'Employee Created Successfully.');
    }

    public function edit(Employee $employee)
    {
        $training               =EmployeeTraining::where('employee_id',$employee->id)->first();
        $official               =EmployeeOfficial::where('employee_id',$employee->id)->first();
        $address                =EmployeeAddress::where('employee_id',$employee->id)->first();
        $experience             =EmployeeExperience::where('employee_id',$employee->id)->first();
        $eStatus                =EmployeeStatus::where('employee_id',$employee->id)->first();
        $certificate            =ProfessionalCertificate::where('employee_id',$employee->id)->first();
    //    dd($status);

        $genders        = Gender::pluck('name','id');
        $maritialStatus = MaritialStatus::pluck('name','id');
        $bloodGroup     = BloodGroup::pluck('name','id');
        $religion       = Religion::pluck('name','id');
        $status         = Status::pluck('name','id');
        $country        = Country::pluck('name','id');
        $unit           = Unit::pluck('name', 'id');
        $floor          = Floor::pluck('name', 'id');
        $section        = Section::pluck('name', 'id');
        $department     = Department::pluck('name', 'id');
        $line           = Line::pluck('name', 'id');
        $designation    = Designation::pluck('name', 'id');
        $employeeType   = EmployeeType::pluck('name', 'id');
        $grade          = Grade::pluck('name', 'id');
        $shift          = Shift::pluck('name', 'id');
        $calendar       = Calendar::pluck('name', 'id');
        $leaveCategory  = LeaveCategory::pluck('name', 'id');
        $transport      = Transport::pluck('location', 'id');
        $days           = Day::pluck('name', 'id');
        $education      = Education::pluck('name', 'id');
        $academicResult = AcademicResult::pluck('name', 'id');
        return view('dashboard.employee.employee.edit', compact('employee','training',
            'official','address','experience','eStatus','certificate','genders','maritialStatus','bloodGroup',
            'religion','status','country','unit','floor','section','department','line','designation',
            'employeeType','grade','shift','calendar','leaveCategory','transport','days','education',
            'academicResult'));
    }

    public function update(Request $request, Employee $employee )
    {
        $training               = EmployeeTraining::where('employee_id',$employee->id)->first();
        $official               = EmployeeOfficial::where('employee_id',$employee->id)->first();
        $address                = EmployeeAddress::where('employee_id',$employee->id)->first();
        $experience             = EmployeeExperience::where('employee_id',$employee->id)->first();
        $status                 = EmployeeStatus::where('employee_id',$employee->id)->first();
        $certificate            = ProfessionalCertificate::where('employee_id',$employee->id)->first();
        $academic               = EmployeeAcademic::where('employee_id',$employee->id)->first();
        $earnLeave              = EarnLeave::where('employee_id',$employee->id)->first();

        $validated = $request->validate([
            'employee_no'          => ['required', 'string'],
            'name'                 => ['required', 'string','max:255'],
            'bn_name'              => ['required', 'string'],
            'father'               => ['string'],
            'mother'               => ['string'],
            'gender_id'            => ['required', 'string'],
            'dob'                  => ['required', 'date'],
            'maritial_status_id'   => ['required', 'integer'],
            'spouse'               => [ 'string'],
            'blood_group_id'       => ['required', 'integer'],
            'religion_id'          => ['required', 'integer'],
            'nid'                  => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'passport'             => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'driving_license'      => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'image'                => ['nullable', 'mimes:jpeg,png,jpg,svg,webp,pdf', 'max:1024'],
            'status_id'            => ['required', 'integer'],
        ]);
        if ($request->nid) {
            $validated['nid'] = uploadFile($request->file('nid'), 'employee/nid');
        }
        if ($request->passport) {
            $validated['passport'] = uploadFile($request->file('passport'), 'employee/passport');
        }
        if ($request->driving_license) {
            $validated['driving_license'] = uploadFile($request->file('driving_license'), 'employee/driving_license');
        }
        if ($request->image) {
            $validated['image'] = uploadFile($request->file('image'), 'employee/image');
        }

        $employee->update($validated);

        $id=$employee->id;

        $employee->days()->sync($request->day_id);

        $validated2 = $request->validate([
            'training_title'        => ['nullable','string'],
            'topics_covered'        => ['nullable','string'],
            'training_institute'    => ['nullable','string'],
            'training_year'         => ['nullable','integer'],
            'training_duration'     => ['nullable','integer'],
            'training_location'     => ['nullable','string'],
            'country_id'            => ['nullable','integer'],
        ]);
        $validated2['employee_id'] ="$id";
        $training->update($validated2);

        $validated3 = $request->validate([
            'unit_id'                   => ['required', 'integer'],
            'floor_id'                  => ['required', 'integer'],
            'section_id'                => ['required', 'integer'],
            'department_id'             => ['required', 'integer'],
            'line_id'                   => ['required', 'integer'],
            'designation_id'            => ['required', 'integer'],
            'employee_type_id'          => ['required', 'integer'],
            'grade_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'calendar_id'               => ['required', 'integer'],
            'joining_date'              => ['required', 'date'],
            'leave_category_id'         => ['required', 'integer'],
            'gross'                     => ['required', 'integer'],
            'fixed'                     => ['required', 'integer'],
            'transport_id'              => ['required', 'integer'],
            'transport_amount'          => ['required', 'integer'],
            'medical_amount'            => ['required', 'integer'],
            'house_rent'                => ['required', 'integer'],
            'other_amount'              => ['required', 'integer'],
        ]);
        $validated3['employee_id'] =$id;
        $official->update($validated3);

        $validated4 = $request->validate([
            'pr_address_line_one'   => ['required','string'],
            'pr_address_line_two'   => ['required','string'],
            'pr_phone_one'          => ['required','integer'],
            'pr_phone_two'          => ['required','integer'],
            'pr_email'              => ['required','string'],
            'pr_village'            => ['required','string'],
            'pr_police_station'     => ['required','string'],
            'pr_post_office'        => ['required','string'],
            'pr_city'               => ['required','string'],
            'pr_country_id'         => ['required','integer'],
            'pa_address_line_one'   => ['required','string'],
            'pa_address_line_two'   => ['required','string'],
            'pa_phone_one'          => ['required','integer'],
            'pa_phone_two'          => ['required','integer'],
            'pa_email'              => ['required','string'],
            'pa_village'            => ['required','string'],
            'pa_police_station'     => ['required','string'],
            'pa_post_office'        => ['required','string'],
            'pa_city'               => ['required','string'],
            'pa_country_id'         => ['required','integer'],
        ]);
        $validated4['employee_id'] =$id;
        $address->update($validated4);

        $validated5 = $request->validate([
            'company'               => ['required','string'],
            'business'              => ['required','string'],
            'designation'           => ['required','string'],
            'area_of_experience'    => ['required','string'],
            'experience_location'   => ['required','string'],
            'experience_start'      => ['required','date'],
            'experience_end'        => ['required','date'],
        ]);
        $validated5['employee_id'] =$id;
        $experience->update($validated5);

        $validated6 = $request->validate([
            'status_id'     => ['required', 'integer'],
            'date'          => ['required', 'date'],
            'description'   => ['required', 'string'],

        ]);
        $validated6['employee_id'] =$id;
        $status->update($validated6);

        $validated7 = $request->validate([
            'certification'             => ['required','string'],
            'certification_institute'   => ['required','string'],
            'certification_location'    => ['required','string'],
            'certification_start'       => ['required','date'],
            'certification_end'         => ['required','date'],
        ]);
        $validated7['employee_id'] =$id;
        $certificate->update($validated7);

        $validated8 = $request->validate([
            'education_id'        => ['required','integer'],
            'degree'              => ['required','string'],
            'major'               => ['required','string'],
            'institute'           => ['required','string'],
            'academic_result_id'  => ['required','integer'],
            'marks'               => ['required','integer'],
            'year'                => ['required','integer'],
            'duration'            => ['required','integer'],
            'achievement'         => ['required','string'],
        ]);
        $validated8['employee_id'] =$id;
        $academic->update($validated8);

        $validated9 = $request->validate([
            'date'              => ['required', 'date'],
            'balance'           => ['required', 'integer'],
            'previous_balance'  => ['required', 'integer'],
            'next_schedule'     => ['required', 'date'],
        ]);
        $validated9['employee_id'] =$id;
        $earnLeave->update($validated9);

        return redirect()->route('employee.index')->with('success', 'Employee updated successfully.');
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
        return back()->with('delete', 'Employee deleted successfully.');
    }

    public function status(Employee $employee)
    {
        if ($employee->is_active==0) {
            $employee->is_active ='1';
        }
        else{
            $employee->is_active ='0';
        }

        $employee->update();

        return redirect()->route('employee.index')->with('success', 'Status updated successfully.');
    }
}
