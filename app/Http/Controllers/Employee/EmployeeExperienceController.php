<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\EmployeeExperience;
use Illuminate\Http\Request;

class EmployeeExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeExperiences    = EmployeeExperience::with('employee')->paginate(10);
        $employee               = Employee::pluck('name','id');
        return view('dashboard.employee.employeeExperience.index', compact('employeeExperiences','employee'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'           => ['required','integer'],
            'company'               => ['required','string'],
            'business'              => ['required','string'],
            'designation'           => ['required','string'],
            'area_of_experience'    => ['required','string'],
            'experience_location'   => ['required','string'],
            'experience_start'      => ['required','date'],
            'experience_end'        => ['required','date'],
        ]);

        EmployeeExperience::create($validated);

        return redirect()->back()->with('success', 'EmployeeExperience Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeExperience  $employeeExperience
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeExperience $employeeExperience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeExperience  $employeeExperience
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeExperience $employeeExperience)
    {
        $employee   = Employee::pluck('name','id');
        return view('dashboard.employee.employeeExperience.edit', compact('employeeExperience','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeExperience  $employeeExperience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeExperience $employeeExperience)
    {
        $validated = $request->validate([
            'employee_id'           => ['required','integer'],
            'company'               => ['required','string'],
            'business'              => ['required','string'],
            'designation'           => ['required','string'],
            'area_of_experience'    => ['required','string'],
            'experience_location'   => ['required','string'],
            'experience_start'      => ['required','date'],
            'experience_end'        => ['required','date'],
        ]);

        $employeeExperience->update($validated);

        return redirect()->route('employeeExperience.index')->with('success', 'EmployeeExperience updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeExperience  $employeeExperience
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeExperience $employeeExperience)
    {
        $employeeExperience->delete();
        return back()->with('delete', 'EmployeeExperience deleted successfully.');
    }

    public function status(EmployeeExperience $employeeExperience)
    {
        if ($employeeExperience->is_active==0) {
            $employeeExperience->is_active ='1';
        }
        else{
            $employeeExperience->is_active ='0';
        }

        $employeeExperience->update();

        return redirect()->route('employeeExperience.index')->with('success', 'Status updated successfully.');
    }
}
