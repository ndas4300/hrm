<?php

namespace App\Http\Controllers\Employee;

use App\Models\Line;
use App\Models\Unit;
use App\Models\Floor;
use App\Models\Section;
use App\Models\Employee;
use App\Models\Department;
use App\Models\Designation;
use App\Models\EmployeeType;
use Illuminate\Http\Request;
use App\Models\EmployeeOfficial;
use App\Http\Controllers\Controller;
use App\Models\Calendar;
use App\Models\Grade;
use App\Models\LeaveCategory;
use App\Models\Shift;
use App\Models\Transport;

class EmployeeOfficialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeOfficials = EmployeeOfficial::with('employee')->paginate(10);
        // dd($employeeOfficials);
        return view('dashboard.employee.employeeOfficial.index', compact('employeeOfficials'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee       = Employee::pluck('name', 'id');
        $unit           = Unit::pluck('name', 'id');
        $floor          = Floor::pluck('name', 'id');
        $section        = Section::pluck('name', 'id');
        $department     = Department::pluck('name', 'id');
        $line           = Line::pluck('name', 'id');
        $designation    = Designation::pluck('name', 'id');
        $employeeType   = EmployeeType::pluck('name', 'id');
        $grade          = Grade::pluck('name', 'id');
        $shift          = Shift::pluck('name', 'id');
        $calendar       =  Calendar::pluck('name', 'id');
        $leaveCategory  = LeaveCategory::pluck('name', 'id');
        $transport      = Transport::pluck('location', 'id');
        return view('dashboard.employee.employeeOfficial.create', compact('employee','unit','floor','section','department','line','designation','employeeType','grade','shift','calendar','leaveCategory','transport'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'employee_id'               => ['required', 'integer'],
            'unit_id'                   => ['required', 'integer'],
            'floor_id'                  => ['required', 'integer'],
            'section_id'                => ['required', 'integer'],
            'department_id'             => ['required', 'integer'],
            'line_id'                   => ['required', 'integer'],
            'designation_id'            => ['required', 'integer'],
            'employee_type_id'          => ['required', 'integer'],
            'grade_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'calendar_id'               => ['required', 'integer'],
            'joining_date'              => ['required', 'date'],
            'leave_category_id'         => ['required', 'integer'],
            'gross'                     => ['required', 'integer'],
            'fixed'                     => ['required', 'integer'],
            'transport_id'              => ['required', 'integer'],
            'transport_amount'          => ['required', 'integer'],
            'medical_amount'            => ['required', 'integer'],
            'house_rent'                => ['required', 'integer'],
            'other_amount'              => ['required', 'integer'],
        ]);

        EmployeeOfficial::create($validated);

        return redirect()->back()->with('success', 'EmployeeOfficial Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeOfficial  $employeeOfficial
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeOfficial $employeeOfficial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeOfficial  $employeeOfficial
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeOfficial $employeeOfficial)
    {
        $employee       = Employee::pluck('name', 'id');
        $unit           = Unit::pluck('name', 'id');
        $floor          = Floor::pluck('name', 'id');
        $section        = Section::pluck('name', 'id');
        $department     = Department::pluck('name', 'id');
        $line           = Line::pluck('name', 'id');
        $designation    = Designation::pluck('name', 'id');
        $employeeType   = EmployeeType::pluck('name', 'id');
        $grade          = Grade::pluck('name', 'id');
        $shift          = Shift::pluck('name', 'id');
        $calendar       =  Calendar::pluck('name', 'id');
        $leaveCategory  = LeaveCategory::pluck('name', 'id');
        $transport      = Transport::pluck('location', 'id');
        return view('dashboard.employee.employeeOfficial.edit', compact('employeeOfficial', 'employee','unit','floor','section','department','line','designation','employeeType','grade','shift','calendar','leaveCategory','transport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeOfficial  $employeeOfficial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeOfficial $employeeOfficial)
    {
        $validated = $request->validate([
            'employee_id'               => ['required', 'integer'],
            'unit_id'                   => ['required', 'integer'],
            'floor_id'                  => ['required', 'integer'],
            'section_id'                => ['required', 'integer'],
            'department_id'             => ['required', 'integer'],
            'line_id'                   => ['required', 'integer'],
            'designation_id'            => ['required', 'integer'],
            'employee_type_id'          => ['required', 'integer'],
            'grade_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'shift_id'                  => ['required', 'integer'],
            'calendar_id'               => ['required', 'integer'],
            'joining_date'              => ['required', 'date'],
            'leave_category_id'         => ['required', 'integer'],
            'gross'                     => ['required', 'integer'],
            'fixed'                     => ['required', 'integer'],
            'transport_id'              => ['required', 'integer'],
            'transport_amount'          => ['required', 'integer'],
            'medical_amount'            => ['required', 'integer'],
            'house_rent'                => ['required', 'integer'],
            'other_amount'              => ['required', 'integer'],
        ]);

        $employeeOfficial->update($validated);

        return redirect()->route('employeeOfficial.index')->with('success', 'EmployeeOfficial updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeOfficial  $employeeOfficial
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeOfficial $employeeOfficial)
    {
        $employeeOfficial->delete();
        return back()->with('delete', 'EmployeeOfficial deleted successfully.');
    }
}
