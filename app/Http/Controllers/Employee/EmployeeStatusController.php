<?php

namespace App\Http\Controllers\Employee;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\EmployeeStatus;
use App\Http\Controllers\Controller;

class EmployeeStatusController extends Controller
{
    public function index()
    {
        $employeeStatuses = EmployeeStatus::with('employee')->paginate(10);
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.employeeStatus.index', compact('employeeStatuses','employee'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.employeeStatus.create');
    // }

    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'employee_id'   => ['required', 'integer'],
            'status_id'     => ['required', 'integer'],
            'date'          => ['required', 'date'],
            'description'   => ['required', 'string'],

        ]);

        EmployeeStatus::create($validated);

        return redirect()->back()->with('success', 'EmployeeStatus Created Successfully.');
    }

    public function edit(EmployeeStatus $employeeStatus)
    {
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.employeeStatus.edit', compact('employeeStatus','employee'));
    }

    public function update(Request $request, EmployeeStatus $employeeStatus)
    {
        $validated = $request->validate([
            'employee_id'   => ['required', 'integer'],
            'status_id'     => ['required', 'integer'],
            'date'          => ['required', 'date'],
            'description'   => ['required', 'string'],
        ]);

        $employeeStatus->update($validated);

        return redirect()->route('employeeStatus.index')->with('success', 'EmployeeStatus updated successfully.');
    }

    public function destroy(EmployeeStatus $employeeStatus)
    {
        $employeeStatus->delete();
        return back()->with('delete', 'EmployeeStatus deleted successfully.');
    }

    public function status(EmployeeStatus $employeeStatus)
    {
        if ($employeeStatus->is_active==0) {
            $employeeStatus->is_active ='1';
        }
        else{
            $employeeStatus->is_active ='0';
        }

        $employeeStatus->update();

        return redirect()->route('employeeStatus.index')->with('success', 'Status updated successfully.');
    }
}
