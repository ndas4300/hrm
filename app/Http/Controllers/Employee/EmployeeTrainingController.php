<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Employee;
use App\Models\EmployeeTraining;
use Illuminate\Http\Request;

class EmployeeTrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employeeTrainings = EmployeeTraining::with('employee')->paginate(10);
        // dd($employeeTrainings);
        return view('dashboard.employee.employeeTraining.index', compact('employeeTrainings'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee   = Employee::pluck('name','id');
        $country    = Country::pluck('name','id');
        return view('dashboard.employee.employeeTraining.create',compact('employee','country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'           => ['required','integer'],
            'training_title'        => ['required','string'],
            'topics_covered'        => ['required','string'],
            'training_institute'    => ['required','string'],
            'training_year'         => ['required','integer'],
            'training_duration'     => ['required','integer'],
            'training_location'     => ['required','string'],
            'country_id'            => ['required','integer'],
        ]);

        EmployeeTraining::create($validated);

        return redirect()->back()->with('success', 'EmployeeTraining Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeTraining  $employeeTraining
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeTraining $employeeTraining)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeTraining  $employeeTraining
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeTraining $employeeTraining)
    {
        $employee   = Employee::pluck('name','id');
        $country    = Country::pluck('name','id');
        return view('dashboard.employee.employeeTraining.edit', compact('employeeTraining','employee','country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeTraining  $employeeTraining
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeTraining $employeeTraining)
    {
        $validated = $request->validate([
            'employee_id'           => ['required','integer'],
            'training_title'        => ['required','string'],
            'topics_covered'        => ['required','string'],
            'training_institute'    => ['required','string'],
            'training_year'         => ['required','integer'],
            'training_duration'     => ['required','integer'],
            'training_location'     => ['required','string'],
            'country_id'            => ['required','integer'],
        ]);

        $employeeTraining->update($validated);

        return redirect()->route('employeeTraining.index')->with('success', 'EmployeeTraining updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeTraining  $employeeTraining
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeTraining $employeeTraining)
    {
        $employeeTraining->delete();
        return back()->with('delete', 'EmployeeTraining deleted successfully.');
    }
}
