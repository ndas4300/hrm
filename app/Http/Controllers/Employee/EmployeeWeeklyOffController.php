<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Day;
use App\Models\Employee;
use App\Models\EmployeeWeeklyOff;
use Illuminate\Http\Request;

class EmployeeWeeklyOffController extends Controller
{
    public function index()
    {
        $employeeWeeklyOffs = EmployeeWeeklyOff::with('employees','day')->paginate(10);
        // dd($employeeWeeklyOffs);
        $employees      =Employee::pluck('name','id');
        $days           =Day::pluck('name','id');
        return view('dashboard.employee.employeeWeeklyOff.index', compact('employeeWeeklyOffs','employees','days'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.employeeWeeklyOff.create');
    // }

    public function store(Request $request)
    {
//         dd($request->all());
//        $validated = $request->validate([
//            'employee_id'               => ['required', 'integer'],
//            'employee_weekly_off_id'    => ['required','array'],
//        ]);
        for ($i=0; $i < count($request->employee_weekly_off_id); $i++){
            EmployeeWeeklyOff::create()->employees()->attach('1')->employee_weekly_offs()->attach($request->employee_weekly_off_id[$i]);
        }


        return redirect()->back()->with('success', 'EmployeeWeeklyOff Created Successfully.');
    }

    public function edit(EmployeeWeeklyOff $employeeWeeklyOff)
    {
        // dd($employeeWeeklyOff);
        $employees  =Employee::pluck('name','id');
        $days       =Day::pluck('name','id');
        return view('dashboard.employee.employeeWeeklyOff.edit', compact('employeeWeeklyOff','employees','days'));
    }

    public function update(Request $request, EmployeeWeeklyOff $employeeWeeklyOff)
    {
        $validated = $request->validate([
            'employee_id'   => ['required', 'integer'],
            'day_id'        => ['required', 'integer'],
        ]);

        $employeeWeeklyOff->update($validated);
        $employeeWeeklyOff->employees()->sync($request->employee_id);

        return redirect()->route('employeeWeeklyOff.index')->with('success', 'EmployeeWeeklyOff updated successfully.');
    }

    public function destroy(EmployeeWeeklyOff $employeeWeeklyOff)
    {
        $employeeWeeklyOff->delete();
        return back()->with('delete', 'EmployeeWeeklyOff deleted successfully.');
    }
}
