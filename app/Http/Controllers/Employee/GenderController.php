<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Gender;
use Illuminate\Http\Request;

class GenderController extends Controller
{
    public function index()
    {
        $genders = Gender::paginate(10);
        return view('dashboard.employee.gender.index', compact('genders'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.gender.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','unique:genders', 'max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Gender::create($validated);

        return redirect()->route('gender.index')->with('success', 'Gender Created Successfully.');
    }

    public function edit(Gender $gender)
    {
        return view('dashboard.employee.gender.edit', compact('gender'));
    }

    public function update(Request $request, Gender $gender)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $gender->update($validated);

        return redirect()->route('gender.index')->with('success', 'Gender updated successfully.');
    }

    public function destroy(Gender $gender)
    {
        $gender->delete();
        return back()->with('delete', 'Gender deleted successfully.');
    }

    public function status(Gender $gender)
    {
        if ($gender->is_active==0) {
            $gender->is_active ='1';
        }
        else{
            $gender->is_active ='0';
        }

        $gender->update();

        return redirect()->route('gender.index')->with('success', 'Status updated successfully.');
    }
}
