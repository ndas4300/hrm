<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\LeaveCategory;
use Illuminate\Http\Request;

class LeaveCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leaveCategories = LeaveCategory::paginate(10);
        return view('dashboard.employee.leaveCategory.index', compact('leaveCategories'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        LeaveCategory::create($validated);

        return redirect()->route('leaveCategory.index')->with('success', 'LeaveCategory Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LeaveCategory  $leaveCategory
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveCategory $leaveCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LeaveCategory  $leaveCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveCategory $leaveCategory)
    {
        return view('dashboard.employee.leaveCategory.edit', compact('leaveCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LeaveCategory  $leaveCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveCategory $leaveCategory)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $leaveCategory->update($validated);

        return redirect()->route('leaveCategory.index')->with('success', 'LeaveCategory updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LeaveCategory  $leaveCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveCategory $leaveCategory)
    {
        $leaveCategory->delete();
        return back()->with('delete', 'LeaveCategory deleted successfully.');
    }

    public function status(LeaveCategory $leaveCategory)
    {
        if ($leaveCategory->is_active==0) {
            $leaveCategory->is_active ='1';
        }
        else{
            $leaveCategory->is_active ='0';
        }

        $leaveCategory->update();

        return redirect()->route('leaveCategory.index')->with('success', 'Status updated successfully.');
    }
}
