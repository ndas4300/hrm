<?php

namespace App\Http\Controllers\Employee;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Leave;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Models\LeaveCategory;
use App\Http\Controllers\Controller;
use App\Models\EarnLeave;

class LeaveController extends Controller
{
    public function index()
    {
        $leaves = Leave::with('employee')->paginate(10);
        $employees = Employee::pluck('name','id');
        $leaveCategories = LeaveCategory::pluck('name','id');
        $users = User::pluck('name','id');
        return view('dashboard.employee.leave.index', compact('leaves','employees','leaveCategories','users'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.leave.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'       => ['required','integer'],
            'leave_category_id' => ['required','integer'],
            'leave_from'        => ['required','date'],
            'leave_to'          => ['required','date'],
            'approved_by'       => ['required','integer'],
        ]);
        $from= new Carbon($request->leave_from);
        $to= new Carbon($request->leave_to);
        $days= $from->diffInDays($to);
        $validated['days'] = $days;
        $leave=Leave::create($validated);


        if ($leave) {
            $earnLeave=EarnLeave::where('employee_id',$request->employee_id)->orderBy('id','Desc')->first();
            if ($earnLeave){
                $remaining= $earnLeave->balance-$days;
                $earnLeave->balance= $remaining;
                $earnLeave->update();
            }
        }


        return redirect()->route('leave.index')->with('success', 'Leave Created Successfully.');
    }

    public function edit(Leave $leave)
    {
        $employees = Employee::pluck('name','id');
        $leaveCategories = LeaveCategory::pluck('name','id');
        $users = User::pluck('name','id');
        return view('dashboard.employee.leave.edit', compact('leave','employees','leaveCategories','users'));
    }

    public function update(Request $request, Leave $leave)
    {
        $validated = $request->validate([
            'employee_id'       => ['required','integer'],
            'leave_category_id' => ['required','integer'],
            'leave_from'        => ['required','date'],
            'leave_to'          => ['required','date'],
            'approved_by'       => ['required','integer'],
        ]);
        $from= new Carbon($request->leave_from);
        $to= new Carbon($request->leave_to);
        $days= $from->diffInDays($to);
        $validated['days'] = $days;
        $leave->update($validated);

        if ($leave) {
            $earnLeave=EarnLeave::where('employee_id',$request->employee_id)->orderBy('id','Desc')->first();
            if ($earnLeave){
                $remaining= $earnLeave->balance-$days;
                $earnLeave->balance= $remaining;
                $earnLeave->update();
            }
        }

        return redirect()->route('leave.index')->with('success', 'Leave updated successfully.');
    }

    public function destroy(Leave $leave)
    {
        $leave->delete();
        return back()->with('delete', 'Leave deleted successfully.');
    }

    public function status(Leave $leave)
    {
        if ($leave->is_active==0) {
            $leave->is_active ='1';
        }
        else{
            $leave->is_active ='0';
        }

        $leave->update();

        return redirect()->route('leave.index')->with('success', 'Status updated successfully.');
    }
}
