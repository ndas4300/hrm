<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\EarnLeave;
use App\Models\Employee;
use App\Models\Grade;
use App\Models\LeaveSetup;
use App\Models\LeaveCategory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeaveSetupController extends Controller
{
    public function index()
    {
        $leaveSetups = LeaveSetup::with('employee')->paginate(10);
        $grades = Grade::pluck('name','id');
        $leaveCategories = LeaveCategory::pluck('name','id');
        $users = User::pluck('name','id');
        return view('dashboard.employee.leaveSetup.index', compact('leaveSetups','grades','leaveCategories','users'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.leaveSetup.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'category_id'       => ['required','integer'],
            'leave_category_id' => ['required','integer'],
            'type'        => ['required','date'],
            'minimum_leave'          => ['required','date'],
            'per_period'       => ['required','integer'],
            'days_worked'       => ['required','integer'],
            'leave_earned'       => ['required','integer'],
            'carry_forward'       => ['required','integer'],
        ]);
        $leaveSetup=LeaveSetup::create($validated);
        return redirect()->route('leaveSetup.index')->with('success', 'LeaveSetup Created Successfully.');
    }

    public function edit(LeaveSetup $leaveSetup)
    {
        $employees = Employee::pluck('name','id');
        $leaveCategories = LeaveCategory::pluck('name','id');
        $users = User::pluck('name','id');
        return view('dashboard.employee.leaveSetup.edit', compact('leaveSetup','employees','leaveCategories','users'));
    }

    public function update(Request $request, LeaveSetup $leaveSetup)
    {
        $validated = $request->validate([
            'category_id'       => ['required','integer'],
            'leave_category_id' => ['required','integer'],
            'type'        => ['required','date'],
            'minimum_leave'          => ['required','date'],
            'per_period'       => ['required','integer'],
            'days_worked'       => ['required','integer'],
            'leave_earned'       => ['required','integer'],
            'carry_forward'       => ['required','integer'],
        ]);
        $leaveSetup->update($validated);
        return redirect()->route('leaveSetup.index')->with('success', 'LeaveSetup updated successfully.');
    }

    public function destroy(LeaveSetup $leaveSetup)
    {
        $leaveSetup->delete();
        return back()->with('delete', 'LeaveSetup deleted successfully.');
    }

    public function status(LeaveSetup $leaveSetup)
    {
        if ($leaveSetup->is_active==0) {
            $leaveSetup->is_active ='1';
        }
        else{
            $leaveSetup->is_active ='0';
        }

        $leaveSetup->update();

        return redirect()->route('leaveSetup.index')->with('success', 'Status updated successfully.');
    }
}
