<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\MaritialStatus;
use Illuminate\Http\Request;

class MaritialStatusController extends Controller
{
    public function index()
    {
        $maritialStatuses = MaritialStatus::paginate(10);
        return view('dashboard.employee.maritialStatus.index', compact('maritialStatuses'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.maritialStatus.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        MaritialStatus::create($validated);

        return redirect()->route('maritialStatus.index')->with('success', 'MaritialStatus Created Successfully.');
    }

    public function edit(MaritialStatus $maritialStatus)
    {
        return view('dashboard.employee.maritialStatus.edit', compact('maritialStatus'));
    }

    public function update(Request $request, MaritialStatus $maritialStatus)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $maritialStatus->update($validated);

        return redirect()->route('maritialStatus.index')->with('success', 'MaritialStatus updated successfully.');
    }

    public function destroy(MaritialStatus $maritialStatus)
    {
        $maritialStatus->delete();
        return back()->with('delete', 'MaritialStatus deleted successfully.');
    }

    public function status(MaritialStatus $maritialStatus)
    {
        if ($maritialStatus->is_active==0) {
            $maritialStatus->is_active ='1';
        }
        else{
            $maritialStatus->is_active ='0';
        }

        $maritialStatus->update();

        return redirect()->route('maritialStatus.index')->with('success', 'Status updated successfully.');
    }
}
