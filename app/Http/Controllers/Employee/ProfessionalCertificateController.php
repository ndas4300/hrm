<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\ProfessionalCertificate;
use Illuminate\Http\Request;

class ProfessionalCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professionalCertificates = ProfessionalCertificate::with('employee')->paginate(10);
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.professionalCertificate.index', compact('professionalCertificates','employee'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'employee_id'               => ['required', 'integer'],
            'certification'             => ['required','string'],
            'certification_institute'   => ['required','string'],
            'certification_location'    => ['required','string'],
            'certification_start'       => ['required','date'],
            'certification_end'         => ['required','date'],
        ]);

        ProfessionalCertificate::create($validated);

        return redirect()->route('professionalCertificate.index')->with('success', 'ProfessionalCertificate Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProfessionalCertificate  $professionalCertificate
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalCertificate $professionalCertificate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProfessionalCertificate  $professionalCertificate
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalCertificate $professionalCertificate)
    {
        $employee = Employee::pluck('name','id');
        return view('dashboard.employee.professionalCertificate.edit', compact('professionalCertificate','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProfessionalCertificate  $professionalCertificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfessionalCertificate $professionalCertificate)
    {
        $validated = $request->validate([
            'employee_id'               => ['required', 'integer'],
            'certification'             => ['required', 'string'],
            'certification_institute'   => ['required', 'string'],
            'certification_location'    => ['required', 'string'],
            'certification_start'       => ['required', 'date'],
            'certification_end'         => ['required', 'date'],
        ]);

        $professionalCertificate->update($validated);

        return redirect()->route('professionalCertificate.index')->with('success', 'ProfessionalCertificate updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProfessionalCertificate  $professionalCertificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalCertificate $professionalCertificate)
    {
        $professionalCertificate->delete();
        return back()->with('delete', 'ProfessionalCertificate deleted successfully.');
    }
}
