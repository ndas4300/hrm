<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Religion;
use Illuminate\Http\Request;

class ReligionController extends Controller
{
    public function index()
    {
        $religions = Religion::paginate(10);
        return view('dashboard.employee.religion.index', compact('religions'));
    }

    // public function create()
    // {
    //     return view('dashboard.employee.religion.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Religion::create($validated);

        return redirect()->route('religion.index')->with('success', 'Religion Created Successfully.');
    }

    public function edit(Religion $religion)
    {
        return view('dashboard.employee.religion.edit', compact('religion'));
    }

    public function update(Request $request, Religion $religion)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $religion->update($validated);

        return redirect()->route('religion.index')->with('success', 'Religion updated successfully.');
    }

    public function destroy(Religion $religion)
    {
        $religion->delete();
        return back()->with('delete', 'Religion deleted successfully.');
    }

    public function status(Religion $religion)
    {
        if ($religion->is_active==0) {
            $religion->is_active ='1';
        }
        else{
            $religion->is_active ='0';
        }

        $religion->update();

        return redirect()->route('religion.index')->with('success', 'Status updated successfully.');
    }
}
