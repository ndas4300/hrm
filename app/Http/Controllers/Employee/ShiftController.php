<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Shift;
use Illuminate\Http\Request;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shifts = Shift::paginate(10);
        return view('dashboard.employee.shift.index', compact('shifts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'           => ['required', 'string', 'unique:shifts', 'max:255'],
            'in'             => ['required'],
            'out'            => ['required'],
            'grace'          => ['required', 'integer'],
            'description'    => ['nullable', 'string'],
        ]);

        Shift::create($validated);

        return redirect()->route('shift.index')->with('success', 'Shift Created Successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Models\Shift $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Shift $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(Shift $shift)
    {
        return view('dashboard.employee.shift.edit', compact('shift'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Shift $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shift $shift)
    {
        $validated = $request->validate([
            'name'           => ['required', 'string', 'unique:shifts', 'max:255'],
            'in'             => ['required'],
            'out'            => ['required'],
            'grace'          => ['required', 'integer'],
            'description'    => ['nullable', 'string'],
        ]);

        $shift->update($validated);

        return redirect()->route('shift.index')->with('success', 'Shift updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Shift $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        $shift->delete();
        return back()->with('delete', 'Shift deleted successfully.');
    }

    public function status(Shift $shift)
    {
        if ($shift->is_active == 0) {
            $shift->is_active = '1';
        } else {
            $shift->is_active = '0';
        }

        $shift->update();

        return redirect()->route('shift.index')->with('success', 'Status updated successfully.');
    }
}
