<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Attendance;
use Illuminate\Http\Request;

class ManualController extends Controller
{
    public function manualIn()
    {
        return view('dashboard.pages.manualInTime');
    }

    public function searchIn(Request $request)
    {
        $trimData=trim($request->employee," ");
        $employee=explode(',',$trimData);
        $employees=Employee::whereIn('employee_no',$employee)->pluck('id');
        $attendances=Attendance::whereIn('employee_id',$employees)->where('date',$request->date)->get();
        return view('dashboard.pages.manualInTime',compact('attendances'));
    }

    public function storeIn(Request $request)
    {
        for ($i=0; $i < count($request->attendance_id); $i++) {
            $attendance=Attendance::where('id',$request->attendance_id[$i])->first();
            $attendance['manual_in_time']= $request->manual_in_time[$i];
            $attendance->update();
        }
        return redirect()->route('manualIn')->with('success', 'Employee In Time Updated Successfully.');

    }

    public function manualOut()
    {
        return view('dashboard.pages.manualOutTime');
    }

    public function searchOut(Request $request)
    {
        $trimData=trim($request->employee," ");
        $employee=explode(',',$trimData);
        $employees=Employee::whereIn('employee_no',$employee)->pluck('id');
        $attendances=Attendance::whereIn('employee_id',$employees)->where('date',$request->date)->get();
        return view('dashboard.pages.manualOutTime',compact('attendances'));
    }

    public function storeOut(Request $request)
    {
        for ($i=0; $i < count($request->attendance_id); $i++) {
            $attendance=Attendance::where('id',$request->attendance_id[$i])->first();
            $attendance['manual_out_time']= $request->manual_out_time[$i];
            $attendance->update();
        }
        return redirect()->route('manualOut')->with('success', 'Employee Out Time Updated Successfully.');

    }
}
