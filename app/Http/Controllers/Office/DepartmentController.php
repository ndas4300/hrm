<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::paginate(10);
        return view('dashboard.office.department.index', compact('departments'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.department.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Department::create($validated);

        return redirect()->route('department.index')->with('success', 'Department Created Successfully.');
    }

    public function edit(Department $department)
    {
        return view('dashboard.office.department.edit', compact('department'));
    }

    public function update(Request $request, Department $department)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $department->update($validated);

        return redirect()->route('department.index')->with('success', 'Department updated successfully.');
    }

    public function destroy(Department $department)
    {
        $department->delete();
        return back()->with('delete', 'Department deleted successfully.');
    }

    public function status(Department $department)
    {
        if ($department->is_active==0) {
            $department->is_active ='1';
        }
        else{
            $department->is_active ='0';
        }

        $department->update();

        return redirect()->route('department.index')->with('success', 'Status updated successfully.');
    }
}
