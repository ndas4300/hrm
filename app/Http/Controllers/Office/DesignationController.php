<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function index()
    {
        $designations = Designation::paginate(10);
        return view('dashboard.office.designation.index', compact('designations'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.designation.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Designation::create($validated);

        return redirect()->route('designation.index')->with('success', 'Designation Created Successfully.');
    }

    public function edit(Designation $designation)
    {
        return view('dashboard.office.designation.edit', compact('designation'));
    }

    public function update(Request $request, Designation $designation)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $designation->update($validated);

        return redirect()->route('designation.index')->with('success', 'Designation updated successfully.');
    }

    public function destroy(Designation $designation)
    {
        $designation->delete();
        return back()->with('delete', 'Designation deleted successfully.');
    }

    public function status(Designation $designation)
    {
        if ($designation->is_active==0) {
            $designation->is_active ='1';
        }
        else{
            $designation->is_active ='0';
        }

        $designation->update();

        return redirect()->route('designation.index')->with('success', 'Status updated successfully.');
    }
}
