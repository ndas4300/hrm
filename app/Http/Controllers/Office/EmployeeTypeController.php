<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\EmployeeType;
use Illuminate\Http\Request;

class EmployeeTypeController extends Controller
{
    public function index()
    {
        $employeeTypes = EmployeeType::paginate(10);
        return view('dashboard.office.employeeType.index', compact('employeeTypes'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.employeeType.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        EmployeeType::create($validated);

        return redirect()->route('employeeType.index')->with('success', 'EmployeeType Created Successfully.');
    }

    public function edit(EmployeeType $employeeType)
    {
        return view('dashboard.office.employeeType.edit', compact('employeeType'));
    }

    public function update(Request $request, EmployeeType $employeeType)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $employeeType->update($validated);

        return redirect()->route('employeeType.index')->with('success', 'EmployeeType updated successfully.');
    }

    public function destroy(EmployeeType $employeeType)
    {
        $employeeType->delete();
        return back()->with('delete', 'EmployeeType deleted successfully.');
    }

    public function status(EmployeeType $employeeType)
    {
        if ($employeeType->is_active==0) {
            $employeeType->is_active ='1';
        }
        else{
            $employeeType->is_active ='0';
        }

        $employeeType->update();

        return redirect()->route('employeeType.index')->with('success', 'Status updated successfully.');
    }
}
