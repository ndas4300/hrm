<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Floor;
use Illuminate\Http\Request;

class FloorController extends Controller
{
    public function index()
    {
        $floors = Floor::paginate(10);
        return view('dashboard.office.floor.index', compact('floors'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.floor.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active' => ['boolean'],
        ]);

        Floor::create($validated);

        return redirect()->route('floor.index')->with('success', 'Floor Created Successfully.');
    }

    public function edit(Floor $floor)
    {
        return view('dashboard.office.floor.edit', compact('floor'));
    }

    public function update(Request $request, Floor $floor)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $floor->update($validated);

        return redirect()->route('floor.index')->with('success', 'Floor updated successfully.');
    }

    public function destroy(Floor $floor)
    {
        $floor->delete();
        return back()->with('delete', 'Floor deleted successfully.');
    }

    public function status(Floor $floor)
    {
        if ($floor->is_active==0) {
            $floor->is_active ='1';
        }
        else{
            $floor->is_active ='0';
        }

        $floor->update();

        return redirect()->route('floor.index')->with('success', 'Status updated successfully.');
    }
}
