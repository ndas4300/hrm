<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Grade;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    public function index()
    {
        $grades = Grade::paginate(10);
        return view('dashboard.office.grade.index', compact('grades'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.grade.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active' => ['boolean'],
        ]);

        Grade::create($validated);

        return redirect()->route('grade.index')->with('success', 'Grade Created Successfully.');
    }

    public function edit(Grade $grade)
    {
        return view('dashboard.office.grade.edit', compact('grade'));
    }

    public function update(Request $request, Grade $grade)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $grade->update($validated);

        return redirect()->route('grade.index')->with('success', 'Grade updated successfully.');
    }

    public function destroy(Grade $grade)
    {
        $grade->delete();
        return back()->with('delete', 'Grade deleted successfully.');
    }

    public function status(Grade $grade)
    {
        if ($grade->is_active==0) {
            $grade->is_active ='1';
        }
        else{
            $grade->is_active ='0';
        }

        $grade->update();

        return redirect()->route('grade.index')->with('success', 'Status updated successfully.');
    }
}
