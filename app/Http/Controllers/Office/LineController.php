<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Line;
use Illuminate\Http\Request;

class LineController extends Controller
{
    public function index()
    {
        $lines = Line::paginate(10);
        return view('dashboard.office.line.index', compact('lines'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.line.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active' => ['boolean'],
        ]);
        // dd($validated);
        Line::create($validated);

        return redirect()->route('line.index')->with('success', 'Line Created Successfully.');
    }

    public function edit(Line $line)
    {
        return view('dashboard.office.line.edit', compact('line'));
    }

    public function update(Request $request, Line $line)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }
        $line->update($validated);

        return redirect()->route('line.index')->with('success', 'Line updated successfully.');
    }

    public function destroy(Line $line)
    {
        $line->delete();
        return back()->with('delete', 'Line deleted successfully.');
    }

    public function status(Line $line)
    {
        if ($line->is_active==0) {
            $line->is_active ='1';
        }
        else{
            $line->is_active ='0';
        }

        $line->update();

        return redirect()->route('line.index')->with('success', 'Status updated successfully.');
    }
}
