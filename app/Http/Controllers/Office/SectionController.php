<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function index()
    {
        $sections = Section::paginate(10);
        return view('dashboard.office.section.index', compact('sections'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.section.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active' => ['boolean'],
        ]);

        Section::create($validated);

        return redirect()->route('section.index')->with('success', 'Section Created Successfully.');
    }

    public function edit(Section $section)
    {
        return view('dashboard.office.section.edit', compact('section'));
    }

    public function update(Request $request, Section $section)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        $section->update($validated);

        return redirect()->route('section.index')->with('success', 'Section updated successfully.');
    }

    public function destroy(Section $section)
    {
        $section->delete();
        return back()->with('delete', 'Section deleted successfully.');
    }

    public function status(Section $section)
    {
        if ($section->is_active==0) {
            $section->is_active ='1';
        }
        else{
            $section->is_active ='0';
        }

        $section->update();

        return redirect()->route('section.index')->with('success', 'Status updated successfully.');
    }
}
