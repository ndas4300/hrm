<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use App\Models\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index()
    {
        $units = Unit::paginate(10);
        return view('dashboard.office.unit.index', compact('units'));
    }

    // public function create()
    // {
    //     return view('dashboard.office.unit.create');
    // }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        Unit::create($validated);

        return redirect()->route('unit.index')->with('success', 'Unit Created Successfully.');
    }

    public function edit(Unit $unit)
    {
        return view('dashboard.office.unit.edit', compact('unit'));
    }

    public function update(Request $request, Unit $unit)
    {
        $validated = $request->validate([
            'name'          => ['required', 'string','max:255'],
            'description'   => ['string','nullable'],
            'is_active'     => ['boolean'],
        ]);

        if ($request->is_active==null) {
            $validated['is_active'] = '0';
        }

        $unit->update($validated);

        return redirect()->route('unit.index')->with('success', 'Unit updated successfully.');
    }

    public function destroy(Unit $unit)
    {
        $unit->delete();
        return back()->with('delete', 'Unit deleted successfully.');
    }

    public function status(Unit $unit)
    {
        switch ($unit->is_active) {
            case '0':
                $unit->is_active = '1';
                break;
            default:
                $unit->is_active = '0';
                break;
        }
        $unit->update();

        return redirect()->route('unit.index')->with('success', 'Status updated successfully.');
    }
}
