<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RawAttendance;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\Attendance;
use App\Models\Department;
use App\Models\Employee;
use App\Models\Line;
use App\Models\Section;
use Barryvdh\DomPDF\Facade\Pdf;


class ReportController extends Controller
{

    protected function createpdf($vw,$ppdf){

    }
    public function get_monthlyview(){

        $year = Attendance::query()
            ->select(DB::raw('year(date) as year'))
            ->groupBy(DB::raw('year(date)'))
            ->pluck('year')
            ->toArray();
        $employee = Employee::query()->pluck('name','id');

        $month = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        ];
        $qr =null;
        return view('dashboard.report.monthlyreport',compact('year','month','employee','qr'));
    }



    Public function get_monthdata(Request $request){
        $year = Attendance::query()
            ->select(DB::raw('year(date) as year'))
            ->groupBy(DB::raw('year(date)'))
            ->pluck('year')
            ->toArray();
        $employee = Employee::query()->pluck('name','id');
        $month = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        ];

        $employee_data = Employee::where('id',$request->employee_id)->get();
        $attendence = Attendance::whereYear('date',$request->year)->whereMonth('date',$request->month)->where('employee_id',$request->employee_id)->get();
        $qr =[
            'emp_id'=>$request->employee_id,
            'year'=>$request->year,
            'month'=>$request->month
        ];
        return view('dashboard.report.monthlyreport',compact('year','month','employee','employee_data','attendence','qr'));
    }



    public function get_monthlypdf(Request $request){
        $employee_data = Employee::where('id',$request->attn['emp_id'])->get();
        $attendence = Attendance::whereYear('date',$request->attn['year'])->whereMonth('date',$request->attn['month'])->where('employee_id',$request->attn['emp_id'])->get();

        view()->share('employee_data', $employee_data);
        view()->share('attendence', $attendence);
        $pdf = PDF::loadView('dashboard.report.pdf.monthlypdf')->setPaper('a4', 'portrait');
        return $pdf->download('monthly_report' . date('ymdhis') . '.pdf');
    }



    public function getemp_id(Request $request){
        Log::info($request->value);
    }




    public function get_datewiseview(){
        $qr =null;
        return view('dashboard.report.datelyview',compact('qr'));
    }



    public function get_datewiseedata(Request $request){

            $date = date('Y-m-d', strtotime($request->date));
            $attendence = Attendance::where('date',$date)->get();
            $qr =[
                'date'=>$request->date
            ];
            return view('dashboard.report.datelyview',compact('attendence','qr'));
    }




    public function get_datelypdf(Request $request){
        $attendence = Attendance::where('date',$request->attn['date'])->get();

        view()->share('attendence', $attendence);
        $pdf = PDF::loadView('dashboard.report.pdf.datelypdf')->setPaper('a4', 'portrait');
        return $pdf->download('dately_report' . date('ymdhis') . '.pdf');
    }




    public function get_departmentwiseview()
    {
        $qr =null;
        $department = Department::query()->pluck('name','id');
        $section = Section::query()->pluck('name','id');
        return view('dashboard.report.departmentview',compact('department','section','qr'));
    }



    public function get_departmentwisedata(Request $request){

        $date = date('Y-m-d', strtotime($request->date));

            $lines = Attendance::query()
            ->where('department_id',$request->department)
            ->where('section_id',$request->section)
            ->where('date',$date)
            ->orderBy('line_id','asc')
            ->get()
            ->groupBy('line_id');
               $department = Department::query()->pluck('name','id');
               $section = Section::query()->pluck('name','id');
               $qr =[
                   'date'=>$request->date,
                   'department'=>$request->department,
                   'section'=>$request->section,
                ];
               return view('dashboard.report.departmentview',compact('lines','department','section','qr'));
        }



    public function get_departmentwisepdf(Request $request){
            $lines = Attendance::query()
            ->where('department_id',$request->attn['department'])
            ->where('section_id',$request->attn['section'])
            ->where('date',$request->attn['date'])
            ->orderBy('line_id','asc')
            ->get()
            ->groupBy('line_id');

            view()->share('lines', $lines);
            $pdf = PDF::loadView('dashboard.report.pdf.departmentpdf')->setPaper('a4', 'portrait');
            return $pdf->download('department_wise_attendance' . date('ymdhis') . '.pdf');
        }


    public function get_datewiselateview(){
        $qr =null;
        return view('dashboard.report.datewiselateview',compact('qr'));
    }



    public function get_datewiselatedata(Request $request){

        $date = date('Y-m-d', strtotime($request->date));
        $attendence = Attendance::where('date',$date)->where('attendance_status_id',3)->get();
        $qr =[
            'date'=>$request->date
        ];
        return view('dashboard.report.datewiselateview',compact('attendence','qr'));
    }



    public function get_datewiselatepdf(Request $request){
        $attendence = Attendance::where('date',$request->attn['date'])->get();

        view()->share('attendence', $attendence);
        $pdf = PDF::loadView('dashboard.report.pdf.datewiselatepdf')->setPaper('a4', 'portrait');
        return $pdf->download('date_wise_late_attendance' . date('ymdhis') . '.pdf');
    }



    public function get_dailyouttimeview()
    {
        $department = Department::query()->pluck('name','id');
        $section = Section::query()->pluck('name','id');
        $line = Line::query()->pluck('name','id');
        $qr =null;
        return view('dashboard.report.dailyoutview',compact('department','section','line','qr'));
    }



    public function get_dailyouttimedata(Request $request){
        $department = Department::query()->pluck('name','id');
        $section = Section::query()->pluck('name','id');
        $line = Line::query()->pluck('name','id');
        $attendance = Attendance::query()->where('department_id',$request->department)->where('section_id',$request->section)->where('line_id',$request->line)->get();
        $qr =[
            'department'=>$request->department,
            'section'=>$request->section,
            'line'=>$request->line
        ];
        return view('dashboard.report.dailyoutview',compact('department','section','line','attendance','qr'));
    }




    public function get_dailyouttimepdf(Request $request){
        $attendance = Attendance::query()->where('department_id',$request->attn['department'])->where('section_id',$request->attn['section'])->where('line_id',$request->attn['line'])->get();

        view()->share('attendance', $attendance);
        $pdf = PDF::loadView('dashboard.report.pdf.dailyoutpdf')->setPaper('a4', 'portrait');
        return $pdf->download('daily_out_time' . date('ymdhis') . '.pdf');
    }



    public function get_dailyintimeview()
    {
        $qr =null;
        $department = Department::query()->pluck('name','id');
        $section = Section::query()->pluck('name','id');
        $line = Line::query()->pluck('name','id');
        return view('dashboard.report.dailyinview',compact('department','section','line','qr')) ;
    }



    public function get_dailyintimedata(Request $request){
        $department = Department::query()->pluck('name','id');
        $section = Section::query()->pluck('name','id');
        $line = Line::query()->pluck('name','id');
        $attendance = Attendance::query()->where('department_id',$request->department)->where('section_id',$request->section)->where('line_id',$request->line)->get();
        $qr =[
            'department'=>$request->department,
            'section'=>$request->section,
            'line'=>$request->line
        ];
        return view('dashboard.report.dailyinview',compact('department','section','line','attendance','qr'));
    }



    public function get_dailyintimepdf(Request $request){
        $attendance = Attendance::query()->where('department_id',$request->attn['department'])->where('section_id',$request->attn['section'])->where('line_id',$request->attn['line'])->get();

        view()->share('attendance', $attendance);
        $pdf = PDF::loadView('dashboard.report.pdf.dailyinpdf')->setPaper('a4', 'portrait');
        return $pdf->download('daily_in_time' . date('ymdhis') . '.pdf');
    }

}
