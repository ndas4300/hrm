<?php

if (!function_exists('uploadFile')) {

    function uploadFile($file, $folder = '/'): ?string
    {
        if ($file) {
            $image_name = Rand() . '.' . $file->getClientOriginalExtension();
            return $file->storeAs($folder, $image_name, 'public');
        }
        return null;
    }
}
