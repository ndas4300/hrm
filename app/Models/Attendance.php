<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    public $fillable = [
        'employee_id',
        'date',
        'in_time',
        'out_time',
        'ot',
        'extra_ot',
        'unit_id',
        'floor_id',
        'section_id',
        'department_id',
        'line_id',
        'designation_id',
        'employee_type_id',
        'grade_id',
        'shift_id',
        'calendar_id',
        'leave_category_id',
        'weekly_off_id',
        'attendance_status_id',
    ];

    public function attendanceWeeklyOffs(){
        return $this->belongsToMany(AttendanceWeeklyOff::class);
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    public function Line(){
        return $this->belongsTo(Line::class);
    }

    public function attendancestatus(){
        return $this->hasMany(AttendanceStatus::class,'id','attendance_status_id');
    }

}
