<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceWeeklyOff extends Model
{
    use HasFactory;

    protected $fillable = [
      'day_id',
      'employee_id'
    ];

    public function attendances(){
        return $this->belongsToMany(Attendance::class);
    }
}
