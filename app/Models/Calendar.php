<?php

namespace App\Models;

use App\Models\CalendarEvent;
use App\Models\EmployeeOfficial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Calendar extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'is_active',
    ];

    public function calendarEvents()
    {
        return $this->hasMany(CalendarEvent::class);
    }
    public function employeeOfficial(){
        return $this->hasMany(EmployeeOfficial::class);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
