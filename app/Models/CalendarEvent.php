<?php

namespace App\Models;

use App\Models\Calendar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CalendarEvent extends Model
{
    use HasFactory;

    protected $dates = ['start','end'];

    protected $fillable = [
        'name',
        'description',
        'calendar_id',
        'start',
        'end',
        'is_holiday',
        'sms',
        'email',
        'is_active',
    ];

    public function calendar()
    {
        return $this->belongsTo(Calendar::class);
    }

}
