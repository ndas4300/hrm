<?php

namespace App\Models;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'number',
        'assigned',
        'note',
        'is_active',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
