<?php

namespace App\Models;

use App\Models\EmployeeTraining;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Country extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'short_code',
    ];

    public function employeeTrainings()
    {
        return $this->hasMany(EmployeeTraining::class);
    }

    public function employeeAddresses()
    {
        return $this->hasMany(EmployeeAddress::class);
    }

}
