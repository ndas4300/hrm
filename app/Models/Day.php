<?php

namespace App\Models;

use App\Models\EmployeeWeeklyOff;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Day extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'is_active',
    ];

    public function employees()
    {
        return $this->belongsToMany(Employee::class,'employee_weekly_off');
    }
}
