<?php

namespace App\Models;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EarnLeave extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'date',
        'balance',
        'previous_balance',
        'next_schedule',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
