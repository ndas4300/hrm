<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;

    protected $table ='education';

    protected $fillable = [
        'name',
        'description',
        'is_active',
    ];

    public function employeeAcademic()
    {
        return $this->hasMany(EmployeeAcademic::class);
    }

}
