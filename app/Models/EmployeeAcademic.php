<?php

namespace App\Models;

use App\Models\Education;
use App\Models\AcademicResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeAcademic extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'education_id',
        'degree',
        'major',
        'institute',
        'academic_result_id',
        'marks',
        'year',
        'duration',
        'achievement',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function education()
    {
        return $this->belongsTo(Education::class);
    }

    public function academicResult()
    {
        return $this->belongsTo(AcademicResult::class,'academic_result_id','id');
    }
}
