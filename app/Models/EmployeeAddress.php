<?php

namespace App\Models;

use App\Models\Country;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'pr_address_line_one',
        'pr_address_line_two',
        'pr_phone_one',
        'pr_phone_two',
        'pr_email',
        'pr_village',
        'pr_police_station',
        'pr_post_office',
        'pr_city',
        'pr_country_id',
        'pa_address_line_one',
        'pa_address_line_two',
        'pa_phone_one',
        'pa_phone_two',
        'pa_email',
        'pa_village',
        'pa_police_station',
        'pa_post_office',
        'pa_city',
        'pa_country_id',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function prCountry()
    {
        return $this->belongsTo(Country::class,'pr_country_id','id');
    }

    public function paCountry()
    {
        return $this->belongsTo(Country::class,'pr_country_id','id');
    }
}
