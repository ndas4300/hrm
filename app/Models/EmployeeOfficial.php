<?php

namespace App\Models;

use App\Models\Line;
use App\Models\Floor;
use App\Models\Grade;
use App\Models\Shift;
use App\Models\Section;
use App\Models\Calendar;
use App\Models\Employee;
use App\Models\Transport;
use App\Models\Department;
use App\Models\Designation;
use App\Models\EmployeeType;
use App\Models\LeaveCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeOfficial extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'unit_id',
        'floor_id',
        'section_id',
        'department_id',
        'line_id',
        'designation_id',
        'employee_type_id',
        'grade_id',
        'shift_id',
        'shift_group_id',
        'calendar_id',
        'joining_date',
        'leave_category_id',
        'gross',
        'fixed',
        'transport_id',
        'transport_amount',
        'medical_amount',
        'house_rent',
        'other_amount',
    ];
    public function employee(){
        return $this->belongsTo(Employee::class);
    }
    public function unit(){
        return $this->belongsTo(Unit::class);
    }
    public function floor(){
        return $this->belongsTo(Floor::class);
    }
    public function section(){
        return $this->belongsTo(Section::class);
    }
    public function department(){
        return $this->belongsTo(Department::class);
    }
    public function line(){
        return $this->belongsTo(Line::class);
    }
    public function designation(){
        return $this->belongsTo(Designation::class);
    }
    public function employeeType(){
        return $this->belongsTo(EmployeeType::class);
    }
    public function grade(){
        return $this->belongsTo(Grade::class);
    }
    public function shift(){
        return $this->belongsTo(Shift::class);
    }
    public function calendar(){
        return $this->belongsTo(Calendar::class);
    }
    public function leaveCategory(){
        return $this->belongsTo(LeaveCategory::class);
    }
    public function transport(){
        return $this->belongsTo(Transport::class);
    }
}
