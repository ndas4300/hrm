<?php

namespace App\Models;

use App\Models\Country;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeTraining extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'training_title',
        'topics_covered',
        'training_institute',
        'training_year',
        'training_duration',
        'training_location',
        'country_id',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
