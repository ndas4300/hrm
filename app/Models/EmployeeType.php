<?php

namespace App\Models;

use App\Models\EmployeeOfficial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'is_active',
    ];

    public function employeeOfficial(){
        return $this->hasMany(EmployeeOfficial::class);
    }
}
