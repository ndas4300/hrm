<?php

namespace App\Models;

use App\Models\Day;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeWeeklyOff extends Model
{
    use HasFactory;


    public function employee_weekly_offs()
    {
        return $this->belongsTo(Day::class,'day_id','id');
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }
}
