<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveSetup extends Model
{
    use HasFactory;

    protected $fillable = [
        'grade_id',
        'leave_category_id',
        'type',
        'minimum_leave',
        'per_period',
        'days_worked',
        'leave_earned',
        'carry_forward',
    ];

}
