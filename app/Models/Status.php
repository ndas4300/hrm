<?php

namespace App\Models;

use App\Models\Employee;
use App\Models\EmployeeOfficial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Status extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'description',
        'is_active',
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
    public function employeeStatuses(){
        return $this->hasMany(employeeStatus::class);
    }


}
