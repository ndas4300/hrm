<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeOfficialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_officials', function (Blueprint $table) {
            $table-> id();
            $table-> bigInteger('employee_id');
            $table-> bigInteger('unit_id');
            $table-> bigInteger('floor_id');
            $table-> bigInteger('section_id');
            $table-> bigInteger('department_id');
            $table-> bigInteger('line_id');
            $table-> bigInteger('designation_id');
            $table-> bigInteger('employee_type_id');
            $table-> bigInteger('grade_id');
            $table-> bigInteger('shift_id');
            $table-> bigInteger('calendar_id');
            $table-> date('joining_date');
            $table-> bigInteger('leave_category_id')->nullable();
            $table-> double('gross');
            $table-> double('fixed');
            $table-> bigInteger('transport_id');
            $table-> double('transport_amount');
            $table-> double('medical_amount');
            $table-> double('house_rent');
            $table-> double('other_amount');
            $table-> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_officials');
    }
}
