<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_academics', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('employee_id')->constrained('employees');
            $table->bigInteger('education_id')->constrained('education');
            $table->string('degree');
            $table->string('major');
            $table->string('institute');
            $table->bigInteger('academic_result_id')->constrained('results');
            $table->integer('marks');
            $table->integer('year');
            $table->integer('duration');
            $table->string('achievement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_academics');
    }
}
