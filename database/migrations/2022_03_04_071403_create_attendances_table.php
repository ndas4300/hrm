<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->nullable();
            $table->date('date')->nullable();
            $table->time('in_time')->nullable();
            $table->time('manual_in_time')->nullable();
            $table->time('out_time')->nullable();
            $table->time('manual_out_time')->nullable();
            $table->integer('ot')->nullable();
            $table->integer('extra_ot')->nullable();
            $table->bigInteger('unit_id')->nullable();
            $table->bigInteger('floor_id')->nullable();
            $table->bigInteger('section_id')->nullable();
            $table->bigInteger('department_id')->nullable();
            $table->bigInteger('line_id')->nullable();
            $table->bigInteger('designation_id')->nullable();
            $table->bigInteger('employee_type_id')->nullable();
            $table->bigInteger('grade_id')->nullable();
            $table->bigInteger('shift_id')->nullable();
            $table->bigInteger('calendar_id')->nullable();
            $table->bigInteger('leave_category_id')->nullable();
            $table->boolean('weekly_off_id')->nullable();
            $table->bigInteger('attendance_status_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
