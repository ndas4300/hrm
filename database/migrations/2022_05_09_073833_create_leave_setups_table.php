<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_setups', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('grade_id');
            $table->bigInteger('leave_category_id');
            $table->enum('type' ,['fixed','calc']);
            $table->integer('minimum_leave');
            $table->integer('per_period');
            $table->integer('days_worked');
            $table->integer('leave_earned');
            $table->boolean('carry_forward');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_setups');
    }
}
