<?php

namespace Database\Seeders;

use App\Models\AcademicResult;
use App\Models\BloodGroup;
use App\Models\Calendar;
use App\Models\Day;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Education;
use App\Models\EmployeeType;
use App\Models\Floor;
use App\Models\Gender;
use App\Models\Grade;
use App\Models\LeaveCategory;
use App\Models\Line;
use App\Models\MaritialStatus;
use App\Models\Religion;
use App\Models\Section;
use App\Models\Status;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createGenders();
        $this->createMaritialStatuses();
        $this->createBloodGroups();
        $this->createReligions();
        $this->createStatuses();
        $this->createDepartments();
        $this->createDesignations();
        // $this->createShifts();
        $this->createEducations();
        $this->createResults();
        $this->createCalenders();
        $this->createDays();
        $this->createLeaveCategorys();
        $this->createUnits();
        $this->createFloors();
        $this->createSections();
        $this->createLines();
        $this->createGrades();
        $this->createEmployeeTypes();
    }

    private function createGenders()
    {
        $gender=['Male','Female','Others'];
        $length=count($gender);

        for ($i=0; $i < $length ; $i++) {
            Gender::create([
                'name'              => $gender[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createMaritialStatuses()
    {
        $maritialStatus=['Single','Married','Divorced'];
        $length=count($maritialStatus);

        for ($i=0; $i < $length; $i++) {
            MaritialStatus::create([
            'name'              => $maritialStatus[0],
            'description'       => '',
            'is_active'         => '1',
        ]);
        }
    }
    private function createBloodGroups()
    {
        $bloodGroup=['A+','A-','B+','B-','O+','O-','AB+','AB-'];
        $length=count($bloodGroup);

        for ($i=0; $i < $length ; $i++) {
            BloodGroup::create([
                'name'              => $bloodGroup[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createReligions()
    {
        $religion=['Muslim','Hindu','Christan','Buddho'];
        $length=count($religion);

        for ($i=0; $i < $length ; $i++) {
            Religion::create([
                'name'              => $religion[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createStatuses()
    {
        $status=['Working','Resigned','Fired'];
        $length=count($status);

        for ($i=0; $i < $length ; $i++) {
            Status::create([
                'name'              => $status[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }

    private function createDepartments()
    {
        $department=['department1','department2','department3'];
        $length=count($department);

        for ($i=0; $i < $length ; $i++) {
            Department::create([
                'name'              => $department[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createDesignations()
    {
        $designation=['designation1','designation2','designation3'];
        $length=count($designation);

        for ($i=0; $i < $length ; $i++) {
            Designation::create([
                'name'              => $designation[$i],
                'is_active'         => '1',
            ]);
        }
    }
    // private function createShifts()
    // {
    //     $shift=['shift1','shift2','shift3'];
    //     $length=count($shift);

    //     for ($i=0; $i < $length ; $i++) {
    //         Shift::create([
    //             'name'              => $shift[$i],
    //             'description'       => '',
    //         ]);
    //     }
    // }
    private function createEducations()
    {
        $education=['education1','education2','education3'];
        $length=count($education);

        for ($i=0; $i < $length ; $i++) {
            Education::create([
                'name'              => $education[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createResults()
    {
        $result=['result1','result2','result3'];
        $length=count($result);

        for ($i=0; $i < $length ; $i++) {
            AcademicResult::create([
                'name'              => $result[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createCalenders()
    {
        $calendar=['calendar1','calendar2','calendar3'];
        $length=count($calendar);

        for ($i=0; $i < $length ; $i++) {
            Calendar::create([
                'name'              => $calendar[$i],
                'description'       => '',
                'is_active'            => '1',
            ]);
        }
    }
    private function createDays()
    {
        $day=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        $length=count($day);

        for ($i=0; $i < $length ; $i++) {
            Day::create([
                'name'              => $day[$i],
            ]);
        }
    }
    private function createLeaveCategorys()
    {
        $leaveCategory=['Leave Category 1','Leave Category 2','Leave Category 3'];
        $length=count($leaveCategory);

        for ($i=0; $i < $length ; $i++) {
            LeaveCategory::create([
                'name'              => $leaveCategory[$i],
                'description'       => '',
                'is_active'            => '1',
            ]);
        }
    }

    private function createUnits()
    {
        $unit=['unit1','unit2','unit3'];
        $length=count($unit);

        for ($i=0; $i < $length ; $i++) {
            Unit::create([
                'name'              => $unit[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createFloors()
    {
        $floor=['floor1','floor2','floor3'];
        $length=count($floor);

        for ($i=0; $i < $length ; $i++) {
            Floor::create([
                'name'              => $floor[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createSections()
    {
        $section=['section1','section2','section3'];
        $length=count($section);

        for ($i=0; $i < $length ; $i++) {
            Section::create([
                'name'              => $section[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createLines()
    {
        $line=['line1','line2','line3'];
        $length=count($line);

        for ($i=0; $i < $length ; $i++) {
            Line::create([
                'name'              => $line[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createGrades()
    {
        $grade=['grade1','grade2','grade3'];
        $length=count($grade);

        for ($i=0; $i < $length ; $i++) {
            Grade::create([
                'name'              => $grade[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }
    private function createEmployeeTypes()
    {
        $employeeType=['employeeType1','employeeType2','employeeType3'];
        $length=count($employeeType);

        for ($i=0; $i < $length ; $i++) {
            EmployeeType::create([
                'name'              => $employeeType[$i],
                'description'       => '',
                'is_active'         => '1',
            ]);
        }
    }

}
