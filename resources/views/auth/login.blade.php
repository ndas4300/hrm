@extends('layouts.app')

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="../../index2.html"><b>Admin</b>LTE</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ __('Sign in to start your session') }}</p>
{{--{{ dd($errors) }}--}}
{{--            @if ($errors->any())--}}
{{--            <div class="alert alert-danger">--}}
{{--                <ul>--}}
{{--                    <li>{{ __( 'The credentials does not match with our records' )}}</li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            @endif--}}

            {{ Form::open(['route' => 'login','method'=>'post']) }}
            <div class="input-group mb-3">
                {{ Form::text('email',null,['class'=>$errors->has('email')?'form-control  is-invalid':'form-control','placeholder'=>'Email']) }}
                <div class="input-group-append">
                    <span class="fas fa-envelope input-group-text"></span>
                </div>
            </div>
            @if($errors->has('email'))<p class="text-danger">{{ $errors->first('email') }}</p>@endif
            <div class="input-group mb-3">
                {{ Form::password('password',['class'=>$errors->has('password')?'form-control  is-invalid':'form-control','id'=>'password','placeholder'=>'Password']) }}
                <div class="input-group-append">
                    <span class=" input-group-text"><i class="far fa-eye" id="togglePassword"></i></span>
                </div>
            </div>
            @if($errors->has('password'))<p class="text-danger">{{ $errors->first('password') }}</p>@endif
            <div class="row">
                <div class="col-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Sign In') }}</button>
                </div>
            </div>
            {{ Form::close() }}
            {{-- <div class="social-auth-links text-center mb-3">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
                </a>
                <a href="#" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                </a>
            </div> --}}
            <p class="mb-1">
                <a href="{{route('password.request')}}">I forgot my password</a>
            </p>
            {{-- <p class="mb-0">
                <a href="{{route('register')}}" class="text-center">Register a new membership</a>
            </p> --}}
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });

</script>
@endpush
