@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit CalendarEvent')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit CalendarEvent')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">

                    {{ Form::model($calendarEvent, ['route' => ['calendarEvent.update', $calendarEvent->id], 'method' =>
                    'PUT', 'class' => 'form-horizontal']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name', null, ['placeholder' => 'Name','class' => 'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            {{ Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class'
                                => 'form-control', 'rows'=>'4']) }}
                                <small class="text-danger">{{ $errors->first('description') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('calendar_id') ? ' has-error' : '' }}">
                            {{ Form::label('calendar_id', 'Calendar', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('calendar_id', $calendars, null, ['id' => 'calendar_id', 'class' =>
                                'form-control']) }}
                                <small class="text-danger">{{ $errors->first('calendar_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                            {{ Form::label('start', 'Start Date', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('start', $calendarEvent->start, ['class' => 'form-control', 'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('start') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
                            {{ Form::label('end', 'End Date', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('end', $calendarEvent->end, ['class' => 'form-control']) }}
                                <small class="text-danger">{{ $errors->first('end') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label> {{ __('Email')}}</label>
                                {{ Form::checkbox('email', '1', null, ['id' => 'email']) }}
                                <label for="email"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('sms') ? ' has-error' : '' }}">
                                <label> {{ __('SMS')}}</label>
                                {{ Form::checkbox('sms', '1', null, ['id' => 'sms']) }}
                                <label for="sms"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('sms') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_holiday') ? ' has-error' : '' }}">
                                <label> {{ __('Holiday')}}</label>
                                {{ Form::checkbox('is_holiday', '1', null, ['id' => 'is_holiday']) }}
                                <label for="is_holiday"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_holiday') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label> {{ __('Status')}}</label>
                                {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                <label for="is_active"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
</section>
@endsection
