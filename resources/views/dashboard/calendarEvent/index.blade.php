@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Calendar Event')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Calendar Event')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New CalendarEvent')}}</h3>
                    </div>
                    {{ Form::open(['route' => 'calendarEvent.store']) }}
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {{ Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::text('name', null, ['placeholder' => 'Name','class' => 'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            {{ Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class'
                                => 'form-control', 'rows'=>'4']) }}
                                <small class="text-danger">{{ $errors->first('description') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('calendar_id') ? ' has-error' : '' }}">
                            {{ Form::label('calendar_id', 'Calendar', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('calendar_id', $calendars, null, ['id' => 'calendar_id', 'class' =>
                                'form-control']) }}
                                <small class="text-danger">{{ $errors->first('calendar_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('start') ? ' has-error' : '' }}">
                            {{ Form::label('start', 'Start Date', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('start', null, ['class' => 'form-control', 'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('start') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('end') ? ' has-error' : '' }}">
                            {{ Form::label('end', 'End Date', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('end', null, ['class' => 'form-control']) }}
                                <small class="text-danger">{{ $errors->first('end') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label> {{ __('Email')}}</label>
                                {{ Form::checkbox('email', '1', null, ['id' => 'email']) }}
                                <label for="email"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('sms') ? ' has-error' : '' }}">
                                <label> {{ __('SMS')}}</label>
                                {{ Form::checkbox('sms', '1', null, ['id' => 'sms']) }}
                                <label for="sms"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('sms') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_holiday') ? ' has-error' : '' }}">
                                <label> {{ __('Holiday')}}</label>
                                {{ Form::checkbox('is_holiday', '1', null, ['id' => 'is_holiday']) }}
                                <label for="is_holiday"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_holiday') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label> {{ __('Status')}}</label>
                                {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                <label for="is_active"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All CalendarEvents')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Description')}}</th>
                                    <th>{{ __('Status')}}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($calendarEvents as $calendarEvent)
                                <tr>
                                    <td>{{ $calendarEvent->name }}</td>
                                    <td> {{ Str::of($calendarEvent->description)->limit(20, '...') }}</td>
                                    <td>
                                        <div class="form-group" type="button" data-toggle="modal"
                                            data-target="#myModal{{$calendarEvent->id}}">
                                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                                {{ Form::checkbox('toggle_active', '1',
                                                $calendarEvent->is_active,['id'=>'active-'.$calendarEvent->id]) }}
                                                <label for=""> </label>
                                            </div>
                                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                                        </div>
                                        <div class="modal fade" id="myModal{{$calendarEvent->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h4>{{ __('Do you want to change status?')}}</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        {{ Form::model($calendarEvents, ['route' =>
                                                        ['calendarEvent.status',
                                                        $calendarEvent->id], 'method' => 'patch', 'class' =>
                                                        'd-inline-block']) }}
                                                        {{ Form::button('Ok', ['type' => 'submit',
                                                        'class' => 'btn btn-info']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('calendarEvent.edit', $calendarEvent->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$calendarEvent->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$calendarEvent->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($calendarEvents, ['route' =>
                                                        ['calendarEvent.destroy',
                                                        $calendarEvent->id],
                                                        'method'=> 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- @push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
