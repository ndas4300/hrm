@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Card</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit Card')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    {{ Form::model($card, ['route' => ['card.update', $card->id], 'method' => 'PUT',
                    'class' => 'form-horizontal']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_id', 'Employee Name') }}
                            {{ Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                            'form-control select2', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            {{ Form::label('number', 'Number') }}
                            {{ Form::number('number', null, ['class' => 'form-control', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('number') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('assigned') ? ' has-error' : '' }}">
                            {{ Form::label('assigned', 'Assigned Date') }}
                            {{ Form::date('assigned', null, ['class' => 'form-control', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('assigned') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                            {{ Form::label('note', 'Note') }}
                            {{ Form::textarea('note', null, ['class' => 'form-control', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('note') }}</small>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label> Status</label>
                                {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                <label for="is_active"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


