@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Card</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Card')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Add New Card')}}</h3>
                        </div>
                        {{ Form::open(['route' => 'card.store']) }}
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                {{ Form::label('employee_id', 'Employee Name', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                    'form-control select2', 'required' => 'required']) }}
                                    <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                                {{ Form::label('number', 'Number', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::text('number', null, ['class' => 'form-control', 'required' => 'required'])
                                    }}
                                    <small class="text-danger">{{ $errors->first('number') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('assigned') ? ' has-error' : '' }}">
                                {{ Form::label('assigned', 'Assigned Date', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::date('assigned', null, ['class' => 'form-control', 'required' => 'required'])
                                    }}
                                    <small class="text-danger">{{ $errors->first('assigned') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                {{ Form::label('note', 'Note', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::textarea('note', null, ['class' => 'form-control', 'required' => 'required'])
                                    }}
                                    <small class="text-danger">{{ $errors->first('note') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                    <label> Status</label>
                                    {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                    <label for="is_active"> </label>
                                </div>
                                <small class="text-danger">{{ $errors->first('is_active') }}</small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('All Cards')}}</h3>
                        </div>
                        <div class="card-body">
                            <table id="example2" class="table table-sm table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Balance')}}</th>
                                    <th>{{  __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($cards as $card)
                                    <tr>
                                        <td>{{ $card->employee->name }}</td>
                                        <td>{{ $card->balance }}</td>
                                        {{-- <td> {{ Str::of($card->description)->limit(20, '...') }}</td> --}}
                                        <td>
                                            <div class="form-group" type="button" data-toggle="modal"
                                                 data-target="#myModal{{$card->id}}">
                                                <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                                    {{ Form::checkbox('toggle_active', '1',
                                                    $card->is_active,['id'=>'active-'.$card->id]) }}
                                                    <label for=""> </label>
                                                </div>
                                                <small class="text-danger">{{ $errors->first('is_active') }}</small>
                                            </div>
                                            <div class="modal fade" id="myModal{{$card->id}}" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row  d-flex justify-content-center">
                                                                <div>
                                                                    <h4>{{ __('Do you want to change status?')}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">{{ __('Close')}}
                                                            </button>
                                                            {{ Form::model($cards, ['route' => ['card.status',
                                                            $card->id], 'method' => 'patch', 'class' => 'd-inline-block']) }}
                                                            {{ Form::button('Ok', ['type' => 'submit',
                                                            'class' => 'btn btn-info']) }}
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td nowrap="nowrap">
                                            <a href="{{ route('card.edit', $card->id) }}">
                                                <span class="btn btn-primary btn-sm mr-2">
                                                    <i class="fas fa-edit"></i>
                                                </span>
                                            </a>
                                            <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                                    data-target="#deleteModal{{$card->id}}"><i
                                                    class="fas fa-trash"></i></button>
                                            <div class="modal fade" id="deleteModal{{$card->id}}" tabindex="-1"
                                                 role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                 aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row  d-flex justify-content-center">
                                                                <div>
                                                                    <h3>{{ __('Are You Sure?')}}</h3>
                                                                </div>
                                                                <div>{{ __('Once deleted, you will not be able to recover this
                                                                    file!')}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline-secondary"
                                                                    data-dismiss="modal">{{ __('Close')}}
                                                            </button>
                                                            {{ Form::model($cards, ['route' => ['card.destroy',
                                                            $card->id],
                                                            'method'
                                                            => 'DELETE', 'class' => 'd-inline-block']) }}
                                                            {{ Form::button('Delete', ['type' => 'submit',
                                                            'class' => 'btn btn-outline-danger']) }}
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}")
        @elseif(Session::has('delete'))
        toastr.error("{{ Session::get('delete') }}")
        @endif
    </script>
@endpush
