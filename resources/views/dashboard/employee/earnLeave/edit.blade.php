@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit EarnLeave')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit EarnLeave')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    {{ Form::model($earnLeave, ['route' => ['earnLeave.update', $earnLeave->id], 'method' => 'PUT',
                    'class' => 'form-horizontal']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_id', 'Employee Name') }}
                            {{ Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                            'form-control select2', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            {{ Form::label('date', 'Date') }}
                            {{ Form::date('date', null, ['class' => 'form-control', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('date') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
                            {{ Form::label('balance', 'Balance') }}
                            {{ Form::number('balance', null, ['class' => 'form-control', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('balance') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('previous_balance') ? ' has-error' : '' }}">
                            {{ Form::label('previous_balance', 'Previous Balance') }}
                            {{ Form::number('previous_balance', null, ['class' => 'form-control', 'required' =>
                            'required']) }}
                            <small class="text-danger">{{ $errors->first('previous_balance') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('next_schedule') ? ' has-error' : '' }}">
                            {{ Form::label('next_schedule', 'Next Schedule') }}
                            {{ Form::date('next_schedule', null, ['class' => 'form-control', 'required' => 'required'])
                            }}
                            <small class="text-danger">{{ $errors->first('next_schedule') }}</small>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


