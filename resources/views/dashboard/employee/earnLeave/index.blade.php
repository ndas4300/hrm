@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('EarnLeave')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('EarnLeave')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New EarnLeave')}}</h3>
                    </div>
                    {{ Form::open(['route' => 'earnLeave.store']) }}
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_id', 'Employee Name', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                'form-control select2', 'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            {{ Form::label('date', 'Date', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('date', null, ['class' => 'form-control', 'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('date') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
                            {{ Form::label('balance', 'Balance', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::number('balance', null, ['class' => 'form-control', 'required' => 'required'])
                                }}
                                <small class="text-danger">{{ $errors->first('balance') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('previous_balance') ? ' has-error' : '' }}">
                            {{ Form::label('previous_balance', 'Previous Balance', ['class' => 'col-sm-3 control-label'])
                            }}
                            <div class="col-sm-9">
                                {{ Form::number('previous_balance', null, ['class' => 'form-control', 'required' =>
                                'required']) }}
                                <small class="text-danger">{{ $errors->first('previous_balance') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('next_schedule') ? ' has-error' : '' }}">
                            {{ Form::label('next_schedule', 'Next Schedule', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('next_schedule', null, ['class' => 'form-control', 'required' =>
                                'required']) }}
                                <small class="text-danger">{{ $errors->first('next_schedule') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All EarnLeaves')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Balance')}}</th>
                                    <th>{{  __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($earnLeaves as $earnLeave)
                                <tr>
                                    <td>{{ $earnLeave->employee->name }}</td>
                                    <td>{{ $earnLeave->balance }}</td>
                                    {{-- <td> {{ Str::of($earnLeave->description)->limit(20, '...') }}</td> --}}
                                    <td nowrap="nowrap">
                                        {{-- <button type="button" class="btn btn-info  btn-sm mr-2" data-toggle="modal"
                                            data-target="#myModal{{$earnLeave->id}}">
                                            <i class="fas fa-toggle-on"></i>
                                        </button> --}}
                                        <!-- The Modal -->
                                        {{-- <div class="modal fade" id="myModal{{$earnLeave->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Status</h4>
                                                        <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{ Form::model($earnLeave, ['route' => ['earnLeave.status',
                                                        $earnLeave->id],
                                                        'method' => 'PUT', 'class' => 'form-horizontal']) }}
                                                        {{ Form::select('is_active', ['0' => 'Inactive', '1' =>
                                                        'Active'],null, ['class'=>'form-control'])}}
                                                    </div>
                                                    <div class="modal-footer">
                                                        {{ Form::button('Submit', ['type' =>
                                                        'submit','class' => 'btn btn-success btn-sm mr-2']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <a href="{{ route('earn-leave.edit', $earnLeave->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$earnLeave->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$earnLeave->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($earnLeaves, ['route' => ['earnLeave.destroy',
                                                        $earnLeave->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

{{-- @push('script')

<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
