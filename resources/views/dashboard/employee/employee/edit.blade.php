@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Add New Employee')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('General Form')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <ul id="tabs-nav">
                        <li><a href="#tab1">{{ __('Employee')}}</a></li>
                        <li><a href="#tab2">{{ __('Training')}}</a></li>
                        <li><a href="#tab3">{{ __('Official')}}</a></li>
                        <li><a href="#tab4">{{ __('Address')}}</a></li>
                        <li><a href="#tab5">{{ __('Experience')}}</a></li>
                        <li><a href="#tab6">{{ __('Status')}}</a></li>
                        <li><a href="#tab7">{{ __('Certificates')}}</a></li>
                        <li><a href="#tab8">{{ __('Weekly Off')}}</a></li>
                    </ul> <!-- END tabs-nav -->
                    <div id="tabs-content">
                        {{ Form::model($employee, ['route' => ['employee.update', $employee->id], 'method' => 'PUT', 'class' => 'form-horizontal' ,'files' => true]) }}
                        <div id="tab1" class="tab-content">
                            @include('includes.employee.create.employee')
                        </div>
                        <div id="tab2" class="tab-content">
                            @include('includes.employee.create.academic')
                        </div>
                        <div id="tab3" class="tab-content">
                            @include('includes.employee.create.training')
                        </div>
                        <div id="tab4" class="tab-content">
                            @include('includes.employee.create.official')
                        </div>
                        <div id="tab5" class="tab-content">
                            @include('includes.employee.create.address')
                        </div>
                        <div id="tab6" class="tab-content">
                            @include('includes.employee.create.experience')
                        </div>
                        <div id="tab7" class="tab-content">
                            @include('includes.employee.create.status')
                        </div>
                        <div id="tab8" class="tab-content">
                            @include('includes.employee.create.certificate')
                        </div>
                        <div id="tab9" class="tab-content">
                            @include('includes.employee.create.weeklyOff')
                        </div>
                        <div id="tab10" class="tab-content">
                            @include('includes.employee.create.earnLeave')
                        </div>
                        <div id="tab11" class="tab-content">
                            @include('includes.employee.create.card')
                        </div>

                        <button type="submit" class="btn btn-block btn-primary ">{{('Submit')}}</button>

                        {{ Form::close() }}
                    </div> <!-- END tabs-content -->
                    <!-- END tabs -->
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')

    <script>
        // Show the first tab and hide the rest
        $('#tabs-nav li:first-child').addClass('active');
        $('.tab-content').hide();
        $('.tab-content:first').show();

        // Click function
        $('#tabs-nav li').click(function () {
            $('#tabs-nav li').removeClass('active');
            $(this).addClass('active');
            $('.tab-content').hide();

            var activeTab = $(this).find('a').attr('href');
            $(activeTab).fadeIn();
            return false;
        });

    </script>
@endpush

@push('style')
    <style>
        /* Tabs */
        .tabs {
            width: 600px;
            background-color: #09F;
            border-radius: 5px 5px 5px 5px;
        }

        ul#tabs-nav {
            list-style: none;
            margin: 0;
            padding: 5px;
            overflow: auto;
        }

        ul#tabs-nav li {
            float: left;
            font-weight: bold;
            margin-right: 2px;
            padding: 8px 10px;
            border-radius: 5px 5px 5px 5px;
            border: 1px solid #ffffff;
            background-color: #2b343ede;
            /* border-bottom: none;*/
            cursor: pointer;
        }

        ul#tabs-nav li:hover,
        ul#tabs-nav li.active {
            background-color: #0e82ff;
        }

        #tabs-nav li a {
            text-decoration: none;
            color: #ffffff;
        }

        .tab-content {
            padding: 10px;
            border: 5px solid #d1deec;
            /* background-color: #FFF; */
        }

        /* Just for CodePen styling - don't include if you copy paste */
        html {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            font-weight: 300;
            margin: 2em;
        }
    </style>

@endpush
