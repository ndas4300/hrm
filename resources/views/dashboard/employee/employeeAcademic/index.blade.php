@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('EmployeeAcademic')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('EmployeeAcademic')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New EmployeeAcademic')}}</h3>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    {{ Form::open(['route' => 'employeeAcademic.store']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('education_id') ? ' has-error' : '' }}">
                            {!! Form::label('education_id', 'Education', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('education_id', $education, null, ['id' => 'education_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('education_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('degree') ? ' has-error' : '' }}">
                            {!! Form::label('degree', 'Degree Name', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('degree', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('degree') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('major') ? ' has-error' : '' }}">
                            {!! Form::label('major', 'Major Name', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('major', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('major') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('institute') ? ' has-error' : '' }}">
                            {!! Form::label('institute', 'Institute Name', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('institute', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('institute') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('academic_result_id') ? ' has-error' : '' }}">
                            {!! Form::label('academic_result_id', 'Academic Result', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('academic_result_id', $academicResult, null, ['id' =>
                                'academic_result_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('academic_result_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('marks') ? ' has-error' : '' }}">
                            {!! Form::label('marks', 'Marks', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('marks', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('marks') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                            {!! Form::label('year', 'Year', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('year', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('year') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                            {!! Form::label('duration', 'Duration', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('duration', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('duration') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('achievement') ? ' has-error' : '' }}">
                            {!! Form::label('achievement', 'Achievement', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('achievement', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('achievement') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All LeaveCategorys')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employeeAcademics as $employeeAcademic)
                                <tr>
                                    <td>{{$employeeAcademic->employee->name}}</td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('employeeAcademic.edit', $employeeAcademic->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$employeeAcademic->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$employeeAcademic->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($employeeAcademics, ['route' =>
                                                        ['employeeAcademic.destroy',
                                                        $employeeAcademic->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush
