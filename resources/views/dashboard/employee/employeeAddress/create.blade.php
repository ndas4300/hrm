@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Add New EmployeeAddress')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('General Form')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Add New EmployeeAddress')}}</h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        {{ Form::open(['route' => 'employeeAddress.store']) }}
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                    'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_address_line_one') ? ' has-error' : '' }}">
                                {!! Form::label('pr_address_line_one', 'Present Address Line 1', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pr_address_line_one', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_address_line_one') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_address_line_two') ? ' has-error' : '' }}">
                                {!! Form::label('pr_address_line_two', 'Present Address Line 2', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pr_address_line_two', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_address_line_two') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_phone_one') ? ' has-error' : '' }}">
                                {!! Form::label('pr_phone_one', 'Present Phone Number 1', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::number('pr_phone_one', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_phone_one') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_phone_two') ? ' has-error' : '' }}">
                                {!! Form::label('pr_phone_two', 'Present Phone Number 2', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::number('pr_phone_two', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_phone_two') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_email') ? ' has-error' : '' }}">
                                {!! Form::label('pr_email', 'Present Email address', ['class' =>'col-sm-3 control-label'])
                                !!}
                                <div class="col-sm-9">
                                    {!! Form::email('pr_email', null, ['class' => 'form-control', 'required' => 'required',
                                    'placeholder' => 'eg: foo@bar.com']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_email') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_village') ? ' has-error' : '' }}">
                                {!! Form::label('pr_village', 'Present Village', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pr_village', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_village') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_police_station') ? ' has-error' : '' }}">
                                {!! Form::label('pr_police_station', 'Present Police Station', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pr_police_station', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_police_station') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_post_office') ? ' has-error' : '' }}">
                                {!! Form::label('pr_post_office', 'Present Post Office', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pr_post_office', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_post_office') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_city') ? ' has-error' : '' }}">
                                {!! Form::label('pr_city', 'Present City', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pr_city', null, ['class' => 'form-control', 'required' => 'required'])
                                    !!}
                                    <small class="text-danger">{{ $errors->first('pr_city') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pr_country_id') ? ' has-error' : '' }}">
                                {!! Form::label('pr_country_id', 'Present Country', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('pr_country_id', $country, null, ['id' => 'pr_country_id', 'class' =>
                                    'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pr_country_id') }}</small>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('pa_address_line_one') ? ' has-error' : '' }}">
                                {!! Form::label('pa_address_line_one', 'Parmanent Address Line 1', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pa_address_line_one', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_address_line_one') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_address_line_two') ? ' has-error' : '' }}">
                                {!! Form::label('pa_address_line_two', 'Parmanent Address Line 2', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pa_address_line_two', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_address_line_two') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_phone_one') ? ' has-error' : '' }}">
                                {!! Form::label('pa_phone_one', 'Parmanent Phone Number 1', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::number('pa_phone_one', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_phone_one') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_phone_two') ? ' has-error' : '' }}">
                                {!! Form::label('pa_phone_two', 'Parmanent Phone Number 2', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::number('pa_phone_two', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_phone_two') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_email') ? ' has-error' : '' }}">
                                {!! Form::label('pa_email', 'Parmanent Email address', ['class' =>'col-sm-3 control-label'])
                                !!}
                                <div class="col-sm-9">
                                    {!! Form::email('pa_email', null, ['class' => 'form-control', 'required' => 'required',
                                    'placeholder' => 'eg: foo@bar.com']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_email') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_village') ? ' has-error' : '' }}">
                                {!! Form::label('pa_village', 'Parmanent Village', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pa_village', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_village') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_police_station') ? ' has-error' : '' }}">
                                {!! Form::label('pa_police_station', 'Parmanent Police Station', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pa_police_station', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_police_station') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_post_office') ? ' has-error' : '' }}">
                                {!! Form::label('pa_post_office', 'Parmanent Post Office', ['class' => 'col-sm-3
                                control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pa_post_office', null, ['class' => 'form-control', 'required' =>
                                    'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_post_office') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_city') ? ' has-error' : '' }}">
                                {!! Form::label('pa_city', 'Parmanent City', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('pa_city', null, ['class' => 'form-control', 'required' => 'required'])
                                    !!}
                                    <small class="text-danger">{{ $errors->first('pa_city') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('pa_country_id') ? ' has-error' : '' }}">
                                {!! Form::label('pa_country_id', 'Parmanent Country', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('pa_country_id', $country, null, ['id' => 'pa_country_id', 'class' =>
                                    'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('pa_country_id') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
    </section>
@endsection
