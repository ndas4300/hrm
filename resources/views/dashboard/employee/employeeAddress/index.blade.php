@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('EmployeeAddress')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('EmployeeAddress')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New EmployeeAddress')}}</h3>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    {{ Form::open(['route' => 'employeeAddress.store']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_address_line_one') ? ' has-error' : '' }}">
                            {!! Form::label('pr_address_line_one', 'Present Address Line 1', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pr_address_line_one', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_address_line_one') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_address_line_two') ? ' has-error' : '' }}">
                            {!! Form::label('pr_address_line_two', 'Present Address Line 2', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pr_address_line_two', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_address_line_two') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_phone_one') ? ' has-error' : '' }}">
                            {!! Form::label('pr_phone_one', 'Present Phone Number 1', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('pr_phone_one', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_phone_one') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_phone_two') ? ' has-error' : '' }}">
                            {!! Form::label('pr_phone_two', 'Present Phone Number 2', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('pr_phone_two', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_phone_two') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_email') ? ' has-error' : '' }}">
                            {!! Form::label('pr_email', 'Present Email address', ['class' =>'col-sm-3 control-label'])
                            !!}
                            <div class="col-sm-9">
                                {!! Form::email('pr_email', null, ['class' => 'form-control', 'required' => 'required',
                                'placeholder' => 'eg: foo@bar.com']) !!}
                                <small class="text-danger">{{ $errors->first('pr_email') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_village') ? ' has-error' : '' }}">
                            {!! Form::label('pr_village', 'Present Village', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pr_village', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_village') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_police_station') ? ' has-error' : '' }}">
                            {!! Form::label('pr_police_station', 'Present Police Station', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pr_police_station', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_police_station') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_post_office') ? ' has-error' : '' }}">
                            {!! Form::label('pr_post_office', 'Present Post Office', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pr_post_office', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_post_office') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_city') ? ' has-error' : '' }}">
                            {!! Form::label('pr_city', 'Present City', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pr_city', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('pr_city') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pr_country_id') ? ' has-error' : '' }}">
                            {!! Form::label('pr_country_id', 'Present Country', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('pr_country_id', $country, null, ['id' => 'pr_country_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('pr_country_id') }}</small>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pa_address_line_one') ? ' has-error' : '' }}">
                            {!! Form::label('pa_address_line_one', 'Parmanent Address Line 1', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pa_address_line_one', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_address_line_one') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_address_line_two') ? ' has-error' : '' }}">
                            {!! Form::label('pa_address_line_two', 'Parmanent Address Line 2', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pa_address_line_two', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_address_line_two') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_phone_one') ? ' has-error' : '' }}">
                            {!! Form::label('pa_phone_one', 'Parmanent Phone Number 1', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('pa_phone_one', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_phone_one') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_phone_two') ? ' has-error' : '' }}">
                            {!! Form::label('pa_phone_two', 'Parmanent Phone Number 2', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('pa_phone_two', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_phone_two') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_email') ? ' has-error' : '' }}">
                            {!! Form::label('pa_email', 'Parmanent Email address', ['class' =>'col-sm-3 control-label'])
                            !!}
                            <div class="col-sm-9">
                                {!! Form::email('pa_email', null, ['class' => 'form-control', 'required' => 'required',
                                'placeholder' => 'eg: foo@bar.com']) !!}
                                <small class="text-danger">{{ $errors->first('pa_email') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_village') ? ' has-error' : '' }}">
                            {!! Form::label('pa_village', 'Parmanent Village', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pa_village', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_village') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_police_station') ? ' has-error' : '' }}">
                            {!! Form::label('pa_police_station', 'Parmanent Police Station', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pa_police_station', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_police_station') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_post_office') ? ' has-error' : '' }}">
                            {!! Form::label('pa_post_office', 'Parmanent Post Office', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pa_post_office', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_post_office') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_city') ? ' has-error' : '' }}">
                            {!! Form::label('pa_city', 'Parmanent City', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('pa_city', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('pa_city') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('pa_country_id') ? ' has-error' : '' }}">
                            {!! Form::label('pa_country_id', 'Parmanent Country', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('pa_country_id', $country, null, ['id' => 'pa_country_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('pa_country_id') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __(Submit)}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All LeaveCategorys')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Present Country')}}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employeeAddresses as $employeeAddress)
                                <tr>
                                    <td>{{$employeeAddress->employee->name}}</td>
                                    <td>{{$employeeAddress->prCountry->name}}</td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('employeeAddress.edit', $employeeAddress->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$employeeAddress->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$employeeAddress->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>Are You Sure?</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($employeeAddresses, ['route' =>
                                                        ['employeeAddress.destroy',
                                                        $employeeAddress->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- @push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
