@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Edit EmployeeExperience')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Edit EmployeeExperience')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="card card-primary">

                        {{ Form::model($employeeExperience, ['route' => ['employeeExperience.update', $employeeExperience->id], 'method' => 'PUT', 'class' => 'form-horizontal']) }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card card-body">
                                    <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                        {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-12
                                        control-label']) !!}
                                        <div class="col-sm-12">
                                            {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id',
                                            'class' =>
                                            'form-control select2', 'required' => 'required']) !!}
                                            <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                        {!! Form::label('company', 'Company Name', ['class' => 'col-sm-8
                                        control-label']) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('company', null, ['class' => 'form-control', 'required'
                                            => 'required'])
                                            !!}
                                            <small class="text-danger">{{ $errors->first('company') }}</small>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('business') ? ' has-error' : '' }}">
                                        {!! Form::label('business', 'Business', ['class' => 'col-sm-8
                                        control-label']) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('business', null, ['class' => 'form-control', 'required'
                                            => 'required'])
                                            !!}
                                            <small class="text-danger">{{ $errors->first('business') }}</small>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                                        {!! Form::label('designation', 'Designation', ['class' => 'col-sm-8
                                        control-label']) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('designation', null, ['class' => 'form-control',
                                            'required' =>
                                            'required'])
                                            !!}
                                            <small class="text-danger">{{ $errors->first('designation') }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card card-body">
                                    <div class="form-group{{ $errors->has('area_of_experience') ? ' has-error' : '' }}">
                                        {!! Form::label('area_of_experience', 'Area of Experience', ['class' =>
                                        'col-sm-8
                                        control-label']) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('area_of_experience', null, ['class' => 'form-control',
                                            'required' =>
                                            'required'])
                                            !!}
                                            <small class="text-danger">{{ $errors->first('area_of_experience')}}</small>
                                        </div>
                                    </div>
                                    <div
                                        class="form-group{{ $errors->has('experience_location') ? ' has-error' : '' }}">
                                        {!! Form::label('experience_location', 'Experience Location', ['class' =>
                                        'col-sm-8
                                        control-label']) !!}
                                        <div class="col-sm-9">
                                            {!! Form::text('experience_location', null, ['class' => 'form-control',
                                            'required' =>
                                            'required'])
                                            !!}
                                            <small class="text-danger">{{ $errors->first('experience_location')}}</small>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('experience_start') ? ' has-error' : '' }}">
                                        {!! Form::label('experience_start', 'Experience Start', ['class' =>
                                        'col-sm-8
                                        control-label']) !!}
                                        <div class="col-sm-9">
                                            {!! Form::date('experience_start', null, ['class' => 'form-control',
                                            'required' =>
                                            'required']) !!}
                                            <small class="text-danger">{{ $errors->first('experience_start')}}</small>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('experience_end') ? ' has-error' : '' }}">
                                        {!! Form::label('experience_end', 'Experience End', ['class' => 'col-sm-8
                                        control-label'])
                                        !!}
                                        <div class="col-sm-9">
                                            {!! Form::date('experience_end', null, ['class' => 'form-control',
                                            'required' =>
                                            'required']) !!}
                                            <small class="text-danger">{{ $errors->first('experience_end')}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
    </section>
@endsection
