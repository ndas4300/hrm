@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('EmployeeExperience')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('EmployeeExperience')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New EmployeeExperience')}}</h3>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    {{ Form::open(['route' => 'employeeExperience.store']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                            {!! Form::label('company', 'Company Name', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('company', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('company') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('business') ? ' has-error' : '' }}">
                            {!! Form::label('business', 'Business', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('business', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('business') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                            {!! Form::label('designation', 'Designation', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('designation', null, ['class' => 'form-control', 'required' =>
                                'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('designation') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('area_of_experience') ? ' has-error' : '' }}">
                            {!! Form::label('area_of_experience', 'Area of Experience', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('area_of_experience', null, ['class' => 'form-control', 'required' =>
                                'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('area_of_experience') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('experience_location') ? ' has-error' : '' }}">
                            {!! Form::label('experience_location', 'Experience Location', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('experience_location', null, ['class' => 'form-control', 'required' =>
                                'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('experience_location') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('experience_start') ? ' has-error' : '' }}">
                            {!! Form::label('experience_start', 'Experience Start', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::date('experience_start', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('experience_start') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('experience_end') ? ' has-error' : '' }}">
                            {!! Form::label('experience_end', 'Experience End', ['class' => 'col-sm-3 control-label'])
                            !!}
                            <div class="col-sm-9">
                                {!! Form::date('experience_end', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('experience_end') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All LeaveCategorys')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Description')}}</th>
                                    <th>{{ __('Status')}}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employeeExperiences as $employeeExperience)
                                <tr>
                                    <td>{{$employeeExperience->employee->name}}</td>
                                    <td>{{$employeeExperience->name}}</td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('employeeExperience.edit', $employeeExperience->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$employeeExperience->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$employeeExperience->id}}"
                                            tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        {{ Form::model($employeeExperiences, ['route' =>
                                                        ['employeeExperience.destroy',
                                                        $employeeExperience->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- @push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
