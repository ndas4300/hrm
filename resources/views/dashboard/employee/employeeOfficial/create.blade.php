@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Add New EmployeeOfficial')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('General Form')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New EmployeeOfficial')}}</h3>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    {{ Form::open(['route' => 'employeeOfficial.store', 'enctype'=>'multipart/form-data']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('unit_id') ? ' has-error' : '' }}">
                            {!! Form::label('unit_id', 'Unit', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('unit_id', $unit, null, ['id' => 'unit_id', 'class' => 'form-control',
                                'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('unit_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('floor_id') ? ' has-error' : '' }}">
                            {!! Form::label('floor_id', 'Floor', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('floor_id',$floor, null, ['id' => 'floor_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('floor_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('section_id') ? ' has-error' : '' }}">
                            {!! Form::label('section_id', 'Section', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('section_id', $section, null, ['id' => 'section_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('section_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
                            {!! Form::label('department_id', 'Department', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('department_id',$department, null, ['id' => 'department_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('department_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('line_id') ? ' has-error' : '' }}">
                            {!! Form::label('line_id', 'Line', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('line_id',$line, null, ['id' => 'line_id', 'class' => 'form-control',
                                'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('line_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('designation_id') ? ' has-error' : '' }}">
                            {!! Form::label('designation_id', 'Designation', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('designation_id',$designation, null, ['id' => 'designation_id', 'class'
                                => 'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('designation_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('employee_type_id') ? ' has-error' : '' }}">
                            {!! Form::label('employee_type_id', 'Employee Type', ['class' =>
                            'col-sm-9 control-label'])
                            !!}
                            <div class="">
                                {!! Form::select('employee_type_id', $employeeType, null, ['id' =>
                                'employee_type_id',
                                'class' => 'form-control select2', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('employee_type_id')
                                    }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('grade_id') ? ' has-error' : '' }}">
                            {!! Form::label('grade_id', 'Grade', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('grade_id', $grade, null, ['id' => 'grade_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('grade_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('shift_id') ? ' has-error' : '' }}">
                            {!! Form::label('shift_id', 'Shift', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('shift_id',$shift, null, ['id' => 'shift_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('shift_id') }}</small>
                            </div>
                        </div>
                        {{-- <div class="form-group{{ $errors->has('shift_group_id') ? ' has-error' : '' }}">
                            {!! Form::label('shift_group_id', 'Shift Group') !!}
                            {!! Form::select('shift_group_id', $shift_group, null, ['id' => 'shift_group_id', 'class' =>
                            'form-control select2']) !!}
                            <small class="text-danger">{{ $errors->first('shift_group_id') }}</small>
                        </div> --}}
                        <div class="form-group{{ $errors->has('calendar_id') ? ' has-error' : '' }}">
                            {!! Form::label('calendar_id', 'Calendar', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('calendar_id', $calendar, null, ['id' => 'calendar_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('calendar_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('joining_date') ? ' has-error' : '' }}">
                            {!! Form::label('joining_date', 'Joining Date', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::date('joining_date', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('joining_date') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('leave_category_id') ? ' has-error' : '' }}">
                            {!! Form::label('leave_category_id', 'Leave Category', ['class' => 'col-sm-3
                            control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('leave_category_id',$leaveCategory, null, ['id' => 'leave_category_id',
                                'class' => 'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('leave_category_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('gross') ? ' has-error' : '' }}">
                            {!! Form::label('gross', 'Gross', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('gross', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('gross') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('fixed') ? ' has-error' : '' }}">
                            {!! Form::label('fixed', 'Fixed', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('fixed', null, ['class' => 'form-control', 'required' => 'required'])
                                !!}
                                <small class="text-danger">{{ $errors->first('fixed') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('transport_id') ? ' has-error' : '' }}">
                            {!! Form::label('transport_id', 'Transport', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('transport_id', $options, null, ['id' => 'transport_id', 'class' =>
                                'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('transport_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('transport_amount') ? ' has-error' : '' }}">
                            {!! Form::label('transport_amount', 'Transport Amount') !!}
                            {!! Form::number('transport_amount', null, ['class' => 'form-control', 'required' =>
                            'required']) !!}
                            <small class="text-danger">{{ $errors->first('transport_amount') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('medical_amount') ? ' has-error' : '' }}">
                            {!! Form::label('medical_amount', 'Medical Amount', ['class' => 'col-sm-3 control-label'])
                            !!}
                            <div class="col-sm-9">
                                {!! Form::number('medical_amount', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('medical_amount') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('house_rent') ? ' has-error' : '' }}">
                            {!! Form::label('house_rent', 'House Rent', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('house_rent', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('house_rent') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('other_amount') ? ' has-error' : '' }}">
                            {!! Form::label('other_amount', 'Other Amount', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::number('other_amount', null, ['class' => 'form-control', 'required' =>
                                'required']) !!}
                                <small class="text-danger">{{ $errors->first('other_amount') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


