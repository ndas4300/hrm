@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit EmployeeStatus')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit EmployeeStatus')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    {{ Form::model($employeeStatus, ['route' => ['employeeStatus.update', $employeeStatus->id],
                    'method' => 'PUT',
                    'class' => 'form-horizontal']) }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card card-body">
                                <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                    {{ Form::label('employee_id', 'Employee Name', ['class' => 'col-sm-12
                                    control-label']) }}
                                    <div class="col-sm-9">
                                        {{ Form::select('employee_id', $employee, null, ['id' => 'employee_id',
                                        'class' =>
                                        'form-control select2', 'required' => 'required']) }}
                                        <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                                    {{ Form::label('status_id', 'Status', ['class' => 'col-sm-9 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ Form::number('status_id', null, ['class' => 'form-control', 'required' =>
                                        'required']) }}
                                        <small class="text-danger">{{ $errors->first('status_id') }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                    {{ Form::label('date', 'Date', ['class' => 'col-sm-9 control-label']) }}
                                    <div class="col-sm-9">
                                        {{ Form::date('date', null, ['class' => 'form-control', 'required' =>
                                        'required']) }}
                                        <small class="text-danger">{{ $errors->first('date') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card card-body">
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    {{ Form::label('description', 'Description', ['class' => 'col-sm-9
                                    control-label']) }}
                                    <div class="col-sm-9">
                                        {{ Form::textarea('description', null, ['class' => 'form-control',
                                        'required' =>
                                        'required','rows'=>'8']) }}
                                        <small class="text-danger">{{ $errors->first('description') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


