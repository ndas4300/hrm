@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit EmployeeTraining')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit EmployeeTraining')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">

                    {{ Form::model($employeeTraining, ['route' => ['employeeTraining.update', $employeeTraining->id],
                    'method' => 'PUT', 'class' => 'form-horizontal']) }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card card-body">
                                <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                    {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-9
                                    control-label']) !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('employee_id', $employee, null, ['id' =>
                                        'employee_id', 'class' => 'form-control select2', 'required' =>
                                        'required'])
                                        !!}
                                        <small class="text-danger">{{ $errors->first('employee_id')
                                            }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('training_title') ? ' has-error' : '' }}">
                                    {!! Form::label('training_title', 'Training Title', ['class' =>
                                    'col-sm-6 control-label']) !!}
                                    <div class="">
                                        {!! Form::text('training_title', null, ['class' => 'form-control',
                                        'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('training_title')
                                            }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('topics_covered') ? ' has-error' : '' }}">
                                    {!! Form::label('topics_covered', 'Topics Covered', ['class' =>
                                    'col-sm-6 control-label']) !!}
                                    <div class="">
                                        {!! Form::text('topics_covered', null, ['class' => 'form-control',
                                        'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('topics_covered')
                                            }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('training_institute') ? ' has-error' : '' }}">
                                    {!! Form::label('training_institute', 'Training Institute', ['class' =>
                                    'col-sm-6 control-label']) !!}
                                    <div class="">
                                        {!! Form::text('training_institute', null, ['class' =>
                                        'form-control', 'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('training_institute')
                                            }}</small>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="card card-body">
                                <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                    {!! Form::label('country_id', 'Training Country', ['class' => 'col-sm-9
                                    control-label']) !!}
                                    <div class="">
                                        {!! Form::select('country_id', $country, null, ['id' =>
                                        'country_id', 'class' => 'form-control select2', 'required' =>
                                        'required'])
                                        !!}
                                        <small class="text-danger">{{ $errors->first('country_id')
                                            }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('training_year') ? ' has-error' : '' }}">
                                    {!! Form::label('training_year', 'Training Year', ['class' => 'col-sm-6
                                    control-label']) !!}
                                    <div class="">
                                        {!! Form::number('training_year', null, ['class' => 'form-control',
                                        'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('training_year')
                                            }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('training_duration') ? ' has-error' : '' }}">
                                    {!! Form::label('training_duration', 'Training Duration', ['class' =>
                                    'col-sm-6 control-label']) !!}
                                    <div class="">
                                        {!! Form::number('training_duration', null, ['class' =>
                                        'form-control', 'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('training_duration')
                                            }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('training_location') ? ' has-error' : '' }}">
                                    {!! Form::label('training_location', 'Training Location', ['class' =>
                                    'col-sm-6 control-label']) !!}
                                    <div class="">
                                        {!! Form::text('training_location', null, ['class' =>
                                        'form-control', 'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('training_location')
                                            }}</small>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection


