@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit EmployeeWeeklyOff')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit EmployeeWeeklyOff')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    {{ Form::model($employeeWeeklyOff, ['route' => ['employeeWeeklyOff.update', $employeeWeeklyOff->id], 'method' => 'PUT',
                    'class' => 'form-horizontal']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_id', 'Employee Name') }}
                            {{ Form::select('employee_id', $employees, $employeeWeeklyOff->employees, ['id' => 'employee_id', 'class' =>
                            'form-control select2', 'required' => 'required']) }}
                            <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                        </div>
                        <div class="form-group{{ $errors->has('day_id') ? ' has-error' : '' }}">
                        {{ Form::label('day_id', 'Day') }}
                        {{ Form::select('day_id', $days, null, ['id' => 'day_id', 'class' => 'form-control', 'required' => 'required']) }}
                        <small class="text-danger">{{ $errors->first('day_id') }}</small>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


