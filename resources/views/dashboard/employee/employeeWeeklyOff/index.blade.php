@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('EmployeeWeeklyOff')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('EmployeeWeeklyOff')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New EmployeeWeeklyOff')}}</h3>
                    </div>
                    {{ Form::open(['route' => 'employeeWeeklyOff.store']) }}
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_id', 'Employee Name', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('employee_id', $employees, null, ['id' => 'employee_id', 'class' =>
                                'form-control select2-multiple']) }}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('employee_weekly_off_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_weekly_off_id', 'Day', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('employee_weekly_off_id[]', $days, null, ['id' => 'employee_weekly_off_id', 'class' => 'form-control select2',
                                'required' => 'required', 'multiple']) }}
                                <small class="text-danger">{{ $errors->first('employee_weekly_off_id') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All EmployeeWeeklyOffs')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Employee Name')}}</th>
                                    <th>{{ __('Off Day')}}</th>
                                    <th>{{  __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employeeWeeklyOffs as $employeeWeeklyOff)
                                <tr>
                                    <td>
                                        @foreach ($employeeWeeklyOff->employees as $employee)
                                        {{ $employee->name }}
                                        @endforeach
                                    </td>
                                    <td>{{$employeeWeeklyOff->day->name}}</td>
                                    <td nowrap="nowrap">
                                        {{-- <button type="button" class="btn btn-info  btn-sm mr-2" data-toggle="modal"
                                            data-target="#myModal{{$employeeWeeklyOff->id}}">
                                            <i class="fas fa-toggle-on"></i>
                                        </button> --}}
                                        <!-- The Modal -->
                                        {{-- <div class="modal fade" id="myModal{{$employeeWeeklyOff->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Status</h4>
                                                        <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{ Form::model($employeeWeeklyOff, ['route' =>
                                                        ['employeeWeeklyOff.status',
                                                        $employeeWeeklyOff->id],
                                                        'method' => 'PUT', 'class' => 'form-horizontal']) }}
                                                        {{ Form::select('is_active', ['0' => 'Inactive', '1' =>
                                                        'Active'],null, ['class'=>'form-control'])}}
                                                    </div>
                                                    <div class="modal-footer">
                                                        {{ Form::button('Submit', ['type' =>
                                                        'submit','class' => 'btn btn-success btn-sm mr-2']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        {{-- <a href="{{ route('employeeWeeklyOff.edit', $employeeWeeklyOff->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a> --}}
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$employeeWeeklyOff->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$employeeWeeklyOff->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($employeeWeeklyOffs, ['route' =>
                                                        ['employeeWeeklyOff.destroy',
                                                        $employeeWeeklyOff->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

{{-- @push('script')

<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
