@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit Gender')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit Gender')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    {{ Form::model($gender, ['route' => ['gender.update', $gender->id], 'method' => 'PUT', 'class' =>
                    'form-horizontal']) }}
                    @include('includes.generalForm')
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
