@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Edit Leave')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Edit Leave')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <div class="card card-primary">
                        {{ Form::model($leave, ['route' => ['leave.update', $leave->id], 'method' => 'PUT', 'class' => 'form-horizontal']) }}
                        <div class="card-body">
                            <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                {{ Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('employee_id', $employees, null, ['id' => 'employee_id', 'class' =>
                                    'form-control select2', 'required' => 'required']) }}
                                    <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('leave_category_id') ? ' has-error' : '' }}">
                                {{ Form::label('leave_category_id', 'Leave Category', ['class' => 'col-sm-3
                                control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('leave_category_id', $leaveCategories, null, ['id' =>
                                    'leave_category_id', 'class' => 'form-control', 'required' => 'required'])
                                    }}
                                    <small class="text-danger">{{ $errors->first('leave_category_id') }}</small>
                                </div>
                            </div>
                            {{ Form::hidden('date', \Carbon\Carbon::now()) }}

                            <div class="form-group{{ $errors->has('leave_from') ? ' has-error' : '' }}">
                                {{ Form::label('leave_from', 'Leave From', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::date('leave_from', null, ['class' => 'form-control', 'required' =>
                                    'required']) }}
                                    <small class="text-danger">{{ $errors->first('leave_from') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('leave_to') ? ' has-error' : '' }}">
                                {{ Form::label('leave_to', 'Leave To', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::date('leave_to', null, ['class' => 'form-control', 'required' => 'required'])
                                    }}
                                    <small class="text-danger">{{ $errors->first('leave_to') }}</small>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('approved_by') ? ' has-error' : '' }}">
                                {{ Form::label('approved_by', 'Approved By', ['class' => 'col-sm-3 control-label']) }}
                                <div class="col-sm-9">
                                    {{ Form::select('approved_by', $users, null, ['id' => 'approved_by', 'class' => 'form-control',
                                    'required' => 'required']) }}
                                    <small class="text-danger">{{ $errors->first('approved_by') }}</small>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
    </section>
@endsection
