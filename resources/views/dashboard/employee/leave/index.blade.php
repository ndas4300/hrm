@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Leave')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Leave')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New Leave')}}</h3>
                    </div>
                    {{ Form::open(['route' => 'leave.store']) }}
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                            {{ Form::label('employee_id', 'Employee', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('employee_id', $employees, null, ['id' => 'employee_id', 'class' =>
                                'form-control select2', 'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('leave_category_id') ? ' has-error' : '' }}">
                            {{ Form::label('leave_category_id', 'Leave Category', ['class' => 'col-sm-3
                            control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('leave_category_id', $leaveCategories, null, ['id' =>
                                'leave_category_id', 'class' => 'form-control', 'required' => 'required'])
                                }}
                                <small class="text-danger">{{ $errors->first('leave_category_id') }}</small>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('leave_from') ? ' has-error' : '' }}">
                            {{ Form::label('leave_from', 'Leave From', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('leave_from', null, ['class' => 'form-control', 'required' =>
                                'required']) }}
                                <small class="text-danger">{{ $errors->first('leave_from') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('leave_to') ? ' has-error' : '' }}">
                            {{ Form::label('leave_to', 'Leave To', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::date('leave_to', null, ['class' => 'form-control', 'required' => 'required'])
                                }}
                                <small class="text-danger">{{ $errors->first('leave_to') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('approved_by') ? ' has-error' : '' }}">
                            {{ Form::label('approved_by', 'Approved By', ['class' => 'col-sm-3 control-label']) }}
                            <div class="col-sm-9">
                                {{ Form::select('approved_by', $users, null, ['id' => 'approved_by', 'class' => 'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('approved_by') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All Leaves')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Employee')}}</th>
                                    <th>{{  __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($leaves as $leave)
                                <tr>
                                    <td>{{ $leave->employee->name }}</td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('leave.edit', $leave->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$leave->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$leave->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($leaves, ['route' => ['leave.destroy',
                                                        $leave->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

{{-- @push('script')

<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
