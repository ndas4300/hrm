@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Maritial Status')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Maritial Status</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add New Maritial Status</h3>
                    </div>
                    {{ Form::open(['route' => 'maritialStatus.store']) }}
                    @include('includes.generalForm')
                    {{ Form::close() }}
                </div>
            </div>

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">All Maritial Statuss</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                        <th>Status</th>
                                    <th>{{  __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($maritialStatuses as $maritialStatus)
                                <tr>
                                    <td>{{$maritialStatus->name}}</td>
                                    <td> {{Str::of($maritialStatus->description)->limit(20, '...')}}</td>
                                    <td>
                                        <div class="form-group" type="button" data-toggle="modal"
                                            data-target="#myModal{{$maritialStatus->id}}">
                                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                                {{ Form::checkbox('toggle_active', '1',
                                                $maritialStatus->is_active,['id'=>'active-'.$maritialStatus->id]) }}
                                                <label for=""> </label>
                                            </div>
                                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                                        </div>
                                        <div class="modal fade" id="myModal{{$maritialStatus->id}}" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h4>Do you want to change status?</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        {{ Form::model($maritialStatuses, ['route' => ['maritialStatus.status',
                                                        $maritialStatus->id], 'method' => 'patch', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Ok', ['type' => 'submit',
                                                        'class' => 'btn btn-info']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td nowrap="nowrap">
                                        {{-- Edit Modal --}}
                                        <button type="button" class="btn btn-primary btn-sm mr-2" data-toggle="modal"
                                            data-target="#editModal{{$maritialStatus->id}}">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="editModal{{$maritialStatus->id}}" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Details
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{ Form::model($maritialStatus, ['route' => ['maritialStatus.update', $maritialStatus->id],
                                                        'method' => 'PUT', 'class' => 'form-horizontal']) }}
                                                        <div class="card-body">
                                                            <div class="form-group">
                                                                <div
                                                                    class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                                    {{ Form::label('name', 'Name') }}
                                                                    {{ Form::text('name', null, ['placeholder' =>
                                                                    'Name', 'class' => 'form-control', 'required' =>
                                                                    'required']) }}
                                                                    <small class="text-danger">{{ $errors->first('name')
                                                                        }}</small>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div
                                                                    class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                                                    {{ Form::label('description', 'Description') }}
                                                                    {{ Form::textarea('description', null,
                                                                    ['placeholder' => 'Write Description', 'class' =>
                                                                    'form-control']) }}
                                                                    <small class="text-danger">{{
                                                                        $errors->first('description') }}</small>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div
                                                                    class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                                                    {{-- <label> Status</label> --}}
                                                                    {{ Form::checkbox('is_active', '1', null, ['id' =>
                                                                    'active'.$maritialStatus->id]) }}
                                                                    <label for="active{{$maritialStatus->id}}"> </label>
                                                                </div>
                                                                <small class="text-danger">{{
                                                                    $errors->first('is_active') }}</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save
                                                            changes</button>
                                                    </div>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Delete Modal --}}
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$maritialStatus->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$maritialStatus->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>Are You Sure?</h3>
                                                            </div>
                                                            <div>Once deleted, you will not be able to recover this
                                                                file!</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        {{ Form::model($maritialStatuses, ['route' => ['maritialStatus.destroy',
                                                        $maritialStatus->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- @push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
