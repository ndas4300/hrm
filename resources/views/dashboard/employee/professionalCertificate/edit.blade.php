@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Edit ProfessionalCertificate')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Edit ProfessionalCertificate')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">

                    {{ Form::model($professionalCertificate, ['route' => ['professionalCertificate.update',
                    $professionalCertificate->id], 'method' => 'PUT', 'class' => 'form-horizontal']) }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card card-body">
                                <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                                    {!! Form::label('employee_id', 'Employee', ['class' => 'col-sm-9
                                    control-label'])
                                    !!}
                                    <div class="col-sm-9">
                                        {!! Form::select('employee_id', $employee, null, ['id' => 'employee_id',
                                        'class'
                                        => 'form-control select2', 'required' => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('employee_id') }}</small>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('certification') ? ' has-error' : '' }}">
                                    {!! Form::label('certification', 'Certificate', ['class' => 'col-sm-9
                                    control-label']) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('certification', null, ['class' => 'form-control', 'required'
                                        =>
                                        'required']) !!}
                                        <small class="text-danger">{{ $errors->first('certification') }}</small>
                                    </div>
                                </div>
                                <div
                                    class="form-group{{ $errors->has('certification_institute') ? ' has-error' : '' }}">
                                    {!! Form::label('certification_institute', 'Certification Institute', ['class'
                                    =>
                                    'col-sm-9
                                    control-label']) !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('certification_institute', null, ['class' => 'form-control',
                                        'required'
                                        => 'required']) !!}
                                        <small class="text-danger">{{ $errors->first('certification_institute')
                                            }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                        </div>
                    </div>
            </div>
        </div>
</section>
@endsection
