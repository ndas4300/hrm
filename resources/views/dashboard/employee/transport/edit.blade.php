@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Edit Transport Location')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Edit Transport Location')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    {{ Form::model($transport, ['route' => ['transport.update', $transport->id], 'method' => 'PUT',
                    'class' => 'form-horizontal']) }}
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                {{ Form::label('location', 'Location') }}
                                {{ Form::text('location', null, ['placeholder' => 'Location', 'class' => 'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('location') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('location_bn') ? ' has-error' : '' }}">
                                {{ Form::label('location_bn', 'Location(bn)') }}
                                {{ Form::text('location_bn', null, ['placeholder' => 'Location in Bengali', 'class' =>
                                'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('location_bn') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('fair') ? ' has-error' : '' }}">
                            {!! Form::label('fair', 'Fair', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('fair', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('fair') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class'
                                => 'form-control']) }}
                                <small class="text-danger">{{ $errors->first('description') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label> Status</label>
                                {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                <label for="is_active"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
