@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Transport Location')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Transport Location')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New Transport Location')}}</h3>
                    </div>
                    {{ Form::open(['route' => 'transport.store']) }}
                    <div class="card-body">
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                {{ Form::label('location', 'Location') }}
                                {{ Form::text('location', null, ['placeholder' => 'Location', 'class' => 'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('location') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('location_bn') ? ' has-error' : '' }}">
                                {{ Form::label('location_bn', 'Location(bn)') }}
                                {{ Form::text('location_bn', null, ['placeholder' => 'Location in Bengali', 'class' =>
                                'form-control',
                                'required' => 'required']) }}
                                <small class="text-danger">{{ $errors->first('location_bn') }}</small>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('fair') ? ' has-error' : '' }}">
                            {!! Form::label('fair', 'Fair', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('fair', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                <small class="text-danger">{{ $errors->first('fair') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class'
                                => 'form-control']) }}
                                <small class="text-danger">{{ $errors->first('description') }}</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label> Status</label>
                                {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                <label for="is_active"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('All Transport Locations')}}</h3>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-sm table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Description')}}</th>
                                    <th>{{ __('Status')}}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($transports as $transport)
                                <tr>
                                    <td>{{$transport->location}}</td>
                                    <td> {{Str::of($transport->description)->limit(20, '...')}}</td>
                                    <td>
                                        <div class="form-group" type="button" data-toggle="modal"
                                            data-target="#myModal{{$transport->id}}">
                                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                                {{ Form::checkbox('toggle_active', '1',
                                                $transport->is_active,['id'=>'active-'.$transport->id]) }}
                                                <label for=""> </label>
                                            </div>
                                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                                        </div>
                                        <div class="modal fade" id="myModal{{$transport->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h4>{{ __('Do you want to change status?')}}</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                        {{ Form::model($transports, ['route' => ['transport.status',
                                                        $transport->id], 'method' => 'patch', 'class' =>
                                                        'd-inline-block']) }}
                                                        {{ Form::button('Ok', ['type' => 'submit',
                                                        'class' => 'btn btn-info']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td nowrap="nowrap">
                                        <a href="{{ route('transport.edit', $transport->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        <button class="btn btn-danger btn-sm mr-2" data-toggle="modal"
                                            data-target="#deleteModal{{$transport->id}}"> <i
                                                class="fas fa-trash"></i></button>
                                        <div class="modal fade" id="deleteModal{{$transport->id}}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row  d-flex justify-content-center">
                                                            <div>
                                                                <h3>{{ __('Are You Sure?')}}</h3>
                                                            </div>
                                                            <div>{{ __('Once deleted, you will not be able to recover this
                                                                file!')}}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-secondary"
                                                            data-dismiss="modal">{{ __('Close')}}</button>
                                                        {{ Form::model($transports, ['route' => ['transport.destroy',
                                                        $transport->id],
                                                        'method'
                                                        => 'DELETE', 'class' => 'd-inline-block']) }}
                                                        {{ Form::button('Delete', ['type' => 'submit',
                                                        'class' => 'btn btn-outline-danger']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- @push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
