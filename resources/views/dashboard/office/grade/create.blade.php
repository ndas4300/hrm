@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Grade</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Grade')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('Add New Grade')}}</h3>
                        </div>
                        {{ Form::open(['route' => 'grade.store']) }}
                        <div class="card-body">
                            <div class="form-group">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control', 'required' => 'required']) }}
                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    {{ Form::label('description', 'Description') }}
                                    {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class' => 'form-control']) }}
                                    <small class="text-danger">{{ $errors->first('description') }}</small>
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                                <label> Status</label>
                                {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                                <label for="is_active"> </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('is_active') }}</small>
                        </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ __('Name')}}</th>
                                        <th>{{ __('Description')}}</th>
                                        <th>{{ __('Status')}}</th>
                                        <th>{{ __('Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($grades as $grade)
                                    <tr>
                                        <td>{{$grade->name}}</td>
                                        <td> {{$grade->description}}</td>
                                        <td nowrap="nowrap">
                                        <button type="button" class="btn btn-info  btn-sm mr-2" data-toggle="modal"
                                            data-target="#myModal{{$grade->id}}">
                                            <i class="fas fa-toggle-on"></i>
                                        </button>
                                        <!-- The Modal -->
                                        <div class="modal fade" id="myModal{{$grade->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">{{ __('Status')}}</h4>
                                                        <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        {{ Form::model($grade, ['route' => ['grade.status', $grade->id],
                                                        'method' => 'PUT', 'class' => 'form-horizontal']) }}
                                                        {{ Form::select('is_active', ['0' => 'Inactive', '1' =>
                                                        'Active'],null, ['class'=>'form-control'])}}
                                                    </div>
                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                        {{ Form::button('Submit', ['type' =>
                                                        'submit','class' => 'btn btn-success btn-sm mr-2']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{{ route('grade.edit', $grade->id) }}">
                                                    <span class="btn btn-primary btn-sm mr-2">
                                                        <i class="fas fa-edit"></i>
                                                    </span>
                                                </a>

                                                {{ Form::model($grades, ['route' => ['grade.destroy',$grade->id], 'method' => 'DELETE','class' => 'd-inline-block']) }}
                                                        {{ Form::button('<span class="fas fa-trash"></span>', ['type' => 'submit','class' => 'btn btn-danger btn-sm mr-2']) }}
                                                {{ Form::close() }}


                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
