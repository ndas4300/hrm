@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Line')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Line')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Add New Line')}}</h3>
                    </div>
                    {{ Form::open(['route' => 'line.store']) }}
                    @include('includes.generalForm')
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Description')}}</th>
                                        <th>{{ __('Status')}}</th>
                                    <th>{{  __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lines as $line)
                                <tr>
                                    <td>{{$line->name}}</td>
                                    <td> {{$line->description}}</td>
                                    <td nowrap="nowrap">
                                        <button type="button" class="btn btn-info  btn-sm mr-2" data-toggle="modal"
                                            data-target="#myModal{{$line->id}}">
                                            <i class="fas fa-toggle-on"></i>
                                        </button>
                                        <!-- The Modal -->
                                        <div class="modal fade" id="myModal{{$line->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">{{ __('Status')}}</h4>
                                                        <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        {{ Form::model($line, ['route' => ['line.status', $line->id],
                                                        'method' => 'PUT', 'class' => 'form-horizontal']) }}
                                                        {{ Form::select('is_active', ['0' => 'Inactive', '1' =>
                                                        'Active'],null, ['class'=>'form-control'])}}
                                                    </div>
                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                        {{ Form::button('Submit', ['type' =>
                                                        'submit','class' => 'btn btn-success btn-sm mr-2']) }}
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{{ route('line.edit', $line->id) }}">
                                            <span class="btn btn-primary btn-sm mr-2">
                                                <i class="fas fa-edit"></i>
                                            </span>
                                        </a>
                                        {{ Form::model($lines, ['route' => ['line.destroy',$line->id], 'method' =>
                                        'DELETE','class' => 'd-inline-block']) }}
                                        {{ Form::button('<span class="fas fa-trash"></span>', ['type' =>
                                        'submit','class' => 'btn btn-danger btn-sm mr-2']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
