@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Manual Out Time')}}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                    <li class="breadcrumb-item active">{{ __('Manual Out Time')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('Manual Out Time')}}</h3>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'POST', 'route'=>'searchOut', 'class' => 'form-horizontal']) !!}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                                    {!! Form::label('employee', 'Employee Id', ['class' => 'col-sm-9 control-label'])
                                    !!}
                                    <div class="col-sm-9">
                                        {!! Form::text('employee', null, ['class' => 'form-control', 'required' =>
                                        'required']) !!}
                                        <small class="text-danger">{{ $errors->first('employee') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                    {!! Form::label('date', 'Date', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-9">
                                        {!! Form::date('date', null, ['class' => 'form-control', 'required' =>
                                        'required']) !!}
                                        <small class="text-danger">{{ $errors->first('date') }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group pull-right p-1">
                            {!! Form::button('<span class="fas fa-search"> Search</span>', ['type'=>'submit','class' => 'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @if (isset($attendances))

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Employee Details</h3>
                    </div>

                    <div class="card-body p-0">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Employee Name</th>
                                    <th>Employee No.</th>
                                    <th>Manual Out Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                {!! Form::open(['method' => 'POST', 'route' => 'storeOut', 'class' => 'form-horizontal']) !!}
                                @foreach ($attendances as $attendance)
                                <tr>
                                    <td>1.</td>
                                    <td>{{$attendance->employee->name}}</td>
                                    <td>{{$attendance->employee->employee_no}}</td>
                                    {!! Form::hidden('attendance_id[]', $attendance->id) !!}
                                    <td><div class="form-group{{ $errors->has('manual_out_time') ? ' has-error' : '' }}">
                                    {!! Form::time('manual_out_time[]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('manual_out_time') }}</small>
                                    </div></td>
                                </tr>
                                @endforeach
                                <div class="btn-group pull-right p-3">
                                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                </div>
                                {!! Form::close() !!}
                            </tbody>
                        </table>
                    </div>

                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection

{{-- @push('script')
<script>
    @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
    @elseif(Session::has('delete'))
    toastr.error("{{ Session::get('delete') }}")
    @endif
</script>
@endpush --}}
