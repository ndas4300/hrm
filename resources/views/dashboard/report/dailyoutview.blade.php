
@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Daily out report')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Department')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                            <div class="card-header shadow p-3 mb-5 bg-white rounded no_print">
                              {{ Form::open(['route' => 'get_dailyouttimedata']) }}

                                <div class="row">
                                    <div class="col-5">
                                        <div class="row">
                                            <div class="col-6"> {!! Form::date('date', null, ['id'=>'date','class' => 'form-control','required'=>'required']) !!}</div>
                                            <div class="col-6"> {!! Form::select('department',$department,null, ['id' => 'department','class' => 'form-control select2','required'=>'required', 'placeholder' => 'Select Department']) !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="row">
                                            <div class="col-6"> {!! Form::select('section',$section,null, ['id' => 'section','class' => 'form-control select2','placeholder' => 'Select Section']) !!}</div>
                                            <div class="col-6"> {!! Form::select('line',$line,null, ['id' => 'line','class' => 'form-control select2','placeholder' => 'Select Line']) !!}</div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                           <button type="submit" style="background-color: #1bc1f3;" class="btn btn-primary text-white" style="background-color: #1bc1f3;"><i class="fas fa-search text-white"></i></button>
                                    </div>
                                    {{ Form::close() }}
                                    <div class="col-1 d-flex justify-content-end">
                                        <a href="{{route('dailyouttimepdf',['attn'=>$qr])}}" class="btn btn-secondary pb-0 mr-1"><i class="fas fa-file-pdf"></i></a>
                                        <a class="btn btn-info pb-0" onclick="window.print()"><i class="fas fa-print"></i></a>
                                    </div>
                                </div>

                            </div>

                            <div class="d-none print">
                                <h2>Daily Out report</h2>
                                <hr/>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card card-body">
                                        <div class="row">

                                            <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">Section</th>
                                                    <th scope="col">Line</th>
                                                    <th scope="col">Empoyee ID</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">Out Time</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($attendance))
                                                        @foreach ($attendance as $data)
                                                            <tr>
                                                                <th scope="row">{{$data->employee->officials->section->name}}</th>
                                                                <td>{{$data->employee->officials->line->name}}</td>
                                                                <td>{{$data->employee->employee_no}}</td>
                                                                <td>{{$data->employee->name}}</td>
                                                                <td>{{$data->employee->officials->designation->name}}</td>

                                                                <td>
                                                                    @if($data->manual_out_time || $data->manual_out_time != 0)
                                                                        {{$data->manual_out_time}}
                                                                    @else
                                                                        {{$data->out_time}}
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                              </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- END tabs -->
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')

<script>



</script>
@endpush

@push('style')
<style>

@media print {

    .no_print {
      display: none;
    }
    .print {
        display: block!important;
    }
  }

</style>

@endpush












