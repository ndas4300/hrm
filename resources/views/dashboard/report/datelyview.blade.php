
@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Daily attendance report')}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Department')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                            <div class="card-header shadow p-3 mb-5 bg-white rounded no_print">
                                {{ Form::open(['route' => 'get_datewiseedata']) }}
                                <div class="row d-flex justify-content-between">
                                    <div class="col-3">
                                        {!! Form::date('date',null, ['id'=>'date','class' => 'form-control',
                                        'required' =>
                                        'required']) !!}
                                    </div>
                                    <div class="col-3">
                                        <button type="submit" class="btn btn-primary" style="background-color: #1bc1f3;"><i class="fas fa-search text-white"></i></button>
                                    </div>
                                {{ Form::close() }}
                                    <div class="col-6 d-flex justify-content-end">
                                        <a href="{{route('datelypdf',['attn'=>$qr])}}" class="btn btn-secondary pb-0 mr-1"><i class="fas fa-file-pdf"></i></a>
                                        <a class="btn btn-info pb-0" onclick="window.print()"><i class="fas fa-print"></i></a>
                                    </div>
                                </div>

                            </div>

                            <div class="d-none print">
                                <h2>Daily attendance report</h2>
                                <hr/>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card card-body">
                                                          <div class="row">

                                            <table class="table">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">Emp No</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">Section</th>
                                                    <th scope="col">Shift Out time</th>
                                                    <th scope="col">Out Time</th>
                                                    <th scope="col">Status</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                @if (isset($attendence))
                                                    @foreach ($attendence as $data)
                                                        <tr>
                                                            <th scope="row">{{$data->employee->employee_no}}</th>
                                                            <td>{{$data->employee->name}}</td>
                                                            <td>{{$data->employee->officials->designation->name}}</td>
                                                            <td>{{$data->employee->officials->section->name}}</td>
                                                            <td>{{$data->employee->officials->shift->out}}</td>

                                                            <td>
                                                                @if($data->manual_out_time || $data->manual_out_time != 0)
                                                                    {{$data->manual_out_time}}
                                                                @else
                                                                    {{$data->out_time}}
                                                                @endif
                                                            </td>
                                                            <td>{{$data->attendancestatus[0]->name}}</td>
                                                      </tr>
                                                    @endforeach
                                                @endif

                                                </tbody>
                                              </table>



                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- END tabs -->
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')

<script>




</script>
@endpush

@push('style')
<style>

@media print {

    .no_print {
      display: none;
    }
    .print {
        display: block!important;
    }
  }

</style>

@endpush












