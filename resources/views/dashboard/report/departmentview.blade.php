@extends('layouts.master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Department wise report') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Department') }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="card-header shadow p-3 mb-5 bg-white rounded no_print">
                        {{ Form::open(['route' => 'get_departmentwisedata']) }}
                        <div class="row">
                            <div class="col-3">
                                {!! Form::select('department', $department, null, ['id' => 'department', 'class' => 'form-control select2', 'placeholder' => 'Select Department']) !!}
                            </div>
                            <div class="col-3">
                                {!! Form::select('section', $section, null, ['id' => 'section', 'class' => 'form-control select2', 'placeholder' => 'Select Section']) !!}
                            </div>
                            <div class="col-3">
                                {!! Form::date('date', null, ['id' => 'date', 'class' => 'form-control', 'required' => 'required']) !!}
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-primary" style="background-color: #1bc1f3;"><i class="fas fa-search text-white"></i></button>

                            </div>
                            {{ Form::close() }}
                            <div class="col-2 d-flex justify-content-end">
                                <a href="{{route('departmentwisepdf',['attn'=>$qr])}}" class="btn btn-secondary pb-0 mr-1"><i class="fas fa-file-pdf"></i></a>
                                <a class="btn btn-info pb-0" onclick="window.print()"><i class="fas fa-print"></i></a>
                            </div>
                        </div>

                    </div>

                    <div class="d-none print">
                        <h2>Department wise report</h2>
                        <hr/>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">

                                @if (isset($lines) && count($lines) > 0)
                                    <h3> {{ $lines[1][0]?->employee?->officials?->department?->name }} </h3>
                                @else
                                    <h3> Department Name </h3>

                                    <div class="card card-body">
                                        <p>Line : </p>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">In Time</th>
                                                    <th scope="col">Out Time</th>
                                                    <th scope="col">total Hour</th>
                                                    <th scope="col">OT</th>
                                                    <th scope="col">Extra OT</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="row mt-3">
                                            <div class="col-3">Total Employee: </div>
                                            <div class="col-3">Present: </div>
                                            <div class="col-3">Absent: </div>
                                            <div class="col-3">Late: </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            @if (isset($lines) && $lines != [])
                                @foreach ($lines as $line => $attendance)
                                    <div class="card card-body">
                                        <p>Line : {{ $attendance[0]->Line->name }}</p>
                                        @php
                                            $emp_no = 0;
                                            $abs = 0;
                                            $pre = 0;
                                            $lt = 0;
                                        @endphp
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Designation</th>
                                                    <th scope="col">In Time</th>
                                                    <th scope="col">Out Time</th>
                                                    <th scope="col">total Hour</th>
                                                    <th scope="col">OT</th>
                                                    <th scope="col">Extra OT</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            @foreach ($attendance as $data)
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">{{ $data->Employee->name }}</th>
                                                        <td>{{ $data->employee->officials->designation->name }}</td>
                                                        <td>
                                                            @if($data->manual_in_time || $data->manual_in_time != 0)
                                                                {{$data->manual_in_time}}
                                                            @else
                                                                {{$data->in_time}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($data->manual_out_time || $data->manual_out_time != 0)
                                                                {{$data->manual_out_time}}
                                                            @else
                                                                {{$data->out_time}}
                                                            @endif
                                                        </td>
                                                        <td>{{ \Carbon\Carbon::parse($data->out_time)->floatDiffInHours($data->in_time) }}
                                                        </td>
                                                        <td>{{ $data->ot }}</td>
                                                        <td>{{ $data->extra_ot }}</td>
                                                        <td>{{ $data->attendancestatus[0]->name }}</td>
                                                    </tr>
                                                </tbody>
                                                @php
                                                    $emp_no += 1;
                                                    if ($data->attendance_status_id == 1) {
                                                        $pre += 1;
                                                    } elseif ($data->attendance_status_id == 2) {
                                                        $abs += 1;
                                                    } elseif ($data->attendance_status_id == 3) {
                                                        $lt += 1;
                                                    }
                                                @endphp
                                            @endforeach
                                        </table>
                                        <div class="row mt-3">
                                            <div class="col-3">Total Employee: {{ $emp_no }}</div>
                                            <div class="col-3">Present: {{ $pre }}</div>
                                            <div class="col-3">Absent: {{ $abs }}</div>
                                            <div class="col-3">Late: {{ $lt }}</div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <!-- END tabs -->
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')

    <script>



    </script>
@endpush


@push('style')
<style>

@media print {

    .no_print {
      display: none;
    }
    .print {
        display: block!important;
    }
  }

</style>

@endpush
