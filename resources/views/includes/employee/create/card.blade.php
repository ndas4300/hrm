<div class="card-header shadow p-3 mb-5 bg-light rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add Card')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                {{ Form::label('number', 'Number', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::text('number', null, ['class' => 'form-control', 'required' => 'required'])
                    }}
                    <small class="text-danger">{{ $errors->first('number') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('assigned') ? ' has-error' : '' }}">
                {{ Form::label('assigned', 'Assigned Date', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::date('assigned', null, ['class' => 'form-control', 'required' => 'required'])
                    }}
                    <small class="text-danger">{{ $errors->first('assigned') }}</small>
                </div>
            </div>
            <div class="form-group">
                <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
                    <label> Status</label>
                    {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
                    <label for="is_active"> </label>
                </div>
                <small class="text-danger">{{ $errors->first('is_active') }}</small>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                {{ Form::label('note', 'Note', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::textarea('note', null, ['class' => 'form-control', 'required' => 'required','rows'=>'7'])
                    }}
                    <small class="text-danger">{{ $errors->first('note') }}</small>
                </div>
            </div>
        </div>
    </div>
</div>
