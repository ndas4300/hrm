<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add ProfessionalCertificate')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('certification') ? ' has-error' : '' }}">
                {!! Form::label('certification', 'Certificate', ['class' => 'col-sm-9
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('certification',null, ['class' => 'form-control', 'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('certification') }}</small>
                </div>
            </div>
            <div
                class="form-group{{ $errors->has('certification_institute') ? ' has-error' : '' }}">
                {!! Form::label('certification_institute', 'Certification Institute', ['class'
                =>
                'col-sm-9
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('certification_institute',null, ['class' => 'form-control',
                    'required'
                    => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('certification_institute')}}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-body">
            <div
                class="form-group{{ $errors->has('certification_location') ? ' has-error' : '' }}">
                {!! Form::label('certification_location', 'Certification Location', ['class' =>
                'col-sm-9
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('certification_location',null, ['class' => 'form-control',
                    'required'
                    => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('certification_location')}}</small>
                </div>
            </div>
            <div class="row">
                <div
                    class="form-group{{ $errors->has('certification_start') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('certification_start', 'Certification Start', ['class' =>
                    'col-sm-10
                    control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::date('certification_start',null, ['class' => 'form-control',
                        'required' =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('certification_start')}}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('certification_end') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('certification_end', 'Certification End', ['class' => 'col-sm-10
                    control-label']) !!}
                    <div class="col-sm-10">
                        {!! Form::date('certification_end',null, ['class' => 'form-control',
                        'required'
                        =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('certification_end') }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
