
<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add New Employee')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="row">
                <div class="form-group{{ $errors->has('employee_no') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('employee_no', 'Employee No', ['class' => 'col-sm-9
                    control-label'])
                    !!}
                    <div>
                        {!! Form::text('employee_no', null, ['class' =>
                        'form-control',
                        'required' =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('employee_no') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('dob', 'Date Of Birth', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::date('dob', null, ['class' => 'form-control',
                        'required'
                        =>
                        'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('dob') }}</small>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('name', 'Name', ['class' => '
                    control-label'])
                    !!}
                    <div class="">
                        {!! Form::text('name', null, ['class' => 'form-control',
                        'required'
                        =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('bn_name') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('bn_name', 'Namne (Bangla)', ['class' => '
                    control-label'])
                    !!}
                    <div class="">
                        {!! Form::text('bn_name', null, ['class' => 'form-control',
                        'required' =>
                        'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('bn_name')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('father') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('father', 'Father Name', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::text('father', null, ['class' => 'form-control'])
                        !!}
                        <small class="text-danger">{{ $errors->first('father')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('mother') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('mother', 'Mother Name', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::text('mother', null, ['class' => 'form-control'])
                        !!}
                        <small class="text-danger">{{ $errors->first('mother')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('driving_license') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('driving_license', 'Driving License', ['class' =>
                    '
                    control-label'])
                    !!}
                    <div class="">
                        {!! Form::file('driving_license',['class'=>'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('driving_license')
                                                    }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('image', 'Image', ['class' => '
                    control-label'])
                    !!}
                    <div class="">
                        {!! Form::file('image', ['required' => 'required','class'=>'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('image') }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="row">
                <div
                    class="form-group{{ $errors->has('gender_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('gender_id', 'Gender', ['class' => '
                    control-label']) !!}
                    <div class="col-sm-12">
                        {!! Form::select('gender_id', $genders, null, ['id' =>
                        'gender_id',
                        'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('gender_id')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('spouse') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('spouse', 'Spouse Name', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::text('spouse', null, ['class' => 'form-control',
                        'required' =>
                        'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('spouse')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('maritial_status_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('maritial_status_id', 'Maritial Status', ['class' =>
                    '
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('maritial_status_id', $maritialStatus, null,
                        ['id'
                        =>
                        'maritial_status_id',
                        'class' => 'form-control select2', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{
                                                    $errors->first('maritial_status_id')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('status_id', 'Employee Status', ['class' =>
                    '
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('status_id', $status, null, ['id' =>
                        'status_id', 'class' => 'form-control select2', 'required' =>
                        'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('status_id')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('blood_group_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('blood_group_id', 'Blood Group', ['class' =>
                    '
                    control-label'])
                    !!}
                    <div class="">
                        {!! Form::select('blood_group_id', $bloodGroup, null, ['id' =>
                        'blood_group_id',
                        'class'
                        => 'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('blood_group_id')
                                                    }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('religion_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('religion_id', 'Religion', ['class' => '
                    control-label'])
                    !!}
                    <div class="">
                        {!! Form::select('religion_id', $religion, null, ['id' =>
                        'religion_id', 'class'
                        => 'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('religion_id')
                                                    }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('nid') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('nid', 'NID', ['class' => ' control-label'])
                    !!}
                    <div class="">
                        {!! Form::file('nid', ['required' => 'required','class'=>'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('nid') }}</small>
                    </div>
                </div>
                <div
                    class="form-group{{ $errors->has('passport') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('passport', 'Passport', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::file('passport',['class'=>'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('passport')
                                                    }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

