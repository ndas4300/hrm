<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add EmployeeStatus')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                {{ Form::label('status_id', 'Status', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::select('status_id', $status,null, ['class' => 'form-control select2', 'required' =>
                    'required']) }}
                    <small class="text-danger">{{ $errors->first('status_id') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                {{ Form::label('date', 'Date', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::date('date', null, ['class' => 'form-control', 'required' =>
                    'required']) }}
                    <small class="text-danger">{{ $errors->first('date') }}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                {{ Form::label('description', 'Description', ['class' => 'col-sm-9
                control-label']) }}
                <div class="col-sm-9">
                    {{ Form::textarea('description',null, ['class' => 'form-control',
                    'required' =>
                    'required','rows'=>'5']) }}
                    <small class="text-danger">{{ $errors->first('description') }}</small>
                </div>
            </div>
        </div>
    </div>
</div>
