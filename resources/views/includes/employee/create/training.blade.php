
<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add EmployeeTraining')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('training_title') ? ' has-error' : '' }}">
                {!! Form::label('training_title', 'Training Title', ['class' =>
                'col-sm-6 control-label']) !!}
                <div class="">
                    {!! Form::text('training_title',null, ['class' => 'form-control',
                    'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('training_title')}}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('topics_covered') ? ' has-error' : '' }}">
                {!! Form::label('topics_covered', 'Topics Covered', ['class' =>
                'col-sm-6 control-label']) !!}
                <div class="">
                    {!! Form::text('topics_covered',null, ['class' => 'form-control',
                    'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('topics_covered')}}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('training_institute') ? ' has-error' : '' }}">
                {!! Form::label('training_institute', 'Training Institute', ['class' =>
                'col-sm-6 control-label']) !!}
                <div class="">
                    {!! Form::text('training_institute',null, ['class' =>
                    'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('training_institute')}}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                {!! Form::label('country_id', 'Training Country', ['class' => 'col-sm-9
                control-label']) !!}
                <div class="">
                    {!! Form::select('country_id', $country,null, ['id' =>
                    'country_id', 'class' => 'form-control select2', 'required' =>
                    'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('country_id')}}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('training_year') ? ' has-error' : '' }}">
                {!! Form::label('training_year', 'Training Year', ['class' => 'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::number('training_year',null, ['class' => 'form-control',
                    'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('training_year')}}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('training_duration') ? ' has-error' : '' }}">
                {!! Form::label('training_duration', 'Training Duration', ['class' =>
                'col-sm-6 control-label']) !!}
                <div class="">
                    {!! Form::number('training_duration',null, ['class' =>
                    'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('training_duration')}}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('training_location') ? ' has-error' : '' }}">
                {!! Form::label('training_location', 'Training Location', ['class' =>
                'col-sm-6 control-label']) !!}
                <div class="">
                    {!! Form::text('training_location',null,['class' =>
                    'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('training_location')}}</small>
                </div>
            </div>
        </div>
    </div>
</div>
