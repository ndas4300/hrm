<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add Weekly Off')}}</h3>
</div>
<div class="row">
    <div class="col-sm-9">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('day_id') ? ' has-error' : '' }} col-sm-9">
                {!! Form::label('day_id', 'Weekly Off', ['class' => 'col-sm-12
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::select('day_id[]', $days,null, ['id' =>
                    'day_id', 'class' => 'select2', 'required' =>
                    'required','multiple' => 'multiple'])
                    !!}
                    <small class="text-danger">{{ $errors->first('day_id')}}</small>
                </div>
            </div>
        </div>
    </div>
</div>
