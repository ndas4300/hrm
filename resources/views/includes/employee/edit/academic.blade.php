<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Edit Academic')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card-body">

                <div class="form-group{{ $errors->has('education_id') ? ' has-error' : '' }}">
                    {!! Form::label('education_id', 'Education', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::select('education_id', $education, null, ['id' => 'education_id', 'class' =>
                        'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('education_id') }}</small>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('academic_result_id') ? ' has-error' : '' }}">
                    {!! Form::label('academic_result_id', 'Academic Result', ['class' => 'col-sm-9
                    control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::select('academic_result_id', $academicResult, null, ['id' =>
                        'academic_result_id', 'class' =>
                        'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('academic_result_id') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('marks') ? ' has-error' : '' }}">
                    {!! Form::label('marks', 'Marks', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::number('marks', null, ['class' => 'form-control', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('marks') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                    {!! Form::label('year', 'Year', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::number('year', null, ['class' => 'form-control', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('year') }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-info">
            <div class="card-body">

                <div class="form-group{{ $errors->has('degree') ? ' has-error' : '' }}">
                    {!! Form::label('degree', 'Degree Name', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('degree', null, ['class' => 'form-control', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('degree') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('major') ? ' has-error' : '' }}">
                    {!! Form::label('major', 'Major Name', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('major', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('major') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('institute') ? ' has-error' : '' }}">
                    {!! Form::label('institute', 'Institute Name', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('institute', null, ['class' => 'form-control', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('institute') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                    {!! Form::label('duration', 'Duration', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::number('duration', null, ['class' => 'form-control', 'required' =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('duration') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('achievement') ? ' has-error' : '' }}">
                    {!! Form::label('achievement', 'Achievement', ['class' => 'col-sm-9 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('achievement', null, ['class' => 'form-control', 'required' =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('achievement') }}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
