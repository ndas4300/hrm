<div class="card-header shadow p-3 mb-5 bg-light rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add EmployeeAddress')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-sm-6">
        <div class="card card-header">
            <h3 class="card-title card-primary font-weight-bold">{{ __('Present')}}</h3>
        </div>
        <div class="card card-body">
            <div
                class="form-group{{ $errors->has('pr_address_line_one') ? ' has-error' : '' }}">
                {!! Form::label('pr_address_line_one', ' Address Line 1',
                ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pr_address_line_one', $address ? $address->pr_address_line_one : '', ['class' =>
                    'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_address_line_one')
                                                    }}</small>
                </div>
            </div>
            <div
                class="form-group{{ $errors->has('pr_address_line_two') ? ' has-error' : '' }}">
                {!! Form::label('pr_address_line_two', ' Address Line 2',
                ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pr_address_line_two', $address ? $address->pr_address_line_two : '', ['class' =>
                    'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_address_line_two')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_phone_one') ? ' has-error' : '' }}">
                {!! Form::label('pr_phone_one', ' Phone Number 1', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::number('pr_phone_one', $address ? $address->pr_phone_one : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_phone_one')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_phone_two') ? ' has-error' : '' }}">
                {!! Form::label('pr_phone_two', ' Phone Number 2', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::number('pr_phone_two', $address ? $address->pr_phone_two : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_phone_two')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_email') ? ' has-error' : '' }}">
                {!! Form::label('pr_email', ' Email address', ['class'
                =>'col-sm-6
                control-label'])
                !!}
                <div class="">
                    {!! Form::email('pr_email', $address ? $address->pr_email : '', ['class' => 'form-control',
                    'required' =>
                    'required',
                    'placeholder' => 'eg: foo@bar.com']) !!}
                    <small class="text-danger">{{ $errors->first('pr_email') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_village') ? ' has-error' : '' }}">
                {!! Form::label('pr_village', ' Village', ['class' => 'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pr_village', $address ? $address->pr_village : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_village')
                                                    }}</small>
                </div>
            </div>
            <div
                class="form-group{{ $errors->has('pr_police_station') ? ' has-error' : '' }}">
                {!! Form::label('pr_police_station', ' Police Station', ['class'
                =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pr_police_station', $address ? $address->pr_police_station : '', ['class' =>
                    'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_police_station')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_post_office') ? ' has-error' : '' }}">
                {!! Form::label('pr_post_office', ' Post Office', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pr_post_office', $address ? $address->pr_post_office : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_post_office')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_city') ? ' has-error' : '' }}">
                {!! Form::label('pr_city', ' City', ['class' => 'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pr_city', $address ? $address->pr_city : '', ['class' => 'form-control',
                    'required' =>
                    'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('pr_city') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pr_country_id') ? ' has-error' : '' }}">
                {!! Form::label('pr_country_id', ' Country', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::select('pr_country_id', $country, $address ? $address->pr_country_id : '', ['id' =>
                    'pr_country_id',
                    'class' =>
                    'form-control select2', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('pr_country_id')
                                                    }}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-header">
            <h3 class="card-title card-primary font-weight-bold">{{ __('Permanent')}}</h3>
        </div>
        <div class="card card-body">
            <div
                class="form-group{{ $errors->has('pa_address_line_one') ? ' has-error' : '' }}">
                {!! Form::label('pa_address_line_one', ' Address Line 1',
                ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pa_address_line_one', $address ? $address->pa_address_line_one : '', ['class' =>
                    'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_address_line_one')
                                                    }}</small>
                </div>
            </div>
            <div
                class="form-group{{ $errors->has('pa_address_line_two') ? ' has-error' : '' }}">
                {!! Form::label('pa_address_line_two', ' Address Line 2',
                ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pa_address_line_two', $address ? $address->pa_address_line_two : '', ['class' =>
                    'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_address_line_two')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_phone_one') ? ' has-error' : '' }}">
                {!! Form::label('pa_phone_one', ' Phone Number 1', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::number('pa_phone_one', $address ? $address->pa_phone_one : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_phone_one')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_phone_two') ? ' has-error' : '' }}">
                {!! Form::label('pa_phone_two', ' Phone Number 2', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::number('pa_phone_two', $address ? $address->pa_phone_two : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_phone_two')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_email') ? ' has-error' : '' }}">
                {!! Form::label('pa_email', ' Email address', ['class'
                =>'col-sm-6
                control-label'])
                !!}
                <div class="">
                    {!! Form::email('pa_email', $address ? $address->pa_email : '', ['class' => 'form-control',
                    'required' =>
                    'required',
                    'placeholder' => 'eg: foo@bar.com']) !!}
                    <small class="text-danger">{{ $errors->first('pa_email') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_village') ? ' has-error' : '' }}">
                {!! Form::label('pa_village', ' Village', ['class' => 'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pa_village', $address ? $address->pa_village : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_village')
                                                    }}</small>
                </div>
            </div>
            <div
                class="form-group{{ $errors->has('pa_police_station') ? ' has-error' : '' }}">
                {!! Form::label('pa_police_station', ' Police Station',
                ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pa_police_station', $address ? $address->pa_police_station : '', ['class' =>
                    'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_police_station')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_post_office') ? ' has-error' : '' }}">
                {!! Form::label('pa_post_office', ' Post Office', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pa_post_office', $address ? $address->pa_post_office : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_post_office')
                                                    }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_city') ? ' has-error' : '' }}">
                {!! Form::label('pa_city', ' City', ['class' => 'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::text('pa_city', $address ? $address->pa_city : '', ['class' => 'form-control',
                    'required' =>
                    'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('pa_city') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('pa_country_id') ? ' has-error' : '' }}">
                {!! Form::label('pa_country_id', ' Country', ['class' =>
                'col-sm-6
                control-label']) !!}
                <div class="">
                    {!! Form::select('pa_country_id', $country, $address ? $address->pa_country_id : '', ['id' =>
                    'pa_country_id',
                    'class' =>
                    'form-control select2', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('pa_country_id')
                                                    }}</small>
                </div>
            </div>
        </div>
    </div>
</div>
