<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Edit Earn Leaves')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-md-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                {{ Form::label('date', 'Date', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::date('date', null, ['class' => 'form-control', 'required' => 'required']) }}
                    <small class="text-danger">{{ $errors->first('date') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('balance') ? ' has-error' : '' }}">
                {{ Form::label('balance', 'Balance', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::number('balance', null, ['class' => 'form-control', 'required' => 'required'])
                    }}
                    <small class="text-danger">{{ $errors->first('balance') }}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('next_schedule') ? ' has-error' : '' }}">
                {{ Form::label('next_schedule', 'Next Schedule', ['class' => 'col-sm-9 control-label']) }}
                <div class="col-sm-9">
                    {{ Form::date('next_schedule', null, ['class' => 'form-control', 'required' =>
                    'required']) }}
                    <small class="text-danger">{{ $errors->first('next_schedule') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('previous_balance') ? ' has-error' : '' }}">
                {{ Form::label('previous_balance', 'Previous Balance', ['class' => 'col-sm-9 control-label'])
                }}
                <div class="col-sm-9">
                    {{ Form::number('previous_balance', null, ['class' => 'form-control', 'required' =>
                    'required']) }}
                    <small class="text-danger">{{ $errors->first('previous_balance') }}</small>
                </div>
            </div>
        </div>
    </div>
</div>
