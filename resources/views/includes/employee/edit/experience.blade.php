<div class="card-header shadow p-3 mb-5 bg-light rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add EmployeeExperience')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                {!! Form::label('company', 'Company Name', ['class' => 'col-sm-8
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('company', $experience ? $experience->company : '', ['class' => 'form-control', 'required'
                    => 'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('company') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('business') ? ' has-error' : '' }}">
                {!! Form::label('business', 'Business', ['class' => 'col-sm-8
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('business', $experience ? $experience->business : '', ['class' => 'form-control', 'required'
                    => 'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('business') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                {!! Form::label('designation', 'Designation', ['class' => 'col-sm-8
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('designation', $experience ? $experience->designation : '', ['class' => 'form-control',
                    'required' =>
                    'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('designation') }}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('area_of_experience') ? ' has-error' : '' }}">
                {!! Form::label('area_of_experience', 'Area of Experience', ['class' =>
                'col-sm-8
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('area_of_experience', $experience ? $experience->area_of_experience : '', ['class' => 'form-control',
                    'required' =>
                    'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('area_of_experience')}}</small>
                </div>
            </div>
            <div
                class="form-group{{ $errors->has('experience_location') ? ' has-error' : '' }}">
                {!! Form::label('experience_location', 'Experience Location', ['class' =>
                'col-sm-8
                control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::text('experience_location', $experience ? $experience->experience_location : '', ['class' => 'form-control',
                    'required' =>
                    'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('experience_location')}}</small>
                </div>
            </div>
            <div class="row">
                <div class="form-group{{ $errors->has('experience_start') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('experience_start', 'Experience Start', ['class' =>
                    'col-sm-8
                    control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::date('experience_start', $experience ? $experience->experience_start : '', ['class' => 'form-control',
                        'required' =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('experience_start')}}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('experience_end') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('experience_end', 'Experience End', ['class' => 'col-sm-8
                    control-label'])
                    !!}
                    <div class="col-sm-9">
                        {!! Form::date('experience_end', $experience ? $experience->experience_end : '', ['class' => 'form-control',
                        'required' =>
                        'required']) !!}
                        <small class="text-danger">{{ $errors->first('experience_end')}}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
