<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add EmployeeOfficial')}}</h3>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="form-group{{ $errors->has('joining_date') ? ' has-error' : '' }} col-sm-10">
                {!! Form::label('joining_date', 'Joining Date', ['class' => '
                control-label']) !!}
                <div class="">
                    {!! Form::date('joining_date', $official ? $official->joining_date : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('joining_date')}}</small>
                </div>
            </div>
            <div class="row">
                <div class="form-group{{ $errors->has('unit_id') ? ' has-error' : '' }} col-sm-4">
                    {!! Form::label('unit_id', 'Unit', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('unit_id', $unit, $official ? $official->unit_id : '', ['id' => 'unit_id', 'class'
                        => 'form-control select2',
                        'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('unit_id') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('floor_id') ? ' has-error' : '' }} col-sm-4">
                    {!! Form::label('floor_id', 'Floor', ['class' => 'col-sm-8
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('floor_id',$floor, $official ? $official->floor_id : '', ['id' => 'floor_id',
                        'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('floor_id') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('section_id') ? ' has-error' : '' }} col-sm-4">
                    {!! Form::label('section_id', 'Section', ['class' => 'col-sm-8
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('section_id', $section, $official ? $official->section_id : '', ['id' =>
                        'section_id', 'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('section_id')}}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group{{ $errors->has('line_id') ? ' has-error' : '' }} col-sm-4">
                    {!! Form::label('line_id', 'Line', ['class' => 'col-sm-8
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('line_id',$line, $official ? $official->line_id : '', ['id' => 'line_id', 'class'
                        => 'form-control select2',
                        'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('line_id') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('grade_id') ? ' has-error' : '' }} col-sm-4">
                    {!! Form::label('grade_id', 'Grade', ['class' => 'col-sm-9
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('grade_id', $grade, $official ? $official->grade_id : '', ['id' => 'grade_id',
                        'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('grade_id') }}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('shift_id') ? ' has-error' : '' }} col-sm-4">
                    {!! Form::label('shift_id', 'Shift', ['class' => 'col-sm-9
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('shift_id',$shift, $official ? $official->shift_id : '', ['id' => 'shift_id',
                        'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('shift_id') }}</small>
                    </div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('gross') ? ' has-error' : '' }}">
                {!! Form::label('gross', 'Gross', ['class' => 'col-sm-9 control-label'])
                !!}
                <div class="">
                    {!! Form::number('gross', $official ? $official->gross : '', ['class' => 'form-control',
                    'required' => 'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('gross') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('fixed') ? ' has-error' : '' }}">
                {!! Form::label('fixed', 'Fixed', ['class' => 'col-sm-9 control-label'])
                !!}
                <div class="">
                    {!! Form::number('fixed', $official ? $official->fixed : '', ['class' => 'form-control',
                    'required' => 'required'])
                    !!}
                    <small class="text-danger">{{ $errors->first('fixed') }}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('medical_amount') ? ' has-error' : '' }}">
                {!! Form::label('medical_amount', 'Medical Amount', ['class' =>
                'col-sm-9 control-label'])
                !!}
                <div class="">
                    {!! Form::number('medical_amount', $official ? $official->medical_amount : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('medical_amount')}}</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-body">
            <div class="row">
                <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('department_id', 'Department', ['class' => '
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('department_id',$department, $official ? $official->department_id : '', ['id' =>
                        'department_id', 'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('department_id')}}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('designation_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('designation_id', 'Designation', ['class' => 'col-sm-9
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('designation_id',$designation, $official ? $official->designation_id : '', ['id' =>
                        'designation_id', 'class'
                        => 'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('designation_id')}}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group{{ $errors->has('calendar_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('calendar_id', 'Calendar', ['class' => 'col-sm-9
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('calendar_id', $calendar, $official ? $official->calendar_id : '', ['id' =>
                        'calendar_id', 'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('calendar_id')}}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('employee_type_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('employee_type_id', 'Employee Type', ['class' =>
                    'col-sm-9 control-label'])
                    !!}
                    <div class="">
                        {!! Form::select('employee_type_id', $type, $official ? $official->employee_type_id : '', ['id' =>
                        'employee_type_id',
                        'class' => 'form-control select2', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('employee_type_id')}}</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group{{ $errors->has('leave_category_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('leave_category_id', 'Leave Category', ['class' =>
                    'col-sm-9
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('leave_category_id',$leaveCategory, $official ? $official->leave_category_id : '', ['id' =>
                        'leave_category_id',
                        'class' => 'form-control select2', 'required' => 'required'])
                        !!}
                        <small class="text-danger">{{ $errors->first('leave_category_id')}}</small>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('transport_id') ? ' has-error' : '' }} col-sm-6">
                    {!! Form::label('transport_id', 'Transport', ['class' => 'col-sm-9
                    control-label']) !!}
                    <div class="">
                        {!! Form::select('transport_id', $transport, $official ? $official->transport_id : '', ['id' =>
                        'transport_id', 'class' =>
                        'form-control select2', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('transport_id')}}</small>
                    </div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('transport_amount') ? ' has-error' : '' }}">
                {!! Form::label('transport_amount', 'Transport Amount') !!}
                {!! Form::number('transport_amount', $official ? $official->transport_amount : '', ['class' => 'form-control',
                'required' =>
                'required']) !!}
                <small class="text-danger">{{ $errors->first('transport_amount')
                                            }}</small>
            </div>
            <div class="form-group{{ $errors->has('house_rent') ? ' has-error' : '' }}">
                {!! Form::label('house_rent', 'House Rent', ['class' => 'col-sm-9
                control-label']) !!}
                <div class="">
                    {!! Form::number('house_rent', $official ? $official->house_rent : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('house_rent')}}</small>
                </div>
            </div>
            <div class="form-group{{ $errors->has('other_amount') ? ' has-error' : '' }}">
                {!! Form::label('other_amount', 'Other Amount', ['class' => 'col-sm-9
                control-label']) !!}
                <div class="">
                    {!! Form::number('other_amount', $official ? $official->other_amount : '', ['class' => 'form-control',
                    'required' =>
                    'required']) !!}
                    <small class="text-danger">{{ $errors->first('other_amount')}}</small>
                </div>
            </div>
        </div>
    </div>
</div>
