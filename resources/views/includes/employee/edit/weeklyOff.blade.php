<div class="card-header shadow p-3 mb-5 bg-white rounded">
    <h3 class="card-title font-weight-bold">{{ __('Add Weekly Off')}}</h3>
</div>
<div class="card card-body">
    <div class="form-group{{ $errors->has('day_id') ? ' has-error' : '' }}">
        {{ Form::label('day_id', 'Weekly Off', ['class' => 'col-sm-9 control-label']) }}
        <div class="col-sm-9">
            {{ Form::select('day_id[]', $days, $employee->days ? $employee->days : '', ['id' => 'day_id', 'class' => 'form-control select2',
            'required' => 'required', 'multiple']) }}
            <small class="text-danger">{{ $errors->first('day_id') }}</small>
        </div>
    </div>
</div>
