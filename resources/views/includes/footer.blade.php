<footer class="main-footer no_print">
    <strong>{{ __('Copyright')}} &copy; {{ __('2021-2022')}} <a href="https://webpointbd.com/" target="_blank">Web Point Ltd</a>.</strong>
    {{ __('All rights reserved.')}}
    <div class="float-right d-none d-sm-inline-block">
      <b>{{ __('Version')}}</b> {{ __('1.0.0')}}
    </div>
  </footer>
