<div class="card-body">
    <div class="form-group">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control',
            'required' => 'required']) }}
            <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="form-group">
        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            {{ Form::label('description', 'Description') }}
            {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class'
            => 'form-control']) }}
            <small class="text-danger">{{ $errors->first('description') }}</small>
        </div>
    </div>
    <div class="form-group">
        <div class="toggle{{ $errors->has('is_active') ? ' has-error' : '' }}">
            <label> {{ __('Status')}}</label>
            {{ Form::checkbox('is_active', '1', null, ['id' => 'is_active']) }}
            <label for="is_active"> </label>
        </div>
        <small class="text-danger">{{ $errors->first('is_active') }}</small>
    </div>
</div>
<div class="card-footer">
    <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
</div>
