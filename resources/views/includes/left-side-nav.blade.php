<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{ asset('lte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{ __('AdminLTE 3')}}</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('lte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item ">
                    <a href="{{ route('home') }}"
                        class="nav-link  {{ request()->is('dashboard') ? 'nav-link active' : null }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ __('Dashboard')}}
                        </p>
                    </a>
                </li>
                <li class="nav-item  {{ request()->is('employee') || request()->is('inactiveEmployee*') || request()->is('diciplinaryManagement*') || request()->is('manageLeave*') || request()->is('idCard*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('employee') || request()->is('inactiveEmployee*') || request()->is('diciplinaryManagement*') || request()->is('manageLeave*') || request()->is('idCard*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            {{ __('Employee')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('employee.index')}}"
                                class="nav-link  {{ request()->is('employee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('List')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('inactiveEmployee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Inactive Employee')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('diciplinaryManagement') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Diciplinary Management')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('manageLeave') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Manage Leave')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('idCard') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('ID Card')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                {{-- <li class="nav-item {{ request()->is('report') ? 'active' : null }}">
                    <a href="{{route('report')}}"
                        class="nav-link {{ request()->is('report') ? 'active' : null }}">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            {{ __('Report')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                </li> --}}
                <li class="nav-item  {{ request()->is('monthlyreport') || request()->is('datelyreport') || request()->is('departmentlyreport') || request()->is('datelylatereport') || request()->is('dailyouttimereport') || request()->is('dailyintimereport') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('monthlyreport') || request()->is('datelyreport') || request()->is('departmentlyreport') || request()->is('datelylatereport') || request()->is('dailyouttimereport') || request()->is('dailyintimereport') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon far fa-file"></i>
                        <p>
                            {{ __('Reports')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('monthlyreport') }}"
                                class="nav-link  {{ request()->is('monthlyreport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Monthly Report')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('datewisereport')}}"
                                class="nav-link  {{ request()->is('datelyreport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Date wise Report')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('departmentwisereport')}}"
                                class="nav-link  {{ request()->is('departmentlyreport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Department wise report')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('datewiselatereport')}}"
                                class="nav-link  {{ request()->is('datelylatereport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Date wise late report')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('dailyouttimereport')}}"
                                class="nav-link  {{ request()->is('dailyouttimereport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Daily out time report')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('dailyintimereport')}}"
                                class="nav-link  {{ request()->is('dailyintimereport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Daily in time report')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  {{  request()->is('presentEmplyee*') || request()->is('absentEmplyee*') || request()->is('lateEmployee*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{  request()->is('presentEmployee*') || request()->is('absentEmplyee*') || request()->is('lateEmployee*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon fas fa-business-time"></i>
                        <p>
                            {{ __('Daily Time Sheet')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('allEmployee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('All Employee')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('presentEmplyee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Present Emplyee')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('absentEmployee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Absent Employee')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('lateEmployee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Late Employee')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li class="nav-item  {{ request()->is('manualInTime*') || request()->is('manualOutTime*') || request()->is('bulkInOutTime*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('manualInTime*') || request()->is('manualOutTime*') || request()->is('bulkInOutTime*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon far fa-list-alt"></i>
                        <p>
                            {{ __('Attendace')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('manualIn')}}"
                                class="nav-link  {{ request()->is('manualInTime') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Manual In Time')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('manualOut')}}"
                                class="nav-link  {{ request()->is('manualOutTime') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Manual Out Time')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('bulkInOutTime') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Bulk In/Outn Time')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  {{ request()->is('shift*') || request()->is('leave-category*') || request()->is('floor*') || request()->is('line*') || request()->is('grade*') || request()->is('holidaySetup*') || request()->is('employee-status*') || request()->is('department*') || request()->is('designation*') || request()->is('district*') || request()->is('calendar*') || request()->is('employee-type*') || request()->is('appraisalCategory*') || request()->is('transport*') || request()->is('division*') || request()->is('section*') || request()->is('weeklyOff*') || request()->is('shiftSetting*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('shift*') || request()->is('leaveCategory*') || request()->is('floor*') || request()->is('line*') || request()->is('grade*') || request()->is('holidaySetup*') || request()->is('employee-status*') || request()->is('department*') || request()->is('designation*') || request()->is('district*') || request()->is('calendar*') || request()->is('employee-type*') || request()->is('appraisalCategory*') || request()->is('transport*') || request()->is('division*') || request()->is('section*') || request()->is('weeklyOff*') || request()->is('shiftSetting*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon far fa-building"></i>
                        <p>
                            {{ __('Office Setup')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('shift.index') }}"
                                class="nav-link  {{ request()->is('shift') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Shift Group')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('leaveCategory.index') }}"
                                class="nav-link  {{ request()->is('leave-category') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Leave Categroy')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('floor.index') }}"
                                class="nav-link  {{ request()->is('floor') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Floor Assign')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('line.index') }}"
                                class="nav-link  {{ request()->is('line') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Line Assigne')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('grade.index') }}"
                                class="nav-link  {{ request()->is('grade') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Grade')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('holidaySetup') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Holiday Setup')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('employeeStatus.index') }}"
                                class="nav-link  {{ request()->is('employee-status') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Employee Statuses')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('department.index') }}"
                                class="nav-link  {{ request()->is('department') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Department')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('designation.index') }}"
                                class="nav-link  {{ request()->is('designation') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Designation')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('district') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('District')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('calendar.index') }}"
                                class="nav-link  {{ request()->is('calendar') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Calendar Code')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('employeeType.index') }}"
                                class="nav-link  {{ request()->is('employee-type') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Employee Type')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('appraisalCategory') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Appraisal Category')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('transport.index') }}"
                                class="nav-link  {{ request()->is('transport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Transport Location')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('dashboard/list') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Division')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('section.index') }}"
                                class="nav-link  {{ request()->is('section') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Section')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('employeeWeeklyOff') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Weekly Off')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('shift.index') }}"
                                class="nav-link  {{ request()->is('shiftSetting') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Shift Setting')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li class="nav-item  {{ request()->is('gender*') || request()->is('blood-group*') || request()->is('nationality*') || request()->is('marital-status*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link  {{ request()->is('gender*') || request()->is('blood-group*') || request()->is('nationality*') || request()->is('marital-status*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon fas fa-user-plus"></i>
                        <p>
                            {{ __('Employee Setup')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('gender.index') }}"
                                class="nav-link  {{ request()->is('gender') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Gender')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('bloodGroup.index') }}"
                                class="nav-link  {{ request()->is('blood-group') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Blood Group')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('maritialStatus.index') }}"
                                class="nav-link  {{ request()->is('marital-status') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Maritial Status')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('nationality') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Nationality')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li class="nav-item  {{ request()->is('smsGateway*') || request()->is('smtp*') || request()->is('company*') || request()->is('Language*') || request()->is('currency*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('smsGateway*') || request()->is('smtp*') || request()->is('company*') || request()->is('Language*') || request()->is('currency*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon fas fa-chart-bar"></i>
                        <p>
                            {{ __('Business Setup')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('smsGateway') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('SMS Gateway')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('smtp') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('SMTP')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('company') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Company')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('Language') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Language')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('currency') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Currency')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li class="nav-item  {{ request()->is('lineWise*') || request()->is('departmentWise*') || request()->is('sectionWise*') || request()->is('sectionWiseOt*') || request()->is('inactiveEmployee*') || request()->is('individualAttendance*') || request()->is('dailyOutTimeNull*') || request()->is('dailyOutTimeNullStaff*') || request()->is('dailyManpowerReport*') || request()->is('monthlyLeave*') || request()->is('manualAttendance*') || request()->is('lateReport*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('lineWise*') || request()->is('departmentWise*') || request()->is('sectionWise*') || request()->is('sectionWiseOt*') || request()->is('inactiveEmployee*') || request()->is('individualAttendance*') || request()->is('dailyOutTimeNull*') || request()->is('dailyOutTimeNullStaff*') || request()->is('dailyManpowerReport*') || request()->is('monthlyLeave*') || request()->is('manualAttendance*') || request()->is('lateReport*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            {{ __('Report')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('line.index') }}"
                                class="nav-link  {{ request()->is('lineWise') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Line Wise')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('departmentWise') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Department Wise')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('sectionWise') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Section Wise')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('sectionWiseOt') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Section Wise (OT)')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('inactiveEmployee') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Inactive Employee')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('individualAttendance') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Individual Attendace')}}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('dailyOutTimeNull') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Daily Oute Time Null')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('dailyOutTimeNullStaff') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Daily Out Time Null (Staff)')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('dailyManpowerReport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Daily Manpower Report')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('monthlyLeave') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Monthly Leave')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('manualAttendance') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Manudal Attendance')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#"
                                class="nav-link  {{ request()->is('lateReport') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Late Report')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                 <li class="nav-item  {{ request()->is('unitWise*') || request()->is('sectionWise*') || request()->is('lineWise*') ? 'nav-item active menu-open' : null }}">
                    <a href="javascript:"
                        class="nav-link {{ request()->is('unitWise*') || request()->is('sectionWise*') || request()->is('lineWise*') ? 'nav-link active menu-open' : null }}">
                        <i class="nav-icon fas fa-clock"></i>
                        <p>
                           {{ __(' Time Card')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('unit.index') }}"
                                class="nav-link  {{ request()->is('unitWise') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Unit Wise')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('section.index') }}"
                                class="nav-link  {{ request()->is('sectionWise') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Section Wise')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('line.index') }}"
                                class="nav-link  {{ request()->is('lineWise') ? 'active' : null }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ __('Line Wise')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>
