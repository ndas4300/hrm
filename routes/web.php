<?php

use App\Http\Controllers\Employee\LeaveSetupController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\Office\LineController;
use App\Http\Controllers\Office\UnitController;
use App\Http\Controllers\Employee\DayController;
use App\Http\Controllers\Office\FloorController;
use App\Http\Controllers\Office\GradeController;
use App\Http\Controllers\CalendarEventController;
use App\Http\Controllers\Employee\CardController;
use App\Http\Controllers\Employee\LeaveController;
use App\Http\Controllers\Employee\ShiftController;
use App\Http\Controllers\Office\SectionController;
use App\Http\Controllers\Employee\GenderController;
use App\Http\Controllers\Employee\StatusController;
use App\Http\Controllers\Employee\EmployeeController;
use App\Http\Controllers\Employee\ReligionController;
use App\Http\Controllers\Office\DepartmentController;
use App\Http\Controllers\Employee\EarnLeaveController;
use App\Http\Controllers\Employee\EducationController;
use App\Http\Controllers\Employee\TransportController;
use App\Http\Controllers\Office\DesignationController;
use App\Http\Controllers\Employee\AttendanceController;
use App\Http\Controllers\Employee\BloodGroupController;
use App\Http\Controllers\Office\EmployeeTypeController;
use App\Http\Controllers\Employee\LeaveCategoryController;
use App\Http\Controllers\Employee\AcademicResultController;
use App\Http\Controllers\Employee\EmployeeStatusController;
use App\Http\Controllers\Employee\MaritialStatusController;
use App\Http\Controllers\Employee\EmployeeAddressController;
use App\Http\Controllers\Employee\EmployeeAcademicController;
use App\Http\Controllers\Employee\EmployeeOfficialController;
use App\Http\Controllers\Employee\EmployeeTrainingController;
use App\Http\Controllers\Employee\EmployeeWeeklyOffController;
use App\Http\Controllers\Employee\EmployeeExperienceController;
use App\Http\Controllers\Employee\ProfessionalCertificateController;
use App\Http\Controllers\ManualController;
use App\Http\Controllers\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});
//Software Setup Routes
Route::get('reboot', function () {
    Artisan::call('optimize:clear');
    dd('Done');
});
Route::get('migrate', function () {
    Artisan::call('migrate:fresh --seed');
    dd('Done');
});

Route::middleware(['auth', 'verified'])->group(function () {

    Route::get('/', function () {
        return view('welcome');
    })->name('home');

    // Employee Routes

        Route::resource('gender', GenderController::class);//ok
        Route::resource('religion', ReligionController::class);//others
        Route::resource('employee', EmployeeController::class);//ok
        Route::resource('leave', LeaveController::class);
        Route::resource('day', DayController::class);
        Route::resource('card', CardController::class);
        Route::resource('status', StatusController::class);
        Route::resource('shift', ShiftController::class);
        Route::resource('transport', TransportController::class);
        Route::resource('education', EducationController::class);
        Route::resource('attendance', AttendanceController::class);



        // Blood Group
        Route::get('blood-group',[BloodGroupController::class,'index'])->name('bloodGroup.index');
        Route::post('blood-group',[BloodGroupController::class,'store'])->name('bloodGroup.store');
        Route::put('blood-group/{bloodGroup}',[BloodGroupController::class,'update'])->name('bloodGroup.update');
        Route::delete('blood-group/{bloodGroup}',[BloodGroupController::class,'destroy'])->name('bloodGroup.destroy');
        // Route::get('blood-group/create',[BloodGroupController::class,'create'])->name('bloodGroup.create');
        // Route::get('blood-group/{bloodGroup}/edit',[BloodGroupController::class,'edit'])->name('bloodGroup.edit');
        // Blood Group

        // Maritial Status
        Route::get('marital-status',[MaritialStatusController::class,'index'])->name('maritialStatus.index');
        Route::post('marital-status',[MaritialStatusController::class,'store'])->name('maritialStatus.store');
        Route::put('marital-status/{maritialStatus}',[MaritialStatusController::class,'update'])->name('maritialStatus.update');
        Route::delete('marital-status/{maritialStatus}',[MaritialStatusController::class,'destroy'])->name('maritialStatus.destroy');
        // Maritial Status

        // Leave Category
        Route::get('leave-category',[LeaveCategoryController::class,'index'])->name('leaveCategory.index');
        Route::post('leave-category',[LeaveCategoryController::class,'store'])->name('leaveCategory.store');
        Route::put('leave-category/{leaveCategory}',[LeaveCategoryController::class,'update'])->name('leaveCategory.update');
        Route::delete('leave-category/{leaveCategory}',[LeaveCategoryController::class,'destroy'])->name('leaveCategory.destroy');

        // Leave Setup
        Route::get('leave-setup',[LeaveSetupController::class,'index'])->name('leaveSetup.index');
        Route::post('leave-setup',[LeaveSetupController::class,'store'])->name('leaveSetup.store');
        Route::put('leave-setup/{leaveSetup}',[LeaveSetupController::class,'update'])->name('leaveSetup.update');
        Route::delete('leave-setup/{leaveSetup}',[LeaveSetupController::class,'destroy'])->name('leaveSetup.destroy');
        // Leave Category

        // Earn Leave
        Route::get('earn-leave',[EarnLeaveController::class,'index'])->name('earnLeave.index');
        Route::post('earn-leave',[EarnLeaveController::class,'store'])->name('earnLeave.store');
        Route::put('earn-leave/{earnLeave}',[EarnLeaveController::class,'update'])->name('earnLeave.update');
        Route::delete('earn-leave/{earnLeave}',[EarnLeaveController::class,'destroy'])->name('earnLeave.destroy');
        Route::get('earn-leave/{earn-leave}/edit',[EarnLeaveController::class,'edit'])->name('earn-leave.edit');
        // Earn Leave

        // Employee Weekly Off
        Route::get('employee-weekly-off',[EmployeeWeeklyOffController::class,'index'])->name('employeeWeeklyOff.index');
        Route::post('employee-weekly-off',[EmployeeWeeklyOffController::class,'store'])->name('employeeWeeklyOff.store');
        Route::put('employee-weekly-off/{employeeWeeklyOff}',[EmployeeWeeklyOffController::class,'update'])->name('employeeWeeklyOff.update');
        Route::delete('employee-weekly-off/{employeeWeeklyOff}',[EmployeeWeeklyOffController::class,'destroy'])->name('employeeWeeklyOff.destroy');
        // Employee Weekly Off

        // EmployeeStatus
        Route::get('employee-status',[EmployeeStatusController::class,'index'])->name('employeeStatus.index');
        Route::post('employee-status',[EmployeeStatusController::class,'store'])->name('employeeStatus.store');
        Route::put('employee-status/{employeeStatus}',[EmployeeStatusController::class,'update'])->name('employeeStatus.update');
        Route::delete('employee-status/{employeeStatus}',[EmployeeStatusController::class,'destroy'])->name('employeeStatus.destroy');
        Route::get('employee-status/{employeeStatus}/edit',[EmployeeStatusController::class,'edit'])->name('employeeStatus.edit');
        // EmployeeStatus

        // Employee Training
        Route::get('employee-training',[EmployeeTrainingController::class,'index'])->name('employeeTraining.index');
        Route::post('employee-training',[EmployeeTrainingController::class,'store'])->name('employeeTraining.store');
        Route::put('employee-training/{employeeTraining}',[EmployeeTrainingController::class,'update'])->name('employeeTraining.update');
        Route::delete('employee-training/{employeeTraining}',[EmployeeTrainingController::class,'destroy'])->name('employeeTraining.destroy');
        Route::get('employee-training/{employeeTraining}/edit',[EmployeeTrainingController::class,'edit'])->name('employeeTraining.edit');
        // Employee Training

        // EmployeeOfficial
        Route::get('employee-official',[EmployeeOfficialController::class,'index'])->name('employeeOfficial.index');
        Route::post('employee-official',[EmployeeOfficialController::class,'store'])->name('employeeOfficial.store');
        Route::put('employee-official/{employeeOfficial}',[EmployeeOfficialController::class,'update'])->name('employeeOfficial.update');
        Route::delete('employee-official/{employeeOfficial}',[EmployeeOfficialController::class,'destroy'])->name('employeeOfficial.destroy');
        Route::get('employee-official/{employeeOfficial}/edit',[EmployeeOfficialController::class,'edit'])->name('employeeOfficial.edit');
        // EmployeeOfficial

        // Professional Certificate
        Route::get('professional-certificate',[ProfessionalCertificateController::class,'index'])->name('professionalCertificate.index');
        Route::post('professional-certificate',[ProfessionalCertificateController::class,'store'])->name('professionalCertificate.store');
        Route::put('professional-certificate/{professionalCertificate}',[ProfessionalCertificateController::class,'update'])->name('professionalCertificate.update');
        Route::delete('professional-certificate/{professionalCertificate}',[ProfessionalCertificateController::class,'destroy'])->name('professionalCertificate.destroy');
        Route::get('professional-certificate/{professionalCertificate}/edit',[ProfessionalCertificateController::class,'edit'])->name('professionalCertificate.edit');
        // Professional Certificate

        // Employee Address
        Route::get('employee-address',[EmployeeAddressController::class,'index'])->name('employeeAddress.index');
        Route::post('employee-address',[EmployeeAddressController::class,'store'])->name('employeeAddress.store');
        Route::put('employee-address/{employeeAddress}',[EmployeeAddressController::class,'update'])->name('employeeAddress.update');
        Route::delete('employee-address/{employeeAddress}',[EmployeeAddressController::class,'destroy'])->name('employeeAddress.destroy');
        Route::get('employee-address/{employeeAddress}/edit',[EmployeeAddressController::class,'edit'])->name('employeeAddress.edit');
        // Employee Address

        // Employee Experience
        Route::get('employee-experience',[EmployeeExperienceController::class,'index'])->name('employeeExperience.index');
        Route::post('employee-experience',[EmployeeExperienceController::class,'store'])->name('employeeExperience.store');
        Route::put('employee-experience/{employeeExperience}',[EmployeeExperienceController::class,'update'])->name('employeeExperience.update');
        Route::delete('employee-experience/{employeeExperience}',[EmployeeExperienceController::class,'destroy'])->name('employeeExperience.destroy');
        Route::get('employee-experience/{employeeExperience}/edit',[EmployeeExperienceController::class,'edit'])->name('employeeExperience.edit');
        // Employee Experience

        // Employee Academic
        Route::get('employee-academic',[EmployeeAcademicController::class,'index'])->name('employeeAcademic.index');
        Route::post('employee-academic',[EmployeeAcademicController::class,'store'])->name('employeeAcademic.store');
        Route::put('employee-academic/{employeeAcademic}',[EmployeeAcademicController::class,'update'])->name('employeeAcademic.update');
        Route::delete('employee-academic/{employeeAcademic}',[EmployeeAcademicController::class,'destroy'])->name('employeeAcademic.destroy');
        Route::get('employee-academic/{employeeAcademic}/edit',[EmployeeAcademicController::class,'edit'])->name('employeeAcademic.edit');
        // Employee Academic

        // Employee Type
        Route::get('employee-type',[EmployeeTypeController::class,'index'])->name('employeeType.index');
        Route::post('employee-type',[EmployeeTypeController::class,'store'])->name('employeeType.store');
        Route::put('employee-type/{employeeType}',[EmployeeTypeController::class,'update'])->name('employeeType.update');
        Route::delete('employee-type/{employeeType}',[EmployeeTypeController::class,'destroy'])->name('employeeType.destroy');
        Route::get('employee-type/{employeeType}/edit',[EmployeeTypeController::class,'edit'])->name('employeeType.edit');
        // Employee Type

        // Academic Result
        Route::get('academic-result',[AcademicResultController::class,'index'])->name('academicResult.index');
        Route::post('academic-result',[AcademicResultController::class,'store'])->name('academicResult.store');
        Route::put('academic-result/{academicResult}',[AcademicResultController::class,'update'])->name('academicResult.update');
        Route::delete('academic-result/{academicResult}',[AcademicResultController::class,'destroy'])->name('academicResult.destroy');
        // Academic Result

        // Calendar Event
        Route::get('calendar-event',[CalendarEventController::class,'index'])->name('calendarEvent.index');
        Route::post('calendar-event',[CalendarEventController::class,'store'])->name('calendarEvent.store');
        Route::put('calendar-event/{calendarEvent}',[CalendarEventController::class,'update'])->name('calendarEvent.update');
        Route::delete('calendar-event/{calendarEvent}',[CalendarEventController::class,'destroy'])->name('calendarEvent.destroy');
        Route::get('calendar-event/{calendarEvent}/edit',[CalendarEventController::class,'edit'])->name('calendarEvent.edit');

        // Calendar Event

        // Manual In/Out
        Route::post('manual-in/search',[ManualController::class,'searchIn'])->name('searchIn');
        Route::post('manual-in/store',[ManualController::class,'storeIn'])->name('storeIn');
        Route::get('manual-in',[ManualController::class,'manualIn'])->name('manualIn');

        Route::post('manual-out/search',[ManualController::class,'searchOut'])->name('searchOut');
        Route::post('manual-out/store',[ManualController::class,'storeOut'])->name('storeOut');
        Route::get('manual-out',[ManualController::class,'manualOut'])->name('manualOut');
        // Manual In/Out


    // Office Routes
    Route::resource('unit', UnitController::class);
    Route::resource('grade', GradeController::class);
    Route::resource('department', DepartmentController::class);
    Route::resource('designation', DesignationController::class);
    Route::resource('floor', FloorController::class);
    Route::resource('line', LineController::class);
    Route::resource('section', SectionController::class);


    Route::resource('calendar', CalendarController::class);

    // Status Routes
    Route::patch('day/status/{day}', [DayController::class, 'status'])->name('day.status');
    Route::patch('bloodGroup/status/{bloodGroup}', [BloodGroupController::class, 'status'])->name('bloodGroup.status');
    Route::patch('gender/status/{gender}', [GenderController::class, 'status'])->name('gender.status');
    Route::patch('maritialStatus/status/{maritialStatus}', [MaritialStatusController::class, 'status'])->name('maritialStatus.status');
    Route::patch('religion/status/{religion}', [ReligionController::class, 'status'])->name('religion.status');
    Route::patch('employee/status/{employee}', [EmployeeController::class, 'status'])->name('employee.status');
    Route::patch('leave/status/{leave}', [LeaveController::class, 'status'])->name('leave.status');
    Route::patch('leaveCategory/status/{leaveCategory}', [LeaveCategoryController::class, 'status'])->name('leaveCategory.status');
    Route::patch('unit/status/{unit}', [UnitController::class, 'status'])->name('unit.status');
    Route::patch('grade/status/{grade}', [GradeController::class, 'status'])->name('grade.status');
    Route::patch('department/status/{department}', [DepartmentController::class, 'status'])->name('department.status');
    Route::patch('designation/status/{designation}', [DesignationController::class, 'status'])->name('designation.status');
    Route::patch('employeeType/status/{employeeType}', [EmployeeTypeController::class, 'status'])->name('employeeType.status');
    Route::patch('floor/status/{floor}', [FloorController::class, 'status'])->name('floor.status');
    Route::patch('line/status/status/status/{line}', [LineController::class, 'status'])->name('line.status');
    Route::patch('section/status/{section}', [SectionController::class, 'status'])->name('section.status');
    Route::patch('card/status/{card}', [CardController::class, 'status'])->name('card.status');
    Route::patch('calendar/status/{calendar}', [CalendarController::class, 'status'])->name('calendar.status');
    Route::patch('calendarEvent/status/{calendarEvent}', [CalendarEventController::class, 'status'])->name('calendarEvent.status');
    Route::patch('status/status/{status}', [StatusController::class, 'status'])->name('status.status');
    Route::patch('transport/status/{transport}', [TransportController::class, 'status'])->name('transport.status');
    Route::patch('education/status/{education}', [EducationController::class, 'status'])->name('education.status');
    Route::patch('academicResult/status/{academicResult}', [AcademicResultController::class, 'status'])->name('academicResult.status');


    /************* Report Routes start **************/

    Route::controller(ReportController::class)->group(function(){
        Route::get('monthlyreport','get_monthlyview')->name('monthlyreport');
        Route::post('getmonthdata','get_monthdata')->name('get_monthdata');
        Route::get('monthlypdf','get_monthlypdf')->name('monthlypdf');
        /* ajex call for emp id */
        Route::get('getemp_id','getemp_id')->name('getemp_id');

        Route::get('datelyreport','get_datewiseview')->name('datewisereport');
        Route::post('getdatewisedata','get_datewiseedata')->name('get_datewiseedata');
        Route::get('datelypdf','get_datelypdf')->name('datelypdf');

        Route::get('departmentlyreport','get_departmentwiseview')->name('departmentwisereport');
        Route::post('departmentwise','get_departmentwisedata')->name('get_departmentwisedata');
        Route::get('departmentwisepdf','get_departmentwisepdf')->name('departmentwisepdf');

        Route::get('datelylatereport','get_datewiselateview')->name('datewiselatereport');
        Route::post('datewiselatedata','get_datewiselatedata')->name('get_datewiselatedata');
        Route::get('datewiselatepdf','get_datewiselatepdf')->name('datewiselatepdf');

        Route::get('dailyouttimereport','get_dailyouttimeview')->name('dailyouttimereport');
        Route::post('dailyouttimedata','get_dailyouttimedata')->name('get_dailyouttimedata');
        Route::get('dailyouttimepdf','get_dailyouttimepdf')->name('dailyouttimepdf');

        Route::get('dailyintimereport','get_dailyintimeview')->name('dailyintimereport');
        Route::post('dailyintimedata','get_dailyintimedata')->name('get_dailyintimedata');
        Route::get('dailyintimepdf','get_dailyintimepdf')->name('dailyintimepdf');

    });

    /************* Report Routes end **************/

});
